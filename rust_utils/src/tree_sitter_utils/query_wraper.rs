use rayon::iter::{IntoParallelRefIterator, ParallelIterator};
use tree_sitter::Query;

use super::languages::Language;

/// Limit of the tree sitter parser
const MAX_QUERY_SIZE : usize = 1_000_000;



/// Wrapper for a query, the query string and the language.
/// Note : store the query_str to not drop it before the query
pub struct QueryWrapper {
    query : Query,
    _query_str : String, // WARN ! : keep the string to avoid the query to be dropped (c pointer)
    _language : Language
}

impl QueryWrapper {
    pub fn new(query_str : &str, language : &Language) -> Vec<QueryWrapper> {
       let mut queries = Vec::new();
       let mut query_acc = String::new();
       for line in query_str.lines() {
           
           if query_acc.len() + line.len() >= MAX_QUERY_SIZE {
                queries.push(query_acc);
                query_acc = String::new();
                
           }
           query_acc.push_str(format!("{}\n", line).as_str());
        }


        if query_acc.len() > 0 {
            queries.push(query_acc);
        }

        queries.par_iter().map(|query_str| {
            let query = Query::new(language.get_tree_sitter_language(), query_str).unwrap();
            QueryWrapper {
                query,
                _query_str : query_str.to_string(),
                _language : language.clone()
            }
        }).collect()
    }

    pub fn get_query(&self) -> &Query {
        &self.query
    }
}


#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_load_and_cut_query_empty() {
        let query_str = "";
        let language = Language::Rust;
        let query = QueryWrapper::new(query_str, &language);
        assert_eq!(query.len(), 0);
    }

    #[test]
    fn test_load_and_cut_query() {
        let query_str = "(arguments (identifier) (call_expression function: (identifier) arguments: (arguments (string_literal))) (identifier) (reference_expression value: (call_expression function: (scoped_identifier path: (identifier) name: (identifier)) arguments: (arguments))) (generic_function function: (identifier) type_arguments: (type_arguments (reference_type type: (type_identifier)))) (generic_function function: (identifier) type_arguments: (type_arguments (reference_type type: (type_identifier))))) @CVE-2023-40030_17";
        let language = Language::Rust;
        let query = QueryWrapper::new(query_str, &language);
        let tree_sitter_query = Query::new(language.get_tree_sitter_language(), query_str).unwrap();
        assert_eq!(query.len(), 1);
        assert_eq!(query[0].get_query().capture_names(), tree_sitter_query.capture_names());
    }

    #[test]
    #[ignore]
    fn test_load_and_cut_rust_query() {
        let query_str = include_str!("../../tests/ressources/combined_queries_rust.scm");
        let language = Language::Rust;
        let query = QueryWrapper::new(query_str, &language);
        assert_eq!(query.len(), 1);
    }

    #[test]
    #[ignore]
    fn test_load_and_cut_too_long_c_query() {
        let query_str = include_str!("../../tests/ressources/combined_queries_c.scm");
        let language = Language::C;
        let query = QueryWrapper::new(query_str, &language);
        assert!(query.len() > 1);
    }
}