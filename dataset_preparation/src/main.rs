use std::io;

use log::info;
use dotenv::dotenv;
use rust_utils::logger::common_logger::init_logger;
use crate::params::argv::get_program_args;
use crate::pipeline::prepare_dataset;


mod params;
mod cve;
mod conversion;
mod pipeline;




fn main() -> Result<(), io::Error>{
    dotenv::dotenv().ok();
    init_logger();
    dotenv().expect("Failed to load .env file");
    let argv = get_program_args();
    info!("🚀 Start dataset_preparation.");
    prepare_dataset(&argv.input_dataset_folder_path, &argv.output_dataset_folder_path, &argv.language)
}