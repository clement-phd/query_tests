import os

# Define your base directory here
base_dir = '/home/clahoche/Documents/github/cve-detection/benchmark/results/all_files_results'

for dirpath, dirnames, files in os.walk(base_dir):
    for file_name in files:
        file_path = os.path.join(dirpath, file_name)
        # Open each file to read and then write back the modified content
        with open(file_path, 'r') as file:
            content = file.read()
        # Replace the text
        modified_content = content.replace("\"min_line_of_code\"", "\"min_query_line_of_code\"")
        # Write back the modified content
        with open(file_path, 'w') as file:
            file.write(modified_content)