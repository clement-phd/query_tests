use std::fs;
use std::path::Path;

use plot_helper::data::filtering::Filter;
use plot_helper::data::plot_data::Layout;
use plot_helper::data::plottable::key::SerieKey;
use plot_helper::data::plottable::sample::Sample;
use plot_helper::data::plottable::PlottableStruct;
use plot_helper::plotter::line_plot::line_plot;
use plot_helper::plotter::scatter_plot::scatter_plot;
use plot_helper::stats::stats_serie::MetricName;
use serde_derive::{Deserialize, Serialize};


pub mod multi_line_query;
pub mod single_line_query;
pub mod correctness;
pub mod dataset;
pub mod memory;
pub mod repo;
pub mod hunk;


/// wrapper for the scatter and line plot function
/// plot 2 line and 2 scatter plot, one with outliers and one without
fn plot_wrapper<SampleType, KeyType>(
    data : &PlottableStruct<SampleType, KeyType>, 
    legend_serie_key : Option<KeyType>,
    folder_path : &str,
    file_name : &str,
    layout : &Layout,

    series : Vec<(KeyType, KeyType, Option<Vec<&Filter<KeyType>>>)>,
    
    remove_outliers : Vec<KeyType>,
) -> Result<(), Box<dyn std::error::Error>> 
where 
    KeyType : SerieKey,
    SampleType : Sample<KeyType>
{
    let file_name_o = Path::new(file_name);
    let file_name = file_name_o.file_stem().unwrap().to_str().unwrap();
    let without_outliers_name = format!("{}_without_outliers", file_name);
    let with_outliers_name = format!("{}_with_outliers", file_name);
    let save_folder_path_o = Path::new(folder_path);
    let save_folder_path_with_outliers_o = save_folder_path_o.join(with_outliers_name.as_str());
    fs::create_dir_all(save_folder_path_with_outliers_o.to_str().unwrap())?;
    let save_folder_path_without_outliers_o = save_folder_path_o.join(without_outliers_name.as_str());
    fs::create_dir_all(save_folder_path_without_outliers_o.to_str().unwrap())?;

    
    let scatter_with_outliers_save_path_o = save_folder_path_with_outliers_o.join(format!("scatter_{}.png", with_outliers_name));
    let scatter_without_outliers_save_path_o = save_folder_path_without_outliers_o.join(format!("scatter_{}.png", without_outliers_name));

    let line_with_outliers_save_path_o = save_folder_path_with_outliers_o.join(format!("line_{}.png", with_outliers_name));
    let line_without_outliers_save_path_o = save_folder_path_without_outliers_o.join(format!("line_{}.png", without_outliers_name));

    if remove_outliers.len() != 0 {
        scatter_plot(
            data,
            legend_serie_key,
            scatter_without_outliers_save_path_o.to_str().unwrap(),
            layout,
            series.clone(),
            Some(remove_outliers.clone()),
        )?;

        line_plot(
            data,
            legend_serie_key,
            line_without_outliers_save_path_o.to_str().unwrap(),
            layout,
            series.clone(),
            Some(remove_outliers.clone()),
            MetricName::Mean,
        )?;
    }

    

    scatter_plot(
        data,
        legend_serie_key,
        scatter_with_outliers_save_path_o.to_str().unwrap(),
        layout,
        series.clone(),
        None,
    )?;

    line_plot(
        data,
        legend_serie_key,
        line_with_outliers_save_path_o.to_str().unwrap(),
        layout,
        series.clone(),
        None,
        MetricName::Mean,
    )?;

    Ok(())
}

/// used to store the stats of a querry in csv
#[derive(Debug, Clone, Deserialize, Serialize)]
struct QueryStatsWrapper {
    pub language : String,
    #[serde(rename(serialize = "parsing time mean (s)", deserialize = "parsing time mean (s)"))]
    pub parsing_time_mean : String,
    #[serde(rename(serialize = "parsing time median (s)", deserialize = "parsing time median (s)"))]
    pub parsing_time_median : String,
    #[serde(rename(serialize = "query time mean (s)", deserialize = "query time mean (s)"))]
    pub query_time_mean : String,
    #[serde(rename(serialize = "query time median (s)", deserialize = "query time median (s)"))]
    pub query_time_median : String,
    #[serde(rename(serialize = "# lines mean", deserialize = "# lines mean"))]
    pub lines_nulber_mean : String,
}


impl QueryStatsWrapper {
    pub fn new(
        language : String,
        parsing_time_mean : f64,
        parsing_time_median : f64,
        query_time_mean : f64,
        query_time_median : f64,
        lines_nulber_mean : f64,
    ) -> QueryStatsWrapper {
        QueryStatsWrapper {
            language,
            parsing_time_mean : format!("{:.5}", parsing_time_mean),
            parsing_time_median : format!("{:.5}", parsing_time_median),
            query_time_mean : format!("{:.5}", query_time_mean),
            query_time_median : format!("{:.5}", query_time_median),
            lines_nulber_mean : format!("{:.5}", lines_nulber_mean),
        }
    }
}