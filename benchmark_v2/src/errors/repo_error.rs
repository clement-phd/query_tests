use std::fmt::{Display, Formatter, self};
use std::io;

use log::error;

/// RepositoryError enum
/// contains all the possible errors that can occur when managing a repository
#[derive(Clone, PartialEq, Eq, Hash, Debug)]
pub enum RepoError {
    /// the commit hash is not found in the repository
    CommitNotFound(String),

    /// the url of the repository is not the same as the one given
    /// UrlMismatch(get, expected),
    UrlMismatch(String, String),

    /// their is an unexpected error
    UnexpectedError(String),
    IOError(String),
}

impl RepoError {
    pub fn log(&self) {
        error!("{}", self);
    }
}

impl Display for RepoError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            RepoError::CommitNotFound(s) => write!(f, "Commit not found: {}", s),
            RepoError::UrlMismatch(get, expected) => write!(f, "Url mismatch: get: {}, expected: {}", get, expected),
            RepoError::UnexpectedError(s) => write!(f, "Unexpected error: {}", s),
            RepoError::IOError(s) => write!(f, "IO error: {}", s),
        }
    }
}

impl From<io::Error> for RepoError {
    fn from(error: io::Error) -> Self {
        RepoError::IOError(error.to_string())
    }
}