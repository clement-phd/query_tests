use std::fs;

use cve_dataset::cve_commit::CveCommit;
use cve_dataset::patch::Patch;
use log::error;
use rust_utils::tree_sitter_utils::languages::Language;
use rust_utils::utils::is_only_whitespace;
use serde_derive::{Deserialize, Serialize};

use crate::cve::{Cve, CveFile};

#[derive(Debug, Clone, Serialize, Deserialize, Hash)]
pub struct ExtractedCveCode {
    start_line : usize,
    end_line : usize,
    code : String,
    original_file_content : String,
}

impl ExtractedCveCode {
    pub fn new(start_line : usize, end_line : usize, original_file_content : impl Into<String>) -> ExtractedCveCode {
        let original_file_content_string = original_file_content.into();
        let code_lines = original_file_content_string.lines();
        let mut code = String::new();
        for (index, line) in code_lines.enumerate() {
            if index >= start_line && index <= end_line {
                code.push_str(line);
                code.push_str("\n");
            }
        }

        ExtractedCveCode {
            start_line,
            end_line,
            code,
            original_file_content : original_file_content_string,
        }
    }

    pub fn lines(&self) -> Vec<&str> {
        self.code.lines().collect()
    }

    pub fn is_cve_code_valid(&self) -> bool {
        !is_only_whitespace(&self.code)
    }

    pub fn get_code(&self) -> &str {
        &self.code
    }

    pub fn get_start_line(&self) -> usize {
        self.start_line
    }

    pub fn get_end_line(&self) -> usize {
        self.end_line
    }

    pub fn get_original_file_content(&self) -> &str {
        &self.original_file_content
    }

    pub fn to_json(&self) -> String {
        serde_json::to_string(self).unwrap()
    }


    pub fn from_json(path : &str) -> ExtractedCveCode {
        match fs::read_to_string(path) {
            Ok(content) => {
                // Successfully read the file
                return serde_json::from_str(content.as_str()).unwrap();
            }
            Err(error) => {
                // Failed to read the file
                error!("Failed to read the file {} : {}", path, error);
                panic!();
            }
        }
        
    }

}




fn get_cve_codes_from_patch(patch : &Patch, original_file_content : &str) -> Vec<ExtractedCveCode> {
    let mut cve_codes : Vec<ExtractedCveCode> = Vec::new();

    for subpatch in patch.subpatchs.iter() {
        // Get the first and last removed line
        let mut has_removed_line = false;
        let mut first_removed_line : usize = 0;
        let mut last_removed_line : usize = 0;
        let mut added_line_number : usize = 0;

        for (index, line) in subpatch.lines.iter().enumerate() {
            if line.starts_with("+") {
                added_line_number += 1;
            }
            if line.starts_with("-") {
                if first_removed_line == 0 {
                    // -1 because the first line is the hunk
                    // additionnal -1 because the start_original_line_number is a real line number (not index)
                    // Don't forget to remove the added line
                    first_removed_line = subpatch.start_original_line_number + index - added_line_number - 2; 
                    last_removed_line = subpatch.start_original_line_number + index - added_line_number - 2;
                }else {
                    last_removed_line = subpatch.start_original_line_number + index - added_line_number - 2;
                }
                has_removed_line = true;
            }
        }

    
        if has_removed_line {
            cve_codes.push(ExtractedCveCode::new(first_removed_line, last_removed_line, original_file_content));
        }
        
    }




    cve_codes
}



/// Get the code of the patch beetween the first and last removed line
pub fn extract_cve_removed_code_changes(pull_request : &CveCommit, language : &Language) -> Cve {

    let mut files : Vec<CveFile> = Vec::new();
    let mut other_than_modified_files = Vec::new();
    let mut other_than_language_files = Vec::new();

    for file in pull_request.files.iter() {

        if !Language::is_file_language(&language, file.file_basename.as_str()) {
            other_than_language_files.push(file.clone());
            continue;
        }

        if !file.patch.is_modified() {
            other_than_modified_files.push(file.clone());
            continue;
        }

        let cve_codes = get_cve_codes_from_patch(&file.patch, &file.original_file_content);

        let cve_file = CveFile {
            cve_id: file.cve_id.clone(),
            file_basename: file.file_basename.clone(),
            file_inside_path: file.inside_file_path.clone(),
            original_file_content: file.original_file_content.clone(),
            fixed_file_content: file.fixed_file_content.clone(),
            cve_codes: cve_codes,
        };
        files.push(cve_file);
    }

    Cve{
        cve_id: pull_request.cve_id.clone(),
        files,
        other_than_language_files : other_than_language_files,
        other_than_modified_files : other_than_modified_files,
    }
}