




#[derive(Debug)]
pub enum GithubRequestError {
    RequestError(reqwest::Error),
    NoCredentialsError,
}

impl From<reqwest::Error> for GithubRequestError {
    fn from(e: reqwest::Error) -> Self {
        GithubRequestError::RequestError(e)
    }
}

impl std::fmt::Display for GithubRequestError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            GithubRequestError::RequestError(e) => write!(f, "An error occurred with the request: {}", e),
            GithubRequestError::NoCredentialsError => write!(f, "No credentials found"),
        }
    }
}