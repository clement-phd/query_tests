
use std::time::{Duration, Instant};



#[derive(Clone, PartialEq, Debug)]
pub struct MonitoringResults {
    pub time : Duration,
    
}

impl MonitoringResults {
    
    pub fn monitor_thread<R, F>(
        work: F,
    ) -> (R, MonitoringResults)
    where
        R : Send + Sync ,
        F : (Fn() -> R) + Send + Sync,
    {
        
        let start_time = Instant::now();
        let result = work();
        let end_time = Instant::now();
        let time = end_time.duration_since(start_time);
        let monitoring_results = MonitoringResults {
            time,
        };
        (result, monitoring_results)
    

        
    }
}