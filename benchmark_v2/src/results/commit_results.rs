use std::path::Path;

use generate_query::params::argv::Generator;
use rust_utils::tree_sitter_utils::languages::Language;
use serde_derive::{Deserialize, Serialize};

use super::file_results::FileResult;
use crate::instances::instance::Instance;
use crate::repository::diff_file::DiffFile;




/// results of the run of the queries on a repository commit
/// identified with the commit hash, and the preceding commit hash
#[derive(Clone, PartialEq, Serialize, Deserialize, Debug)]
pub struct CommitResults {
    // query info
    pub language: Language,
    pub generator: Generator,
    pub min_query_line_of_code: u32,

    // identifiers
    pub repo_url: String,
    pub from_commit_hash: String,
    pub commit_hash: String,

    // results
    pub results: Vec<FileResult>,
    pub files : Vec<DiffFile>,
}


impl CommitResults {
    /// Create a new CommitResults from the results of files and the commit hash
    pub fn new(
        repo_url: &str,
        commit_hash: &str,
        from_commit_hash: &str,
        instance : &Instance,
        results: Vec<FileResult>,
        files : Vec<DiffFile>,
    ) -> CommitResults {
        // check if the results are not empty and are coherent (same language, generator, min_query_line_of_code)
        for file_result in &results {
            if &file_result.language != instance.get_language() {
                panic!("The results of the commit {} are not coherent, the language of the file {} is different from the others", commit_hash, file_result.file.file_path);
            }
            if &file_result.generator != instance.get_generator() {
                panic!("The results of the commit {} are not coherent, the generator of the file {} is different from the others", commit_hash, file_result.file.file_path);
            }
            if file_result.min_query_line_of_code != instance.get_min_line_of_code_for_cve_code() as u32{
                panic!("The results of the commit {} are not coherent, the min_query_line_of_code of the file {} is different from the others", commit_hash, file_result.file.file_path);
            }
        }

        CommitResults {
            repo_url: repo_url.to_string(),
            language : instance.get_language().clone(),
            generator : instance.get_generator().clone(),
            min_query_line_of_code : instance.get_min_line_of_code_for_cve_code() as u32,
            from_commit_hash: from_commit_hash.to_string(),
            commit_hash: commit_hash.to_string(),
            results,
            files,
        }
    }

    pub fn get_file_name(&self) -> String {
        Self::get_commit_file_name(self.commit_hash.as_str())
    }


    pub fn get_commit_file_name(commit_hash : &str) -> String {
        format!("{}.json", commit_hash)
    }

    pub fn save(&self, folder_path : &str) -> Result<(), std::io::Error> {
        let folder_path_o = Path::new(folder_path);
        let file_path = folder_path_o.join(self.get_file_name());
        let file = std::fs::File::create(file_path)?;
        serde_json::to_writer(file, self).unwrap();
        Ok(())
    }
}