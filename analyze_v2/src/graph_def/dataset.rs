
use std::fs;
use std::path::Path;

use plot_helper::data::plottable::{Plottable, PlottableStruct};
use serde::{Deserialize, Serialize};

use crate::data::dataset_sample::DatasetSample;
use crate::data::keys::DatasetSerieCveKey;

/// used to store the stats of the files in csv
#[derive(Debug, Clone, Deserialize, Serialize)]
struct DatasetFilesStatsWrapper {
    language : String,
    cve : String,
    #[serde(rename(serialize = "# of the same language files", deserialize = "# of the same language files"))]
    relevant_number_of_files : String,
    #[serde(rename(serialize = "# of not the same language files", deserialize = "# of not the same language files"))]
    other_than_language_files_number : String,
    #[serde(rename(serialize = "# of not modified files", deserialize = "# of not modified files"))]
    other_than_modified_files_number : String,
    #[serde(rename(serialize = "# of files kept for the study", deserialize = "# of files kept for the study"))]
    kept_files_number : String,
    
    #[serde(rename(serialize = "lost proportion (%)", deserialize = "lost proportion (%)"))]
    lost_proportions : String,
}


impl DatasetFilesStatsWrapper {
    pub fn new(
        language : &str,
        cve : &str,
        kept_files_number : u64,
        other_than_language_files_number : u64,
        other_than_modified_files_number : u64,
        relevant_number_of_files : u64,
        lost_proportions : f32,
    ) -> DatasetFilesStatsWrapper {
        DatasetFilesStatsWrapper {
            language : language.to_string(),
            cve : cve.to_string(),
            kept_files_number : kept_files_number.to_string(),
            other_than_language_files_number : other_than_language_files_number.to_string(),
            other_than_modified_files_number : other_than_modified_files_number.to_string(),
            relevant_number_of_files : relevant_number_of_files.to_string(),
            lost_proportions : format!("{:.2}", lost_proportions * 100.0),
        }
    }
}

pub fn gen_dataset_data(data : &PlottableStruct<DatasetSample, DatasetSerieCveKey>, output_dir_path : &str) -> Result<(), Box<dyn std::error::Error>> {
    let folder_path_o = Path::new(output_dir_path).join("dataset_data");
    fs::create_dir_all(folder_path_o.to_str().unwrap())?;

    // file informations
    {
        let save_folder_path_o = folder_path_o.join("file_informations");
        fs::create_dir_all(save_folder_path_o.to_str().unwrap())?;
        {
            let file_path_o = save_folder_path_o.join(format!("data.csv"));
            let mut csv_writer = csv::Writer::from_path(file_path_o.to_str().unwrap())?;
            let all_languages = data.get_string_series(&DatasetSerieCveKey::Language);
            let mut unique_languages: Vec<String> = data
                    .get_string_series(&DatasetSerieCveKey::Language)
                    .iter()
                    .map(|f| f.clone())
                    .collect::<std::collections::HashSet<String>>()
                    .into_iter()
                    .collect();
            unique_languages.sort();

            for language in unique_languages {
                
                //csv_writer.write_record(&["cve", "kept files number", "other than language files number", "other than modified files number", "relevant number of files", "lost proportions"])?;
                let cves = 
                    data.get_string_series(&DatasetSerieCveKey::Cve)
                    .iter().enumerate().filter(|(i, _)| all_languages[*i] == *language)
                    .map(|(_, cve)| cve.clone()).collect::<Vec<_>>();
                let number_files = 
                    data.get_numeric_series(&DatasetSerieCveKey::NumberFiles)
                    .iter().enumerate().filter(|(i, _)| all_languages[*i] == *language)
                    .map(|(_, number_files)| *number_files).collect::<Vec<_>>();
                let number_other_than_language =
                    data.get_numeric_series(&DatasetSerieCveKey::NumberOtherThanLanguage)
                    .iter().enumerate().filter(|(i, _)| all_languages[*i] == *language)
                    .map(|(_, number_other_than_language)| *number_other_than_language).collect::<Vec<_>>();
                let number_other_than_modified_files =
                    data.get_numeric_series(&DatasetSerieCveKey::NumberOtherThanModifiedFiles)
                    .iter().enumerate().filter(|(i, _)| all_languages[*i] == *language)
                    .map(|(_, number_other_than_modified_files)| *number_other_than_modified_files).collect::<Vec<_>>();

                for (((_cve, number_files), _number_other_than_language), number_other_than_modified_files) in 
                    cves.iter().zip(number_files.iter()).zip(number_other_than_language.iter()).zip(number_other_than_modified_files.iter()) {
                    let relevant_files = number_files + number_other_than_modified_files;
                    let _lost_proportion = number_other_than_modified_files / relevant_files;

                    /*csv_writer.serialize(
                        DatasetFilesStatsWrapper::new(
                            language.as_str(),
                            cve,
                            *number_files as u64,
                            *number_other_than_language as u64,
                            *number_other_than_modified_files as u64,
                            relevant_files as u64,
                            lost_proportion
                        )
                    )?;*/
                }

                // add total
                let total_number_files = number_files.iter().sum::<f32>();
                let total_number_other_than_language = number_other_than_language.iter().sum::<f32>();
                let total_number_other_than_modified_files = number_other_than_modified_files.iter().sum::<f32>();
                let total_relevant_files = total_number_files + total_number_other_than_modified_files;
                let total_lost_proportion = total_number_other_than_modified_files / total_relevant_files;

                csv_writer.serialize(
                    DatasetFilesStatsWrapper::new(
                        language.as_str(),
                        "total",
                        total_number_files as u64,
                        total_number_other_than_language as u64,
                        total_number_other_than_modified_files as u64,
                        total_relevant_files as u64,
                        total_lost_proportion
                    )
                )?;
            }
        }
    }

    Ok(())
}