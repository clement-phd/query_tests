use tree_sitter::Node;

use crate::errors::error::{GenerateQueryErrorPayload, GenerateQueryErrorType};
use crate::errors::warning::GenerateQueryWarning;

/**
 * get the general querry from a node (removing source_code for example)
 */
pub fn get_query_from_node(label : impl Into<String>, node: &Node, min_number_of_line : usize) -> Result<(String, Option<GenerateQueryWarning>), GenerateQueryErrorPayload> {
    let base_query = {
       
        let mut actual_node = node.child(0).unwrap();
        let mut query = String::new();
        loop {
            query.push_str(format!("{}", actual_node.to_sexp()).as_str());

            match actual_node.next_sibling() {
                Some(next_node) => {
                    actual_node = next_node;
                    query.push_str(" . ");
                },
                None => {
                    break;
                }
            }
        }
        query
    

    };

    let query = format!("(_ {} )", base_query);

    if node.end_position().row - node.start_position().row < min_number_of_line {
        let label : String = label.into();
        return Err(GenerateQueryErrorPayload::new(label.as_str(), GenerateQueryErrorType::ContextThreshold, None));
    }

    return Ok((query, None));
}
