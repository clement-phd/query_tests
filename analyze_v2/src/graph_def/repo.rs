use std::collections::HashSet;
use std::fs;
use std::path::Path;

use plot_helper::data::plottable::{Plottable, PlottableStruct};
use serde::{Deserialize, Serialize};

use crate::data::keys::RepoKey;
use crate::data::repo_sample::RepoSample;


#[derive(Debug, Clone, Deserialize, Serialize)]
struct RepoStatsWrapper {
    repo : String,
    #[serde(rename(serialize = "# of commit (1 by instance)", deserialize = "# of commit (1 by instance)"))]
    nb_commit : String,
    #[serde(rename(serialize = "min # of line for the context", deserialize = "min # of line for the context"))]
    threshold : String,
    language : String,
    #[serde(rename(serialize = "total execution time (s)", deserialize = "total execution time (s)"))]
    total_execution_time : String,
    total_file_number : u64,
    total_line_number : u64,
    total_char_number : u64,
    #[serde(rename(serialize = "mean execution time by commit (s)", deserialize = "mean execution time by commit (s)"))]
    mean_execution_time : String,
    mean_file_number_by_commit : String,
    mean_line_number_by_commit : String,
    mean_char_number_by_commit : String,
}

impl RepoStatsWrapper {
    fn new(
        repo : String, 
        nb_commit : String,
        threshold : String,
        language : String,
        total_execution_time : f64, 
        total_file_number : u64, 
        total_line_number : u64, 
        total_char_number : u64, 
        mean_execution_time : f64, 
        mean_file_number_by_commit : f64, 
        mean_line_number_by_commit : f64, 
        mean_char_number_by_commit : f64
    ) -> Self {
        Self {
            repo,
            nb_commit,
            threshold,
            language,
            total_execution_time : format!("{:.2} s", total_execution_time),
            total_file_number,
            total_line_number,
            total_char_number,
            mean_execution_time : format!("{:.2} s", mean_execution_time),
            mean_file_number_by_commit : format!("{:.2}", mean_file_number_by_commit),
            mean_line_number_by_commit : format!("{:.2}", mean_line_number_by_commit),
            mean_char_number_by_commit : format!("{:.2}", mean_char_number_by_commit),
        }
    }
}



pub fn gen_repo_data(data : &PlottableStruct<RepoSample, RepoKey>, output_dir_path : &str) -> Result<(), Box<dyn std::error::Error>> {
    let folder_path_o = Path::new(output_dir_path).join("repository");
    fs::create_dir_all(folder_path_o.to_str().unwrap())?;

    {
        
        

        let mut nb_commit_foreach_repo = std::collections::HashMap::new();
        let mut total_execution_time_foreach_repo = std::collections::HashMap::new();
        let mut total_file_number_foreach_repo = std::collections::HashMap::new();
        let mut total_line_number_foreach_repo = std::collections::HashMap::new();
        let mut total_char_number_foreach_repo = std::collections::HashMap::new();
        let mut instances = HashSet::new();

        let mut nb_commit_foreach_repo_and_threshold = std::collections::HashMap::new();
        let mut total_execution_time_foreach_repo_and_threshold = std::collections::HashMap::new();
        let mut total_file_number_foreach_repo_and_threshold = std::collections::HashMap::new();
        let mut total_line_number_foreach_repo_and_threshold = std::collections::HashMap::new();
        let mut total_char_number_foreach_repo_and_threshold = std::collections::HashMap::new();

        let repo = data.get_string_series(&RepoKey::Repo);
        let execution_time = data.get_numeric_series(&RepoKey::ExecutionTime);
        let file_number = data.get_numeric_series(&RepoKey::FileNumber);
        let line_number = data.get_numeric_series(&RepoKey::LineNumber);
        let char_number = data.get_numeric_series(&RepoKey::CharNumber);
        let threshold = data.get_numeric_series(&RepoKey::MinQueryLineOfCode).into_iter().map(|f| f as u32).collect::<Vec<_>>();
        let languages = data.get_string_series(&RepoKey::Language);

        for i in 0..repo.len() {
            let repo = repo[i].clone();
            let execution_time = execution_time[i];
            let file_number = file_number[i];
            let line_number = line_number[i];
            let char_number = char_number[i];
            let threshold = threshold[i];
            let language = languages[i].clone();

            if !nb_commit_foreach_repo.contains_key(&repo) {
                nb_commit_foreach_repo.insert(repo.clone(), 0);
                total_execution_time_foreach_repo.insert(repo.clone(), 0.0);
                total_file_number_foreach_repo.insert(repo.clone(), 0);
                total_line_number_foreach_repo.insert(repo.clone(), 0);
                total_char_number_foreach_repo.insert(repo.clone(), 0);
            }

            *nb_commit_foreach_repo.get_mut(&repo).unwrap() += 1;
            *total_execution_time_foreach_repo.get_mut(&repo).unwrap() += execution_time;
            *total_file_number_foreach_repo.get_mut(&repo).unwrap() += file_number as u64;
            *total_line_number_foreach_repo.get_mut(&repo).unwrap() += line_number as u64;
            *total_char_number_foreach_repo.get_mut(&repo).unwrap() += char_number as u64;
            

            let instance_key = (repo.clone(), threshold, language.clone());
            instances.insert((threshold, language.clone()));
            if !nb_commit_foreach_repo_and_threshold.contains_key(&instance_key) {
                nb_commit_foreach_repo_and_threshold.insert(instance_key.clone(), 0);
                total_execution_time_foreach_repo_and_threshold.insert(instance_key.clone(), 0.0);
                total_file_number_foreach_repo_and_threshold.insert(instance_key.clone(), 0);
                total_line_number_foreach_repo_and_threshold.insert(instance_key.clone(), 0);
                total_char_number_foreach_repo_and_threshold.insert(instance_key.clone(), 0);
            }

            *nb_commit_foreach_repo_and_threshold.get_mut(&instance_key).unwrap() += 1;
            *total_execution_time_foreach_repo_and_threshold.get_mut(&instance_key).unwrap() += execution_time;
            *total_file_number_foreach_repo_and_threshold.get_mut(&instance_key).unwrap() += file_number as u64;
            *total_line_number_foreach_repo_and_threshold.get_mut(&instance_key).unwrap() += line_number as u64;
            *total_char_number_foreach_repo_and_threshold.get_mut(&instance_key).unwrap() += char_number as u64;            
        }
        
        {
            let folder_path_o = folder_path_o.join("stats_by_repo");
            fs::create_dir_all(folder_path_o.to_str().unwrap())?;
            let save_folder_path_o = folder_path_o.join("stats.csv");
            let mut wtr = csv::Writer::from_path(save_folder_path_o)?;
            let mut keys = nb_commit_foreach_repo.keys().collect::<Vec<_>>();
            keys.sort();

            for key in keys {
                let nb_commit = nb_commit_foreach_repo.get(key).unwrap();
                let total_execution_time = total_execution_time_foreach_repo.get(key).unwrap();
                let total_file_number = total_file_number_foreach_repo.get(key).unwrap();
                let total_line_number = total_line_number_foreach_repo.get(key).unwrap();
                let total_char_number = total_char_number_foreach_repo.get(key).unwrap();

                let mean_execution_time = *total_execution_time as f64/ *nb_commit as f64;
                let mean_file_number_by_commit = *total_file_number as f64 / *nb_commit as f64;
                let mean_line_number_by_commit = *total_line_number as f64 / *nb_commit as f64;
                let mean_char_number_by_commit = *total_char_number as f64 / *nb_commit as f64;

                let stats = RepoStatsWrapper::new(
                    key.clone(),
                    nb_commit.to_string(),
                    "all instances".to_string(),
                    "all languages".to_string(),
                    *total_execution_time as f64,
                    *total_file_number,
                    *total_line_number,
                    *total_char_number,
                    mean_execution_time,
                    mean_file_number_by_commit,
                    mean_line_number_by_commit,
                    mean_char_number_by_commit,
                );

                wtr.serialize(stats)?;
            }
        }

        {
            let folder_path_o = folder_path_o.join("stats_by_repo_and_instances");
            fs::create_dir_all(folder_path_o.to_str().unwrap())?;
            let save_folder_path_o = folder_path_o.join("stats.csv");
            let mut wtr = csv::Writer::from_path(save_folder_path_o)?;
            let mut keys = nb_commit_foreach_repo_and_threshold.keys().collect::<Vec<_>>();
            keys.sort();

            for key in keys {
                let nb_commit = nb_commit_foreach_repo_and_threshold.get(key).unwrap();
                let total_execution_time = total_execution_time_foreach_repo_and_threshold.get(key).unwrap();
                let total_file_number = total_file_number_foreach_repo_and_threshold.get(key).unwrap();
                let total_line_number = total_line_number_foreach_repo_and_threshold.get(key).unwrap();
                let total_char_number = total_char_number_foreach_repo_and_threshold.get(key).unwrap();

                let mean_execution_time = *total_execution_time as f64/ *nb_commit as f64;
                let mean_file_number_by_commit = *total_file_number as f64 / *nb_commit as f64;
                let mean_line_number_by_commit = *total_line_number as f64 / *nb_commit as f64;
                let mean_char_number_by_commit = *total_char_number as f64 / *nb_commit as f64;

                let stats = RepoStatsWrapper::new(
                    key.0.clone(),
                    nb_commit.to_string(),
                    key.1.to_string(),
                    key.2.clone(),
                    *total_execution_time as f64,
                    *total_file_number,
                    *total_line_number,
                    *total_char_number,
                    mean_execution_time,
                    mean_file_number_by_commit,
                    mean_line_number_by_commit,
                    mean_char_number_by_commit,
                );

                wtr.serialize(stats)?;
            }
        }
    }

    Ok(())
}