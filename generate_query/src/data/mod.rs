use dataset_preparation::conversion::line_removed::ExtractedCveCode;




#[derive(Debug)]
pub struct ExtractedCveCodePayload {
    extracted_cve_code : ExtractedCveCode,
    label : String,
}

impl ExtractedCveCodePayload {
    pub fn new(extracted_cve_code : ExtractedCveCode, label : impl Into<String>) -> ExtractedCveCodePayload {
        ExtractedCveCodePayload {
            extracted_cve_code,
            label : label.into(),
        }
    }

    pub fn new_from_file(file_path : impl AsRef<std::path::Path>) -> ExtractedCveCodePayload {
        let extracted_cve_code = ExtractedCveCode::from_json(file_path.as_ref().to_str().unwrap());
        let label = file_path.as_ref().file_stem().unwrap().to_str().unwrap();
        ExtractedCveCodePayload {
            extracted_cve_code,
            label : label.into(),
        }
    }

    pub fn get_extracted_cve_code(&self) -> &ExtractedCveCode {
        &self.extracted_cve_code
    }

    pub fn get_label(&self) -> &str {
        &self.label
    }
}