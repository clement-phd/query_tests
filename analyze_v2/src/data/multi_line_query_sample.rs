use std::collections::HashSet;

use benchmark_v2::prepared_dataset::FileToTest;
use benchmark_v2::results::file_results::FileResult;
use generate_query::params::argv::Generator;
use plot_helper::data::plottable::sample::{Sample, SimpleSample};
use rust_utils::tree_sitter_utils::languages::Language;
use serde::{Deserialize, Serialize};
use use_query::run_query::query_output::QueryOutput;

use super::keys::MultiLineQueryKey;




#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct MultiLineQuerySample {
    parsing_time: f64,
    parsing_max_memory: f64,
    file: FileToTest,
    min_query_line_of_code: u64,
    language: Language,
    generator: Generator,
    file_nb_of_lines: u64,
    file_nb_of_chars: u64,
    query_nb_of_lines: u64,
    query_time: f64,
    query_max_memory: f64,
    query_output: Vec<QueryOutput>,
    matchs_number: u64,

    time_difference_single_multi_line_query: f64,
    true_positive: bool,
    false_positive: bool,
    false_negative: bool,
    true_negative: bool,

    filtered_true_positive: bool,
    filtered_false_positive: bool,
    filtered_false_negative: bool,
    filtered_true_negative: bool,

    non_biaised_filtered_true_positive: u64,
    non_biaised_filtered_false_positive: u64,
    non_biaised_filtered_false_negative: u64,
    non_biaised_filtered_true_negative: u64,
}

impl MultiLineQuerySample {

    pub fn get_generator(&self) -> &Generator {
        &self.generator
    }

    pub fn get_language(&self) -> &Language {
        &self.language
    }
}

impl Sample<MultiLineQueryKey> for MultiLineQuerySample {
    fn get_numeric_value(&self, key: &MultiLineQueryKey) -> f32 {
        match key {
            MultiLineQueryKey::ParsingTime => self.parsing_time as f32,
            MultiLineQueryKey::ParsingMaxMemory => self.parsing_max_memory as f32,
            MultiLineQueryKey::File => unreachable!("file is not numeric"),
            MultiLineQueryKey::MinQueryLineOfCode => self.min_query_line_of_code as f32,
            MultiLineQueryKey::Language => unreachable!("language is not numeric"),
            MultiLineQueryKey::Generator => unreachable!("generator is not numeric"),
            MultiLineQueryKey::FileNbOfLines => self.file_nb_of_lines as f32,
            MultiLineQueryKey::FileNbOfChars => self.file_nb_of_chars as f32,
            MultiLineQueryKey::QueryNbOfLines => self.query_nb_of_lines as f32,
            MultiLineQueryKey::QueryTime => self.query_time as f32,
            MultiLineQueryKey::QueryMaxMemory => self.query_max_memory as f32,
            MultiLineQueryKey::QueryOutput => unreachable!("query output is not numeric"),
            MultiLineQueryKey::MatchsNumber => self.matchs_number as f32,
            MultiLineQueryKey::TimeDifferenceSingleMultiLineQuery => self.time_difference_single_multi_line_query as f32,
            MultiLineQueryKey::TruePositive => if self.true_positive { 1.0 } else { 0.0 },
            MultiLineQueryKey::FalsePositive => if self.false_positive { 1.0 } else { 0.0 },
            MultiLineQueryKey::FalseNegative => if self.false_negative { 1.0 } else { 0.0 },
            MultiLineQueryKey::TrueNegative => if self.true_negative { 1.0 } else { 0.0 },
            MultiLineQueryKey::FilteredTruePositive => if self.filtered_true_positive { 1.0 } else { 0.0 },
            MultiLineQueryKey::FilteredFalsePositive => if self.filtered_false_positive { 1.0 } else { 0.0 },
            MultiLineQueryKey::FilteredFalseNegative => if self.filtered_false_negative { 1.0 } else { 0.0 },
            MultiLineQueryKey::FilteredTrueNegative => if self.filtered_true_negative { 1.0 } else { 0.0 },
            MultiLineQueryKey::NonBiasedTruePositive => self.non_biaised_filtered_true_positive as f32,
            MultiLineQueryKey::NonBiasedFalsePositive => self.non_biaised_filtered_false_positive as f32,
            MultiLineQueryKey::NonBiasedFalseNegative => self.non_biaised_filtered_false_negative as f32,
            MultiLineQueryKey::NonBiasedTrueNegative => self.non_biaised_filtered_true_negative as f32,
        }
    }

    fn get_string_value(&self, key: &MultiLineQueryKey) -> String {
        match key {
            MultiLineQueryKey::File => unreachable!("file is not string"),
            MultiLineQueryKey::Language => self.language.to_string(),
            MultiLineQueryKey::Generator => self.generator.to_string(),
            MultiLineQueryKey::QueryOutput => unreachable!("query output is not string"),
            MultiLineQueryKey::ParsingTime => unreachable!("parsing time is not string"),
            MultiLineQueryKey::ParsingMaxMemory => unreachable!("parsing max memory is not string"),
            MultiLineQueryKey::MinQueryLineOfCode => unreachable!("min query line of code is not string"),
            MultiLineQueryKey::FileNbOfLines => unreachable!("file nb of lines is not string"),
            MultiLineQueryKey::FileNbOfChars => unreachable!("file nb of chars is not string"),
            MultiLineQueryKey::QueryNbOfLines => unreachable!("query nb of lines is not string"),
            MultiLineQueryKey::QueryTime => unreachable!("query time is not string"),
            MultiLineQueryKey::QueryMaxMemory => unreachable!("query max memory is not string"),
            MultiLineQueryKey::MatchsNumber => unreachable!("matchs number is not string"),
            MultiLineQueryKey::TimeDifferenceSingleMultiLineQuery => unreachable!("time difference single multi line query is not string"),
            MultiLineQueryKey::TruePositive => unreachable!("true positive is not string"),
            MultiLineQueryKey::FalsePositive => unreachable!("false positive is not string"),
            MultiLineQueryKey::FalseNegative => unreachable!("false negative is not string"),
            MultiLineQueryKey::TrueNegative => unreachable!("true negative is not string"),
            MultiLineQueryKey::FilteredTruePositive => unreachable!("filtered true positive is not string"),
            MultiLineQueryKey::FilteredFalsePositive => unreachable!("filtered false positive is not string"),
            MultiLineQueryKey::FilteredFalseNegative => unreachable!("filtered false negative is not string"),
            MultiLineQueryKey::FilteredTrueNegative => unreachable!("filtered true negative is not string"),
            MultiLineQueryKey::NonBiasedTruePositive => unreachable!("non biased true positive is not string"),
            MultiLineQueryKey::NonBiasedFalsePositive => unreachable!("non biased false positive is not string"),
            MultiLineQueryKey::NonBiasedFalseNegative => unreachable!("non biased false negative is not string"),
            MultiLineQueryKey::NonBiasedTrueNegative => unreachable!("non biased true negative is not string"),
           
        }
    }
}


impl SimpleSample<MultiLineQueryKey> for MultiLineQuerySample {
    fn new_from_file_path(file_path : &str) -> Result<Self, Box<dyn std::error::Error>> {
        let file_content = std::fs::read_to_string(file_path)?;
        let item : FileResult = serde_json::from_str(&file_content).unwrap();
        let multi_line_query = item.get_multi_line_query_results();

        let mut match_number = 0;
        for query_output in &multi_line_query.query_output {
            match_number += query_output.query_results.len() as u32;
        }


        let mut single_query_time_acc = 0.0;
        for instance in &item.get_single_query_instances() {
            single_query_time_acc += instance.query_time;
        }

        let time_difference_single_multi_line_query = 100.0 * single_query_time_acc/multi_line_query.query_time;

        let is_true_positive = is_true_positive(&item.file, &multi_line_query.query_output);
        let is_false_positive = is_false_positive(&item.file, &multi_line_query.query_output);
        let is_false_negative = is_false_negative(&item.file, &multi_line_query.query_output);
        let is_true_negative = is_true_negative(&item.file, &multi_line_query.query_output);

        let cve_to_filter = multi_line_query.get_all_cve_names().into_iter().collect::<HashSet<String>>();
        let cve_to_filter = cve_to_filter.into_iter().collect::<Vec<String>>();

        let filtered_true_positive = is_filtered_true_positive(&item.file, &multi_line_query.query_output, &cve_to_filter);
        let filtered_false_positive = is_filtered_false_positive(&item.file, &multi_line_query.query_output, &cve_to_filter);
        let filtered_false_negative = is_filtered_false_negative(&item.file, &multi_line_query.query_output, &cve_to_filter);
        let filtered_true_negative = is_filtered_true_negative(&item.file, &multi_line_query.query_output, &cve_to_filter);

        let non_biaised_filtered_true_positive = get_non_biaised_true_positive(&item.file, &multi_line_query.query_output, &cve_to_filter);
        let non_biaised_filtered_false_positive = get_non_biaised_false_positive(&item.file, &multi_line_query.query_output, &cve_to_filter);
        let non_biaised_filtered_false_negative = get_non_biaised_false_negative(&item.file, &multi_line_query.query_output, &cve_to_filter);
        let non_biaised_filtered_true_negative = get_non_biaised_true_negative(&item.file, &multi_line_query.query_output, &cve_to_filter);

        Ok(MultiLineQuerySample {
            parsing_time: item.parsing_time,
            parsing_max_memory: item.parsing_max_memory,
            file: item.file,
            min_query_line_of_code: item.min_query_line_of_code as u64,
            language: item.language,
            generator: item.generator,
            file_nb_of_lines: item.file_nb_of_lines as u64,
            file_nb_of_chars: item.file_nb_of_chars as u64,
            query_nb_of_lines: multi_line_query.query_nb_of_lines as u64,
            query_time: multi_line_query.query_time,
            query_max_memory: multi_line_query.query_max_memory,
            query_output: multi_line_query.query_output,
            matchs_number: match_number as u64,
            time_difference_single_multi_line_query: time_difference_single_multi_line_query, 
            true_positive: is_true_positive,
            false_positive: is_false_positive,
            false_negative: is_false_negative,
            true_negative: is_true_negative,
            filtered_true_positive : filtered_true_positive,
            filtered_false_positive : filtered_false_positive,
            filtered_false_negative : filtered_false_negative,
            filtered_true_negative : filtered_true_negative,
            non_biaised_filtered_true_positive : non_biaised_filtered_true_positive,
            non_biaised_filtered_false_positive : non_biaised_filtered_false_positive,
            non_biaised_filtered_false_negative : non_biaised_filtered_false_negative,
            non_biaised_filtered_true_negative : non_biaised_filtered_true_negative,
        })
        
    }
}

/// get if the file is a true positive (on biaised, ie count the file for one false positive for each non attendu query output)
fn get_non_biaised_true_positive(file: &FileToTest, query_outputs: &[QueryOutput], cve_to_filter : &[String]) -> u64 {
    if is_filtered_true_positive(file, query_outputs, cve_to_filter) {
        1
    } else {
        0
    }
}

/// get if the file is a false positive (on biaised, ie count the file for one false positive for each non attendu query output)
fn get_non_biaised_false_positive(file: &FileToTest, query_outputs: &[QueryOutput], cve_to_filter : &[String]) -> u64 {
    if cve_to_filter.contains(&file.cve_id){
        let mut count = query_outputs
            .iter()
            .filter(|item| !item.has_cve_match(&file.cve_id))
            .count() as u64;
        if file.is_infected { // remove the true positive sample
            count -= 1;
        }
        count
    } else {
        0
    }
}

/// get if the file is a false negative (on biaised, ie count the file for one false positive for each non attendu query output)
fn get_non_biaised_false_negative(file: &FileToTest, query_outputs: &[QueryOutput], cve_to_filter : &[String]) -> u64 {
    if is_filtered_false_negative(file, query_outputs, cve_to_filter) {
        1
    } else {
        0
    }
}

/// get if the file is a true negative (on biaised, ie count the file for one false positive for each non attendu query output)
fn get_non_biaised_true_negative(file: &FileToTest, query_outputs: &[QueryOutput], cve_to_filter : &[String]) -> u64 {
    if is_filtered_true_negative(file, query_outputs, cve_to_filter) {
        1
    } else {
        0
    }
}


/// Filter the true positive and considere only if the file cve is in the query output queries
fn is_filtered_true_positive(file: &FileToTest, query_outputs: &[QueryOutput], cve_to_filter : &[String]) -> bool { 
    file.is_infected
        && query_outputs
            .iter()
            .any(|item| item.has_cve_match(&file.cve_id))
        && cve_to_filter.contains(&file.cve_id)
}

/// Filter the false positive and considere only if the file cve is in the query output queries
fn is_filtered_false_positive(file: &FileToTest, query_outputs: &[QueryOutput], cve_to_filter : &[String]) -> bool { 
    !file.is_infected
        && query_outputs
            .iter()
            .any(|item| item.has_cve_match(&file.cve_id))
        && cve_to_filter.contains(&file.cve_id)
}

/// Filter the false negative and considere only if the file cve is in the query output queries
fn is_filtered_false_negative(file: &FileToTest, query_outputs: &[QueryOutput], cve_to_filter : &[String]) -> bool { 
    file.is_infected
        && !query_outputs
            .iter()
            .any(|item| item.has_cve_match(&file.cve_id))
        && cve_to_filter.contains(&file.cve_id)
}

/// Filter the true negative and considere only if the file cve is in the query output queries
fn is_filtered_true_negative(file: &FileToTest, query_outputs: &[QueryOutput], cve_to_filter : &[String]) -> bool { 
    !file.is_infected
        && !query_outputs
            .iter()
            .any(|item| item.has_cve_match(&file.cve_id))
        && cve_to_filter.contains(&file.cve_id)
}



fn is_true_positive(file: &FileToTest, query_outputs: &[QueryOutput]) -> bool {
    file.is_infected
        && query_outputs
            .iter()
            .any(|item| item.has_cve_match(&file.cve_id))
}

fn is_false_positive(file: &FileToTest, query_outputs: &[QueryOutput]) -> bool {
    !file.is_infected
        && query_outputs
            .iter()
            .any(|item| item.has_cve_match(&file.cve_id))
}

fn is_false_negative(file: &FileToTest, query_outputs: &[QueryOutput]) -> bool {
    file.is_infected
        && !query_outputs
            .iter()
            .any(|item| item.has_cve_match(&file.cve_id))
}

fn is_true_negative(file: &FileToTest, query_outputs: &[QueryOutput]) -> bool {
    !file.is_infected
        && !query_outputs
            .iter()
            .any(|item| item.has_cve_match(&file.cve_id))
}