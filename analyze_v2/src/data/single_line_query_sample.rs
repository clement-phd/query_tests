use std::collections::{HashMap, HashSet};

use benchmark_v2::prepared_dataset::FileToTest;
use benchmark_v2::results::file_results::FileResult;
use generate_query::params::argv::Generator;
use plot_helper::data::plottable::key::SerieKey;
use plot_helper::data::plottable::sample::{MultipleSample, Sample};
use plot_helper::data::plottable::{PlottableSamples, PlottableStruct};
use rust_utils::tree_sitter_utils::languages::Language;
use serde::{Deserialize, Serialize};

use crate::params::ANOMALIE_SINGLE_QUERY_TIME_THRESHOLD;

use super::keys::SingleLineQueryKey;






#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct SingleLineQuerySample {
    anomalies_single_query_time_by_cve_id: HashMap<Language, HashMap<String, Vec<(FileToTest, f64)>>>,
    parsing_time: f64,
    query_time: f64,
    query_max_memory: f64,
    file: FileToTest,
    min_query_line_of_code: u64,
    language: Language,
    file_nb_of_lines: u64,
    file_nb_of_chars: u64,
    generator : Generator
}



impl Sample<SingleLineQueryKey> for SingleLineQuerySample {
    fn get_numeric_value(&self, key: &SingleLineQueryKey) -> f32 {
        match key {
            SingleLineQueryKey::ParsingTime => self.parsing_time as f32,
            SingleLineQueryKey::QueryTime => self.query_time as f32,
            SingleLineQueryKey::QueryMaxMemory => self.query_max_memory as f32,
            SingleLineQueryKey::FileNbOfLines => self.file_nb_of_lines as f32,
            SingleLineQueryKey::FileNbOfChars => self.file_nb_of_chars as f32,
            SingleLineQueryKey::MinQueryLineOfCode => self.min_query_line_of_code as f32,
            SingleLineQueryKey::Language => unreachable!("language is not numeric"),
            SingleLineQueryKey::File => unreachable!("file is not numeric"),
            SingleLineQueryKey::Generator => unreachable!("generator is not numeric"),
        }
    }

    fn get_string_value(&self, key: &SingleLineQueryKey) -> String {
        match key {
            SingleLineQueryKey::File => unreachable!("file is not string"),
            SingleLineQueryKey::Language => self.language.to_string(),
            SingleLineQueryKey::ParsingTime => unreachable!("parsing time is not string"),
            SingleLineQueryKey::QueryTime => unreachable!("query time is not string"),
            SingleLineQueryKey::QueryMaxMemory => unreachable!("query max memory is not string"),
            SingleLineQueryKey::MinQueryLineOfCode => unreachable!("min query line of code is not string"),
            SingleLineQueryKey::FileNbOfLines => unreachable!("file nb of lines is not string"),
            SingleLineQueryKey::FileNbOfChars => unreachable!("file nb of chars is not string"),
            SingleLineQueryKey::Generator => self.generator.to_string(),
        }
    }
}



impl MultipleSample<SingleLineQueryKey> for SingleLineQuerySample {
    fn new_from_file_path(file_path : &str) -> Result<Vec<Self>, Box<dyn std::error::Error>> {
        let file_content = std::fs::read_to_string(file_path)?;
        let item : FileResult = serde_json::from_str(&file_content).unwrap();

        
        let mut samples = Vec::new();
        for instance in &item.get_single_query_instances() {
            let mut anomalies_single_query_time_by_cve_id: HashMap<Language, HashMap<String, Vec<(FileToTest, f64)>>> = HashMap::new();
            if instance.query_time > ANOMALIE_SINGLE_QUERY_TIME_THRESHOLD {
                for cve in get_querries_cve_id(&instance.query_content_lines) {
                    anomalies_single_query_time_by_cve_id
                        .entry(item.language.clone())
                        .or_insert(HashMap::new())
                        .entry(cve.clone())
                        .or_insert(Vec::new())
                        .push((item.file.clone(), instance.query_time));
                }
            }
            samples.push(SingleLineQuerySample {
                anomalies_single_query_time_by_cve_id: anomalies_single_query_time_by_cve_id,
                parsing_time: item.parsing_time,
                query_time: instance.query_time,
                query_max_memory: instance.query_max_memory,
                file: item.file.clone(),
                min_query_line_of_code: item.min_query_line_of_code as u64,
                language: item.language.clone(),
                file_nb_of_lines: item.file_nb_of_lines as u64,
                file_nb_of_chars: item.file_nb_of_chars as u64,
                generator: item.generator.clone()
            });
        }
        Ok(samples)
    }


    
}


pub trait MultiLineQueryPlottable<SampleType, KeyType>
where 
    Self: PlottableSamples<SampleType, KeyType>,
    SampleType: MultipleSample<KeyType>,
    KeyType: SerieKey
{
    /// aggregate the anomalies_single_query_time_by_cve_id of each sample
    fn aggregate_anomalies_single_query_time_by_cve_id(&self) -> HashMap<Language, HashMap<String, Vec<(FileToTest, f64)>>>;
}

impl MultiLineQueryPlottable<SingleLineQuerySample, SingleLineQueryKey> for PlottableStruct<SingleLineQuerySample, SingleLineQueryKey> {
    fn aggregate_anomalies_single_query_time_by_cve_id(&self) -> HashMap<Language, HashMap<String, Vec<(FileToTest, f64)>>> {
        let mut result: HashMap<Language, HashMap<String, Vec<(FileToTest, f64)>>> = HashMap::new();
        for sample in self.get_samples() {
            for (language, anomalies) in &sample.anomalies_single_query_time_by_cve_id {
                for (cve_id, files) in anomalies {
                    result
                        .entry(language.clone())
                        .or_insert(HashMap::new())
                        .entry(cve_id.clone())
                        .or_insert(Vec::new())
                        .extend(files.clone());
                }
            }
        }
        result
    }
}


fn get_querries_cve_id(query_content_lines: &Vec<String>) -> HashSet<String> {
    // Create a HashSet to store the unique CVE IDs.
    let mut result: HashSet<String> = HashSet::new();

    for line in query_content_lines {
        // Split the line by "@" and get the last part.
        if let Some(cve_id) = line.split('@').last() {
            result.insert(cve_id.to_string());
        }
    }

    result
}