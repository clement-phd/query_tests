use std::collections::HashMap;

use generate_query::params::argv::Generator;
use plot_helper::data::plottable::sample::Sample;
use plot_helper::data::plottable::{PlottableSamples, PlottableStruct};
use plot_helper::wrapper::correctness_wrapper::CorrectnessStatsWrapper;
use rust_utils::tree_sitter_utils::languages::Language;
use serde::{Deserialize, Serialize};

use super::keys::{CorrectnessQueryLinesKey, MultiLineQueryKey};
use super::multi_line_query_sample::MultiLineQuerySample;



#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct CorrectnessSample {
    language: Language,
    generator: Generator,
    min_query_line_of_code: u64,
    correctness: CorrectnessStatsWrapper
}


impl CorrectnessSample {
    pub fn new(
        language: Language,
        generator: Generator,
        min_query_line_of_code: u64,
        true_positive: u64,
        false_positive: u64,
        false_negative: u64,
        true_negative: u64,
    ) -> CorrectnessSample {
        CorrectnessSample {
            language,
            generator,
            min_query_line_of_code,
            correctness: CorrectnessStatsWrapper::new(
                true_positive,
                false_positive,
                false_negative,
                true_negative,
            ),
        }
    }

    pub fn from_multiline_plottable(
        plottable : &PlottableStruct<MultiLineQuerySample, MultiLineQueryKey>,
        true_positive_key: MultiLineQueryKey,
        false_positive_key: MultiLineQueryKey,
        false_negative_key: MultiLineQueryKey,
        true_negative_key: MultiLineQueryKey,
        generator: Generator,
    ) -> Vec<CorrectnessSample> {
        let mut true_positive_by_min_query_line_of_code: HashMap<(u32, Generator, Language), u32> = HashMap::new();
        let mut false_positive_by_min_query_line_of_code: HashMap<(u32, Generator, Language), u32> = HashMap::new();
        let mut false_negative_by_min_query_line_of_code: HashMap<(u32, Generator, Language), u32> = HashMap::new();
        let mut true_negative_by_min_query_line_of_code: HashMap<(u32, Generator, Language), u32> = HashMap::new();
        for sample in plottable.get_samples() {
            if sample.get_generator() != &generator { // skip the generator => we want to compare only the same generator
                continue;
            }
            if sample.get_numeric_value(&false_negative_key) == 1.0 {
                *false_negative_by_min_query_line_of_code.entry(
                    (sample.get_numeric_value(&MultiLineQueryKey::MinQueryLineOfCode) as u32, 
                            sample.get_generator().clone(),
                            sample.get_language().clone())
                    ).or_insert(0) += 1;
            } else if sample.get_numeric_value(&false_positive_key) == 1.0 {
                *false_positive_by_min_query_line_of_code.entry(
                    (sample.get_numeric_value(&MultiLineQueryKey::MinQueryLineOfCode) as u32, 
                            sample.get_generator().clone(),
                            sample.get_language().clone())
                    ).or_insert(0) += 1;
            } else if sample.get_numeric_value(&true_positive_key) == 1.0 {
                *true_positive_by_min_query_line_of_code.entry(
                    (sample.get_numeric_value(&MultiLineQueryKey::MinQueryLineOfCode) as u32, 
                            sample.get_generator().clone(),
                            sample.get_language().clone())
                    ).or_insert(0) += 1;
            } else if sample.get_numeric_value(&true_negative_key) == 1.0 {
                *true_negative_by_min_query_line_of_code.entry(
                    (sample.get_numeric_value(&MultiLineQueryKey::MinQueryLineOfCode) as u32, 
                            sample.get_generator().clone()
                            ,sample.get_language().clone())
                    ).or_insert(0) += 1;
            }
        }
        let mut result = Vec::new();
        let mut keys = true_positive_by_min_query_line_of_code.keys().collect::<Vec<&(u32, Generator, Language)>>();
        keys.sort();

        for key in keys {
            let true_positive = *true_positive_by_min_query_line_of_code.get(key).unwrap_or(&0);
            let false_positive = *false_positive_by_min_query_line_of_code.get(key).unwrap_or(&0);
            let false_negative = *false_negative_by_min_query_line_of_code.get(key).unwrap_or(&0);
            let true_negative = *true_negative_by_min_query_line_of_code.get(key).unwrap_or(&0);
            result.push(CorrectnessSample::new(
                key.2.clone(),
                key.1.clone(),
                key.0 as u64,
                true_positive as u64,
                false_positive as u64,
                false_negative as u64,
                true_negative as u64,
            ));
        }
        result
    }
    
    pub fn get_correctness(&self) -> &CorrectnessStatsWrapper {
        &self.correctness
    }
}


impl Sample<CorrectnessQueryLinesKey> for CorrectnessSample {
    fn get_numeric_value(&self, key: &CorrectnessQueryLinesKey) -> f32 {
        match key {
            CorrectnessQueryLinesKey::Language => unreachable!("language is not numeric"),
            CorrectnessQueryLinesKey::Generator => unreachable!("generator is not numeric"),
            CorrectnessQueryLinesKey::MinQueryLineOfCode => self.min_query_line_of_code as f32,
            CorrectnessQueryLinesKey::TruePositive => self.correctness.get_true_positive() as f32,
            CorrectnessQueryLinesKey::FalsePositive => self.correctness.get_false_positive() as f32,
            CorrectnessQueryLinesKey::FalseNegative => self.correctness.get_false_negative() as f32,
            CorrectnessQueryLinesKey::TrueNegative => self.correctness.get_true_negative() as f32,
            CorrectnessQueryLinesKey::Recall => self.correctness.get_recall() as f32,
            CorrectnessQueryLinesKey::Precision => self.correctness.get_precision() as f32,
            CorrectnessQueryLinesKey::F1 => self.correctness.get_f1() as f32,
            CorrectnessQueryLinesKey::Accuracy => self.correctness.get_accuracy() as f32,
        }
    }

    fn get_string_value(&self, key: &CorrectnessQueryLinesKey) -> String {
        match key {
            CorrectnessQueryLinesKey::Language => self.language.to_string(),
            CorrectnessQueryLinesKey::Generator => self.generator.to_string(),
            CorrectnessQueryLinesKey::MinQueryLineOfCode => unreachable!("min query line of code is not string"),
            CorrectnessQueryLinesKey::TruePositive => unreachable!("true positive is not string"),
            CorrectnessQueryLinesKey::FalsePositive => unreachable!("false positive is not string"),
            CorrectnessQueryLinesKey::FalseNegative => unreachable!("false negative is not string"),
            CorrectnessQueryLinesKey::TrueNegative => unreachable!("true negative is not string"),
            CorrectnessQueryLinesKey::Recall => unreachable!("recall is not string"),
            CorrectnessQueryLinesKey::Precision => unreachable!("precision is not string"),
            CorrectnessQueryLinesKey::F1 => unreachable!("f1 is not string"),
            CorrectnessQueryLinesKey::Accuracy => unreachable!("accuracy is not string"),
        }
    }
}