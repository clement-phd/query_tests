import os
import json
import sys

def process_json_file(file_path : str):
    with open(file_path, 'r') as file:
        json_data = json.load(file)

    # Convert JSON back to string removing unnecessary whitespace
    modified_json = json.dumps(json_data, indent=None)

    # Write modified JSON back to file
    with open(file_path, 'w') as file:
        file.write(modified_json)

def process_directory(directory : str):
    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.endswith('.json'):
                file_path = os.path.join(root, file)
                process_json_file(file_path)
                print(f"Processed: {file_path}")

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python script.py <directory>")
        sys.exit(1)

    directory = sys.argv[1]
    if not os.path.isdir(directory):
        print("Error: Directory does not exist.")
        sys.exit(1)

    process_directory(directory)
    print("JSON files processed successfully.")
