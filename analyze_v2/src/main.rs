use std::fs;
use std::path::Path;

use generate_query::params::argv::Generator;
use log::info;
use plot_helper::data::plottable::key::SerieKey;
use plot_helper::data::plottable::sample::Sample;
use plot_helper::data::plottable::{Plottable, PlottableMultipleSamplesFromPaths, PlottableSamples, PlottableSimpleSamplesFromPaths, PlottableStruct};
use plot_helper::static_html::presentation_data::Ir;
use rust_utils::logger::common_logger::init_logger;

use crate::data::correctness_sample::CorrectnessSample;
use crate::data::dataset_sample::load_plottable_dataset;
use crate::data::get_file_data_path;
use crate::data::hunk_serie::load_plottable_hunk;
use crate::data::keys::{CorrectnessQueryLinesKey, MemoryMonitoringSerieKey, MultiLineQueryKey, SingleLineQueryKey};
use crate::data::memory_sample::MemorySample;
use crate::data::multi_line_query_sample::MultiLineQuerySample;
use crate::data::repo_sample::load_plottable_repo;
use crate::data::single_line_query_sample::SingleLineQuerySample;
use crate::graph_def::correctness::gen_correctness_data;
use crate::graph_def::dataset::gen_dataset_data;
use crate::graph_def::hunk::gen_hunk_data;
use crate::graph_def::memory::gen_memory_data;
use crate::graph_def::multi_line_query::gen_multi_line_query_data;
use crate::graph_def::repo::gen_repo_data;
use crate::graph_def::single_line_query::gen_single_line_query_data;
use crate::params::argv::get_program_args;
use crate::params::MEMORY_BENCHMARK_FILE;



mod params;
mod data;
mod graph_def;

fn save_data<T>(data : &T, output_dir : &str, name : &str) -> Result<(), Box<dyn std::error::Error>> where T : serde::Serialize {
    let data_serialized_path_o = Path::new(output_dir).join(name);
    serde_json::to_writer(fs::File::create(data_serialized_path_o.to_str().unwrap())?, &data)?;

    //let data_serialized_prety_path_o = Path::new(output_dir).join(format!("pretty_{}", name));
    //serde_json::to_writer_pretty(fs::File::create(data_serialized_prety_path_o.to_str().unwrap())?, &data)?;

    info!("Data saved at {}.", data_serialized_path_o.to_str().unwrap());

    Ok(())
}

fn load_and_compute_data<SampleT, Key>(
    data : &PlottableStruct<SampleT, Key>,
    data_dir : &str,
    data_name : &str,
    compute : impl FnOnce(&PlottableStruct<SampleT, Key>, &str) -> Result<(), Box<dyn std::error::Error>>,
    is_save_data : bool
) -> Result<(), Box<dyn std::error::Error>> 
where 
SampleT : Sample<Key> + serde::Serialize,
Key : SerieKey  
{
    info!("{} data loaded ({} samples).", data_name, data.get_number_of_samples());
    
    if data.get_number_of_samples() != 0 {
        if is_save_data {
            save_data(data, data_dir, &format!("{}_data.json", data_name))?;
        }
        compute(data, data_dir)?;
        info!("{} graphs generated.", data_name);
    } else {
        info!("No {} data found.", data_name);
    }

    Ok(())
}

/// wrapper for the correctness
fn load_and_compute_correctness_data(
    data: &PlottableStruct<CorrectnessSample, CorrectnessQueryLinesKey>,
    data_dir: &str,
    data_name: &str,
    generator: &Generator,
    is_save_data: bool,
) -> Result<(), Box<dyn std::error::Error>>
{
    load_and_compute_data(
        data,
        data_dir,
        &format!("{}_{}_correctness", generator.to_string(), data_name),
        |data, data_dir| {
            gen_correctness_data(data, data_dir, generator.to_string().as_str(), data_name)
        },
        is_save_data,
    )
}

fn main() -> Result<(), Box<dyn std::error::Error>>  {
    dotenv::dotenv().ok();
    init_logger();
    let argv = get_program_args();
    info!("🚀 Start analyze_v2.");
    let data_output_folder_path = Path::new(&argv.output_dir).join("data");


    if let Some(data_dir) = &argv.action.extracted_data_dir {        
        if data_output_folder_path.exists() {
            fs::remove_dir_all(data_output_folder_path.to_str().unwrap())?;
        }
        fs::create_dir_all(data_output_folder_path.to_str().unwrap())?;
        // memory data
        {
            let memory_data = PlottableStruct::<MemorySample, MemoryMonitoringSerieKey>::new_async_from_paths(&vec![MEMORY_BENCHMARK_FILE.to_string()]);
            load_and_compute_data(&memory_data, data_output_folder_path.to_str().unwrap(), "Memory", gen_memory_data, argv.save_data)?;
        }
        // dataset data
        {
            let dataset_data = load_plottable_dataset(&data_dir);
            load_and_compute_data(&dataset_data, data_output_folder_path.to_str().unwrap(), "Dataset", gen_dataset_data, argv.save_data)?;
        }
        // hunk data
        {
            let hunk_data = load_plottable_hunk(data_dir);
            load_and_compute_data(&hunk_data, data_output_folder_path.to_str().unwrap(), "Hunk", gen_hunk_data, argv.save_data)?;
        }
        
        {
            let file_data = get_file_data_path(&data_dir);
            if file_data.len() != 0 {
                    
                // multiline query data
                {
                    let multi_line_query = 
                        PlottableStruct::<MultiLineQuerySample, MultiLineQueryKey>::new_async_from_paths(&file_data);
                    load_and_compute_data(&multi_line_query, data_output_folder_path.to_str().unwrap(), "MultiLineQuery", gen_multi_line_query_data, argv.save_data)?;
                    // correctness data
                    let mut sorted_generators: Vec<_> = 
                        multi_line_query.get_string_series(&MultiLineQueryKey::Generator).into_iter()
                            .collect::<std::collections::HashSet<_>>().into_iter().collect();
                    sorted_generators.sort();
                    for generator in sorted_generators {
                        info!("correctness data for Generator: {}", generator);
                        {
                            let correctness = 
                                PlottableStruct::<CorrectnessSample, CorrectnessQueryLinesKey>::new(
                                    CorrectnessSample::from_multiline_plottable(
                                        &multi_line_query,
                                        MultiLineQueryKey::TruePositive,
                                        MultiLineQueryKey::FalsePositive,
                                        MultiLineQueryKey::FalseNegative,
                                        MultiLineQueryKey::TrueNegative,
                                        Generator::from_string(generator.as_str()).unwrap()
                                    ),
                                );
                            load_and_compute_correctness_data(
                                &correctness, 
                                data_output_folder_path.to_str().unwrap(), 
                                "with_query_error", 
                                &Generator::from_string(generator.as_str()).unwrap(), 
                                argv.save_data
                            )?;
                            
                        }
                    

                        // filtered correctness
                        {
                            let filtered_correctness = 
                                PlottableStruct::<CorrectnessSample, CorrectnessQueryLinesKey>::new(
                                    CorrectnessSample::from_multiline_plottable(
                                        &multi_line_query,
                                        MultiLineQueryKey::FilteredTruePositive,
                                        MultiLineQueryKey::FilteredFalsePositive,
                                        MultiLineQueryKey::FilteredFalseNegative,
                                        MultiLineQueryKey::FilteredTrueNegative,
                                        Generator::from_string(generator.as_str()).unwrap()
                                    )
                                );
                            load_and_compute_correctness_data(
                                &filtered_correctness, 
                                data_output_folder_path.to_str().unwrap(), 
                                "with_query_error_filtered", 
                                &Generator::from_string(generator.as_str()).unwrap(), 
                                argv.save_data
                            )?;
                        }

                        // non biased
                        {
                            let non_biased_correctness = 
                                PlottableStruct::<CorrectnessSample, CorrectnessQueryLinesKey>::new(
                                    CorrectnessSample::from_multiline_plottable(
                                        &multi_line_query,
                                        MultiLineQueryKey::NonBiasedTruePositive,
                                        MultiLineQueryKey::NonBiasedFalsePositive,
                                        MultiLineQueryKey::NonBiasedFalseNegative,
                                        MultiLineQueryKey::NonBiasedTrueNegative,
                                        Generator::from_string(generator.as_str()).unwrap()
                                    )
                                );
                            load_and_compute_correctness_data(
                                &non_biased_correctness, 
                                data_output_folder_path.to_str().unwrap(), 
                                "without_query_error_non_biased", 
                                &Generator::from_string(generator.as_str()).unwrap(), 
                                argv.save_data
                            )?;
                        }
                    }
                }
                // singleline query data
                {
                    let single_line_query = 
                        PlottableStruct::<SingleLineQuerySample, SingleLineQueryKey>::new_async_from_paths(&file_data);
                    load_and_compute_data(&single_line_query, data_output_folder_path.to_str().unwrap(), "SingleLineQuery", gen_single_line_query_data, argv.save_data)?;
                }
            } else {
                info!("No file data found.")
            }            
        }
        // repo data
        {
            let commit_data = load_plottable_repo(&data_dir);
            load_and_compute_data(&commit_data, data_output_folder_path.to_str().unwrap(), "Repo", gen_repo_data, argv.save_data)?;
        }
    }
    
    let html_output_path = Path::new(&argv.output_dir).join("index.html");
    let html_ir = Ir::new_from_file_system(&data_output_folder_path.to_str().unwrap())?;
    info!("Ir load.");
    let html = html_ir.to_html()?;
    info!("Html generated.");
    fs::write(html_output_path.to_str().unwrap(), html)?;

    info!("Static html generated.");

    Ok(())
}