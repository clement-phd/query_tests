import os
import subprocess
import sys
import time
import psutil
import datetime
import argparse

INTERVAL = 0.01
OUTPUT = "benchmark_memory.txt"

def benchmark_memory(pid : int):
    # Run the program
    
    while True:
        try:
            p = psutil.Process(pid)
            rss = p.memory_info().rss
            vms = p.memory_info().vms
            now = datetime.datetime.now()
            now_formatted = now.strftime("%Y-%m-%d %H:%M:%S.%f")
            mem_status = f"{now_formatted} - RSS: {rss}, VMS: {vms}"
            with open(OUTPUT, "a") as f:
                f.write(mem_status + "\n")
            time.sleep(INTERVAL)
            #print(mem_status)
        except psutil.NoSuchProcess:
            print(f"Process with PID {pid} does not exist.")
            break

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Benchmark the memory usage of a program')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-p', metavar='prog', type=str, help='Program and its arguments as a string')
    group.add_argument('-i', metavar='pid', type=int, help='PID of the running process')

    args = parser.parse_args()

    if os.path.exists(OUTPUT):
        print(f"File {OUTPUT} already exists. Please remove it before running the benchmark.")
        sys.exit(1)

    if args.p:
        program = args.p.split()
        p = subprocess.Popen(program, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        pid = p.pid
    else:
        pid = args.i
    
    benchmark_memory(pid)