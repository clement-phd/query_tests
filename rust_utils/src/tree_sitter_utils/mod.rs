use std::fs;
use std::path::Path;
use tree_sitter::{Parser, Tree};

use self::languages::Language;
pub mod languages;
pub mod query_wraper;

pub fn get_tree_from_file_content(file: &str, language : &Language) -> Tree {
    let mut parser = Parser::new();
    parser.set_language(language.get_tree_sitter_language()).expect("Error loading Rust grammar");

    let tree = parser.parse(file, None).expect("Error loading the tree.");

    return tree;
}

/// get a tree from a file path
/// if the language is not specified, it will be guessed from the file name
pub fn get_tree_from_file_path(file_path: &str, language : Option<&Language>) -> Tree {
    let source_code = fs::read_to_string(file_path).expect("Error loading the file.");
    let file_name = Path::new(file_path).file_name().unwrap().to_str().unwrap();

    let language = match language {
        Some(language) => language.clone(),
        None => Language::new_by_file_name(file_name).unwrap()
    };


    return get_tree_from_file_content(source_code.as_str(), &language);
}
