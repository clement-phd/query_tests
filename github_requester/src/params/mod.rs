


pub const GITHUB_API_URL: &str = "https://api.github.com";

pub(crate) const MIN_TIME_BETWEEN_REQUESTS: f64 = 0.1;