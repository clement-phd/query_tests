use clap::Parser;

/// Benchmark the query
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub struct Argv {
    /// clean the folder where the results are stored
    /// WARNING: this will delete all the results
    #[arg(long, action)]
    pub clean_results: bool,

    /// clean the dataset folder
    #[arg(long, action)]
    pub clean_dataset: bool,

    /// the dataset path to use
    #[arg(short, long)]
    pub dataset_path : String,
}



pub fn get_program_args() -> Argv {
    return Argv::parse();
}