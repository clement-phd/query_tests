use std::fs;
use std::path::{Path, PathBuf};
use log::error;
use walkdir::WalkDir;



/// Read a file to a string, if the file does not exist, return an empty string
/// If the file exists but cannot be read, panic
/// If the file content is not valid utf8, replace invalid characters with the replacement character
pub fn read_file_to_string_lossy(file_path: PathBuf) -> String {
    if file_path.exists() {
        match fs::read(&file_path) {
            Ok(bytes) => String::from_utf8_lossy(&bytes).to_string(),
            Err(err) => {
                error!("Error while reading the file {}: {}", file_path.to_string_lossy(), err);
                panic!("Failed to read file.");
            }
        }
    } else {
        String::new()
    }
}

/// Check if a string is only whitespace
pub fn is_only_whitespace(s: &str) -> bool {
    s.chars().all(|c| c.is_whitespace())
}

/// get all files in a directory, including files in subdirectory
pub fn get_all_files(root: &str) -> Vec<String> {
    let mut file_paths = Vec::new();

    for entry in WalkDir::new(root)
        .into_iter()
        .filter_map(|e| e.ok()) // Here, filter out any Err values and unwrap
        .filter(|e| e.file_type().is_file()) // Only consider files
    {
        if let Some(path_str) = entry.path().to_str() {
            file_paths.push(path_str.to_string());
        }
    }

    file_paths
}

/// from a root absolute path and a file absolute path, return the relative path from the root to the file
/// NOTE : the root and file path must be absolute
/// NOTE : The root path must be a directory
pub fn get_relative_path(root_path: &Path, file_path: &Path) -> String {
    let root_components: Vec<&str> = root_path.components().map(|c| c.as_os_str().to_str().unwrap()).collect();
    let file_components: Vec<&str> = file_path.components().map(|c| c.as_os_str().to_str().unwrap()).collect();

    let mut common_prefix = PathBuf::new();
    for (root_component, file_component) in root_components.iter().zip(file_components.iter()) {
        if root_component == file_component {
            common_prefix.push(root_component);
        } else {
            break;
        }
    }
    let common_prefix_length = common_prefix.components().count();
    let mut relative_path = String::new();
    for _ in 0..root_components.len() - common_prefix_length {
        relative_path.push_str("../");
    }

    let file_without_common_prefix = file_components[common_prefix_length..].join("/");
    relative_path.push_str(&file_without_common_prefix);

    


    relative_path
}



#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_is_only_whitespace() {
        assert_eq!(is_only_whitespace("   "), true);
        assert_eq!(is_only_whitespace("   a"), false);
        assert_eq!(is_only_whitespace("a"), false);
        assert_eq!(is_only_whitespace(""), true);
    }




    #[test]
    fn test_get_relative_path() {
        let root = Path::new("/home/user");
        let file = Path::new("/home/user/file.txt");
        assert_eq!(get_relative_path(root, file), "file.txt");

        let root = Path::new("/home/user");
        let file = Path::new("/home/user/dir/file.txt");
        assert_eq!(get_relative_path(root, file), "dir/file.txt");

        let root = Path::new("/home/user/test");
        let file = Path::new("/home/user/dir1/dir2/file.txt");
        assert_eq!(get_relative_path(root, file), "../dir1/dir2/file.txt");

        let root = Path::new("/home/user/test");
        let file = Path::new("/home/tmp/to_file/file.txt");
        assert_eq!(get_relative_path(root, file), "../../tmp/to_file/file.txt");
    }
}