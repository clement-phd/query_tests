use std::fs;
use std::path::Path;

use plot_helper::data::filtering::Filter;
use plot_helper::data::plot_data::Layout;
use plot_helper::data::plottable::{Plottable, PlottableStruct};
use plot_helper::stats::stats_serie::MetricName;
use rust_utils::tree_sitter_utils::languages::Language;
use crate::data::keys::MultiLineQueryKey;
use crate::data::multi_line_query_sample::MultiLineQuerySample;

use super::{plot_wrapper, QueryStatsWrapper};








pub fn gen_multi_line_query_data(data : &PlottableStruct<MultiLineQuerySample, MultiLineQueryKey>, output_dir_path : &str) -> Result<(), Box<dyn std::error::Error>> {
    let folder_path_o = Path::new(output_dir_path).join("multi_line_query");
    fs::create_dir_all(folder_path_o.to_str().unwrap())?;

    // separate on generator
    let mut sorted_generators: Vec<_> = 
        data.get_string_series(&MultiLineQueryKey::Generator).into_iter()
            .collect::<std::collections::HashSet<_>>().into_iter().collect();
    sorted_generators.sort();
    
    for generator in sorted_generators {
        let folder_path_o = folder_path_o.join(generator.as_str());
        fs::create_dir_all(folder_path_o.to_str().unwrap())?;
        let generator_filter = Filter::new_str(
            MultiLineQueryKey::Generator, 
            move |g : &str| g == generator.as_str()
        );
        // performance comparaison language
        {
            let save_folder_path_o = folder_path_o.join("language_comparaison");
            fs::create_dir_all(save_folder_path_o.to_str().unwrap())?;
            {
                plot_wrapper(
                    data,
                    Some(MultiLineQueryKey::Language),
                    save_folder_path_o.to_str().unwrap(),
                    "performance_comparaison_language.png",
                    &Layout::new(1, 4),
                    vec![
                        (MultiLineQueryKey::FileNbOfLines, MultiLineQueryKey::QueryTime, Some(vec![&generator_filter])),
                        //(MultiLineQueryKey::FileNbOfLines, MultiLineQueryKey::QueryMaxMemory, None),
                        (MultiLineQueryKey::FileNbOfChars, MultiLineQueryKey::QueryTime, Some(vec![&generator_filter])),
                        //(MultiLineQueryKey::FileNbOfChars, MultiLineQueryKey::QueryMaxMemory, None),
                        (MultiLineQueryKey::QueryNbOfLines, MultiLineQueryKey::QueryTime, Some(vec![&generator_filter])),
                        //(MultiLineQueryKey::QueryNbOfLines, MultiLineQueryKey::QueryMaxMemory, None),
                        (MultiLineQueryKey::MinQueryLineOfCode, MultiLineQueryKey::QueryTime, Some(vec![&generator_filter])),
                        //(MultiLineQueryKey::MinQueryLineOfCode, MultiLineQueryKey::QueryMaxMemory, None),
                    ],
                    vec![MultiLineQueryKey::QueryTime]
                )?;
            }
        }
        // comparaison of performances for each language
        {
            let save_folder_path_o = folder_path_o.join("time_performance_comparaison_by_language");
            fs::create_dir_all(save_folder_path_o.to_str().unwrap())?;
            {
                
                for language in Language::get_all_languages() {
                    let language_folder_path_o = save_folder_path_o.join(language.to_string());
                    fs::create_dir_all(language_folder_path_o.to_str().unwrap())?;
                    let save_file_name = format!("performance_{}.png", language);
                    let language_filter = Filter::new_str(
                        MultiLineQueryKey::Language, 
                        move |l : &str| l == language.to_string()
                    );

                    plot_wrapper(
                        data,
                        Some(MultiLineQueryKey::QueryNbOfLines),
                        language_folder_path_o.to_str().unwrap(),
                        save_file_name.as_str(),
                        &Layout::new(1, 1),
                        vec![
                            (MultiLineQueryKey::FileNbOfLines, MultiLineQueryKey::QueryTime, Some(vec![&language_filter, &generator_filter])),
                            //(MultiLineQueryKey::FileNbOfLines, MultiLineQueryKey::QueryMaxMemory, Some(vec![&language_filter])),
                        ],
                        vec![MultiLineQueryKey::QueryTime]
                    )?;
                }
            }
        }

        // comparaison with single
        {
            let save_folder_path_o = folder_path_o.join("comparaison_with_single_line_query");
            fs::create_dir_all(save_folder_path_o.to_str().unwrap())?;
            {
                plot_wrapper(
                    data,
                    Some(MultiLineQueryKey::Language),
                    save_folder_path_o.to_str().unwrap(),
                    "comparaison_with_single_line_query.png",
                    &Layout::new(1, 3),
                    vec![
                        (MultiLineQueryKey::FileNbOfLines, MultiLineQueryKey::TimeDifferenceSingleMultiLineQuery, Some(vec![&generator_filter])),
                        (MultiLineQueryKey::FileNbOfChars, MultiLineQueryKey::TimeDifferenceSingleMultiLineQuery, Some(vec![&generator_filter])),
                        (MultiLineQueryKey::MinQueryLineOfCode, MultiLineQueryKey::TimeDifferenceSingleMultiLineQuery, Some(vec![&generator_filter])),
                    ],
                    vec![MultiLineQueryKey::TimeDifferenceSingleMultiLineQuery]
                )?;
            }
        }
    } // end of the generator loop

    // outlier and language informations
    {
        let save_folder_path_o = folder_path_o.join("detailed_language_informations");
        fs::create_dir_all(save_folder_path_o.to_str().unwrap())?;
        {
            let stats = data.collect_stats_sorted_by_unique_values( 
                &vec![
                    MultiLineQueryKey::QueryTime,
                    MultiLineQueryKey::QueryMaxMemory,
                    MultiLineQueryKey::ParsingTime,
                    MultiLineQueryKey::FileNbOfLines,
                ],
                &MultiLineQueryKey::Language
            );

            let mut stats_stored : Vec<QueryStatsWrapper> = Vec::new();

            for (language, computed_stats) in stats {
                stats_stored.push(
                    QueryStatsWrapper::new (
                        language.clone(),
                        computed_stats.get(&MultiLineQueryKey::ParsingTime).unwrap().stats.get(&MetricName::Mean).unwrap().value,
                        computed_stats.get(&MultiLineQueryKey::ParsingTime).unwrap().stats.get(&MetricName::Median).unwrap().value,
                        computed_stats.get(&MultiLineQueryKey::QueryTime).unwrap().stats.get(&MetricName::Mean).unwrap().value,
                        computed_stats.get(&MultiLineQueryKey::QueryTime).unwrap().stats.get(&MetricName::Median).unwrap().value,
                        computed_stats.get(&MultiLineQueryKey::FileNbOfLines).unwrap().stats.get(&MetricName::Mean).unwrap().value,
                    )
                )
            }

            let file_path = save_folder_path_o.join(format!("stats.csv"));
            let mut csv_writer = csv::Writer::from_path(file_path.to_str().unwrap())?;
            for stat in stats_stored {
                csv_writer.serialize(stat)?;
            }

            csv_writer.flush()?;
        }
    }
    

    // comparaison of generator generator
    {
        let save_folder_path_o = folder_path_o.join("generator_comparaison");
        fs::create_dir_all(save_folder_path_o.to_str().unwrap())?;
        {
            plot_wrapper(
                data,
                Some(MultiLineQueryKey::Generator),
                save_folder_path_o.to_str().unwrap(),
                "generator_comparaison.png",
                &Layout::new(1, 3),
                vec![
                    (MultiLineQueryKey::FileNbOfLines, MultiLineQueryKey::QueryTime, None),
                    (MultiLineQueryKey::FileNbOfChars, MultiLineQueryKey::QueryTime, None),
                    (MultiLineQueryKey::QueryNbOfLines, MultiLineQueryKey::QueryTime, None),
                ],
                vec![MultiLineQueryKey::QueryTime]
            )?;
        }

    }   


    Ok(())
}