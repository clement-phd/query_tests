use std::fmt::{Display, Formatter, self};
use std::io;

use log::error;

use self::repo_error::RepoError;

pub mod repo_error;


/// all the error
#[derive(Clone, PartialEq, Eq, Hash, Debug)]
pub enum BenchmarkError {
    /// the file has not a yielded a query
    /// FileNotYieldQuery(file_path)
    FileNotYieldQuery(String),
    RepoEror(RepoError),

    IO(String),
}

impl BenchmarkError {
    pub fn log(&self) {
        match self {
            BenchmarkError::FileNotYieldQuery(s) => error!("The file {} has not yielded a query", s),
            BenchmarkError::RepoEror(e) => e.log(),
            BenchmarkError::IO(s) => error!("{}", s),
        }
    }
}

impl From<RepoError> for BenchmarkError {
    fn from(error: RepoError) -> Self {
        BenchmarkError::RepoEror(error)
    }
}

impl From<io::Error> for BenchmarkError {
    fn from(error: io::Error) -> Self {
        BenchmarkError::IO(error.to_string())
    }
}



impl Display for BenchmarkError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            BenchmarkError::FileNotYieldQuery(s) => write!(f, "The file {} has not yielded a query", s),
            BenchmarkError::RepoEror(e) => write!(f, "{}", e),
            BenchmarkError::IO(s) => write!(f, "IO error: {}", s),
        }
    }
    
}