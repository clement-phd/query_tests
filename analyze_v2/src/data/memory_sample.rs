use std::error::Error;
use std::fs::File;
use std::io::{BufRead, BufReader};

use chrono::NaiveDateTime;
use log::warn;
use plot_helper::data::plottable::sample::{MultipleSample, Sample};
use regex::Regex;
use serde::{Deserialize, Serialize};

use crate::params::MEMORY_MIN_TIME_INTERVAL;

use super::keys::MemoryMonitoringSerieKey;

const TIMESTAMP_FORMAT : &str = "%Y-%m-%d %H:%M:%S%.f";


#[derive(Clone, Serialize, Deserialize, Debug)]
pub enum MemoryType {
    Rss,
    Vms,
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct MemorySample {
    time : u64,
    memory : u64,
    memory_type : MemoryType,
}


impl MemorySample {
    fn new_from_line(line : &str) -> Option<(MemorySample, MemorySample)> {
        let (timestamp, rss, vms) = MemorySample::parse_line(line)?;
        

        let timestamp_parsed;
        let rss_parsed;
        let vms_parsed;
        if let Ok(parsed) = NaiveDateTime::parse_from_str(timestamp.as_str(), TIMESTAMP_FORMAT) {
            timestamp_parsed = parsed.timestamp_millis() as u64;
        } else {
            warn!("timestamp {} does not match the expected format", timestamp);
            return None;
        }
        if let Ok(parsed) = rss.parse::<u64>() {
            rss_parsed = parsed;
        } else {
            warn!("rss {} does not match the expected format", rss);
            return None;
        }
        if let Ok(parsed) = vms.parse::<u64>() {
            vms_parsed = parsed;
        } else {
            warn!("vms {} does not match the expected format", vms);
            return None;
        }

        Some((
            MemorySample {
                time : timestamp_parsed,
                memory : rss_parsed,
                memory_type : MemoryType::Rss,
            },
            MemorySample {
                time : timestamp_parsed,
                memory : vms_parsed,
                memory_type : MemoryType::Vms,
            }
        ))
    }

    /// return a tuple (timestamp, rss_memory, vms_memory)
    fn parse_line(line : &str) -> Option<(String, String, String)> {
        let re = Regex::new(r"(.+) - RSS: (\d+), VMS: (\d+)").unwrap();
        let timestamp;
        let rss;
        let vms;
        if let Some(captures) = re.captures(line) {
            timestamp = captures.get(1).map_or("", |m| m.as_str());
            rss = captures.get(2).map_or("", |m| m.as_str());
            vms = captures.get(3).map_or("", |m| m.as_str());
        } else {
            warn!("line {} does not match the expected format", line);
            return None;
        }

        Some((timestamp.to_string(), rss.to_string(), vms.to_string()))
    }
}

impl Sample<MemoryMonitoringSerieKey> for MemorySample {

    fn get_numeric_value(&self, key : &MemoryMonitoringSerieKey) -> f32 {
        match key {
            MemoryMonitoringSerieKey::Time => (self.time as f32)/1000.0, // in seconds
            MemoryMonitoringSerieKey::Memory => self.memory as f32,
            MemoryMonitoringSerieKey::MemoryType => unreachable!("memory type is not numeric"),
        }
    }

    fn get_string_value(&self, key : &MemoryMonitoringSerieKey) -> String {
        match key {
            MemoryMonitoringSerieKey::Time => unreachable!("time is not string"),
            MemoryMonitoringSerieKey::Memory => unreachable!("memory is not string"),
            MemoryMonitoringSerieKey::MemoryType => format!("{:?}", self.memory_type),
        }
    }

    

}

impl MultipleSample<MemoryMonitoringSerieKey> for MemorySample {
    /// Load a MemoryMonitoringSerie from a file content ge,erated by the file benchmark_v2/python_src/benchmark_memory.py
    /// and aggregate the result by fixed time interval (MEMORY_MIN_TIME_INTERVAL)
    fn new_from_file_path(file_path : &str) -> Result<Vec<Self>, Box<dyn Error>> {
        let file = File::open(file_path)?;
        let reader = BufReader::with_capacity(1024 * 1024 * 1024 * 10, file);
        //let reader = fs::read_to_string(file_path)?;

        let mut current_interval_start = None;
        let mut starting_time = None;
        let mut interval_rss = 0;
        let mut interval_vms = 0;
        let mut interval_count = 0;
        
        let mut samples = Vec::new();
    
        for line in reader.lines() {
            let line = line?;
            
            let point = MemorySample::new_from_line(&line);
            if point.is_none() {
                continue;
            }
            let point = point.unwrap();

            if starting_time.is_none() {
                starting_time = Some(point.0.time);
            }
    
            if let Some(start) = current_interval_start {
                if point.0.time - start > MEMORY_MIN_TIME_INTERVAL {
                    samples.push(MemorySample {
                        time : start - starting_time.unwrap(),
                        memory : interval_rss / interval_count,
                        memory_type : MemoryType::Rss,
                    });

                    samples.push(MemorySample {
                        time : start - starting_time.unwrap(),
                        memory : interval_vms / interval_count,
                        memory_type : MemoryType::Vms,
                    });
    
                    // Reset interval variables
                    current_interval_start = Some(point.0.time);
                    interval_rss = point.0.memory;
                    interval_vms = point.1.memory;
                    interval_count = 1;
                } else {
                    interval_rss += point.0.memory;
                    interval_vms += point.1.memory;
                    interval_count += 1;
                }
            } else {
                current_interval_start = Some(point.0.time);
                interval_rss = point.0.memory;
                interval_vms = point.1.memory;
                interval_count = 1;
            }
        }
    
        // Push the last interval
        if let Some(start) = current_interval_start {
            samples.push(MemorySample {
                time : start - starting_time.unwrap(),
                memory : interval_rss / interval_count,
                memory_type : MemoryType::Rss,
            });

            samples.push(MemorySample {
                time : start - starting_time.unwrap(),
                memory : interval_vms / interval_count,
                memory_type : MemoryType::Vms,
            });
        }

        Ok(samples)    
    }
}



#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_line_parsing1() {
        let line = "2021-07-01 14:00:00.000 - RSS: 100, VMS: 200";
        let point = MemorySample::new_from_line(line);
        assert!(point.is_some());
        let point = point.unwrap();
        let timestamp = NaiveDateTime::parse_from_str("2021-07-01 14:00:00.000", TIMESTAMP_FORMAT).unwrap();
        assert_eq!(point.0.time, timestamp.timestamp_millis() as u64);
        assert_eq!(point.1.time, timestamp.timestamp_millis() as u64);
        assert_eq!(point.0.memory, 100);
        assert_eq!(point.1.memory, 200);
    }
}