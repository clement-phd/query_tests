#!/bin/sh

# This script is used to launch the benchmark and monitor the memory usage of the process.
# Usage at the root of the repository:
#   ./benchmark_v2/python_src/launch_benchmark.sh <command>

# Execute the provided command in the background
"$@" &

# Get the PID of the last background process
pid=$!

# Check if PID is not empty
if [ -n "$pid" ]; then
    python benchmark_v2/python_src/benchmark_memory.py -i "$pid"
else
    echo "Failed to get PID."
fi
