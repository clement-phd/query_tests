import os
import sys

# Define the pattern as a constant
PATTERN = '_0.c'

def list_files_and_filter_by_pattern(folder_path):
    all_files : list[str] = []
    pattern_files : list[str] = []

    # List all files in the given folder
    for root, dirs, files in os.walk(folder_path):
        for file in files:
            all_files.append(os.path.join(root, file))
    
    # Filter files that end with the given pattern
    pattern_files = [file for file in all_files if file.endswith(PATTERN)]
    
    # Remaining files after filtering out the ones that end with the pattern
    remaining_files = [file for file in all_files if not file.endswith(PATTERN)]
    
    return remaining_files, pattern_files

def main(folder_path):
    remaining_files, pattern_files = list_files_and_filter_by_pattern(folder_path)

    kept_files : list[str] = []

    for file in pattern_files:
        # if the prefix of the file is in the remaining file, delete it from the list
        prefix = file.replace(PATTERN, '')
        is_in = False
        for remaining_file in remaining_files:
            if remaining_file.startswith(prefix):
                is_in = True
                break

        if not is_in:
            kept_files.append(file)
    
    print("Files that will be kept:")
    for file in kept_files:
        print(file)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python script.py <folder_path>")
        sys.exit(1)

    folder_path = sys.argv[1]
    main(folder_path)
