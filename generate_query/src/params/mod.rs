pub mod argv;
use std::env;

use lazy_static::lazy_static;
use log::error;
lazy_static! {
    /// The minimum length of a CVE code to be save
    pub static ref MIN_LINE_LENGTH_OF_CVE_CODE: usize = {
        let min_line_of_cve_code = std::env::var("MIN_LINE_LENGTH_OF_CVE_CODE");
        match min_line_of_cve_code {
            Ok(min_line_of_cve_code) => {
                match min_line_of_cve_code.parse::<usize>() {
                    Ok(min_line_of_cve_code) => {
                        if min_line_of_cve_code < 1 {
                            panic!("MIN_LINE_LENGTH_OF_CVE_CODE environment variable must be at least 1.");
                        }
                        min_line_of_cve_code
                    },
                    Err(_) => {
                        panic!("MIN_LINE_LENGTH_OF_CVE_CODE environment variable is not a valid number.");
                    },
                }
            }
            Err(_) => {
                panic!("MIN_LINE_LENGTH_OF_CVE_CODE environment variable is not set.");
            },
        }
    };

    /// The directory where the benchmark data is stored
    pub static ref QUERY_CHARACTER_SIZE_LIMIT: usize = {
        let env_var = env::var("QUERY_CHARACTER_SIZE_LIMIT").unwrap_or_else(|_| {
            error!("QUERY_CHARACTER_SIZE_LIMIT not found");
            panic!("QUERY_CHARACTER_SIZE_LIMIT not found")
        });
        env_var.parse::<usize>().unwrap()
    };
}