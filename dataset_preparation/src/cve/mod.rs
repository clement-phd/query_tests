use std::fs;
use std::path::Path;

use cve_dataset::cve_commit::cve_commit_file_patched::CveCommitFilePatched;
use rust_utils::tree_sitter_utils::languages::Language;
use serde::{Deserialize, Serialize};

use crate::conversion::line_removed::ExtractedCveCode;




/// This structure handle a file from a CVE and all the information needed to feed the detector
pub struct CveFile {
    pub cve_id: String,
    pub file_basename: String,
    pub file_inside_path: String,
    pub original_file_content: String,
    pub fixed_file_content: String,
    /// The list of CVE codes that are in the file
    pub cve_codes: Vec<ExtractedCveCode>, 

}

impl CveFile {

    pub fn has_cve_code_valid(&self) -> bool {
        for cve in self.cve_codes.iter() {
            if cve.is_cve_code_valid() {
                return true;
            }
        }
        false
    }
}



/// This struct handle the final CVE data to feed to the detector
pub struct Cve {
    pub cve_id: String,
    pub files: Vec<CveFile>,
    pub other_than_language_files: Vec<CveCommitFilePatched>,
    pub other_than_modified_files: Vec<CveCommitFilePatched>,
}   

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct CveInfoWrapper {
    cve_id: String,
    language: Language,
    files_inter_path: Vec<String>,
    other_than_language_files_intern_path: Vec<String>,
    other_than_modified_files_inter_path: Vec<String>,
}

impl CveInfoWrapper {
    pub fn new(cve: &Cve, language: Language) -> CveInfoWrapper {
        let files_inter_path = cve.files.iter().map(|file| file.file_inside_path.clone()).collect();
        let other_than_language_files_intern_path = cve.other_than_language_files.iter().map(|file| file.inside_file_path.clone()).collect();
        let other_than_modified_files_inter_path = cve.other_than_modified_files.iter().map(|file| file.inside_file_path.clone()).collect();
        CveInfoWrapper {
            cve_id: cve.cve_id.clone(),
            files_inter_path,
            other_than_language_files_intern_path,
            other_than_modified_files_inter_path,
            language,
        }
    }

    pub fn get_cve_id(&self) -> &str {
        self.cve_id.as_str()
    }

    pub fn get_files_inter_path(&self) -> &Vec<String> {
        self.files_inter_path.as_ref()
    }

    pub fn get_other_than_language_files_intern_path(&self) -> &Vec<String> {
        self.other_than_language_files_intern_path.as_ref()
    }

    pub fn get_other_than_modified_files_inter_path(&self) -> &Vec<String> {
        self.other_than_modified_files_inter_path.as_ref()
    }

    pub fn get_language(&self) -> &Language {
        &self.language
    }
}

impl Cve {
    /// Save the CVE data to the output folder
    pub fn save(&self, output_folder_path: &str, language: &Language) {
        let cve_codes_folder_path = Path::new(output_folder_path).join(Cve::get_general_cve_code_path());
        let cve_folder_path = Path::new(output_folder_path).join(&self.cve_id);
        fs::create_dir_all(&cve_folder_path).unwrap();

        let mut cve_code_index = 0;

        for cve_file in &self.files {
            let file_as_folder_path = cve_folder_path.join(&cve_file.file_inside_path);

            let original_file_path = file_as_folder_path.join(format!("original.{}", language.get_file_extension()));
            let fixed_file_path = file_as_folder_path.join(format!("fixed.{}", language.get_file_extension()));
            let cve_codes_inside_folder_path = file_as_folder_path.join("cve_codes");

            let has_valid_cve_code = cve_file.has_cve_code_valid();

            if has_valid_cve_code {
                fs::create_dir_all(&cve_codes_folder_path).unwrap();
                fs::create_dir_all(&file_as_folder_path).unwrap();
                fs::write(&original_file_path, &cve_file.original_file_content).unwrap();
                fs::write(&fixed_file_path, &cve_file.fixed_file_content).unwrap();
                fs::create_dir_all(&cve_codes_inside_folder_path).unwrap();
                
                for (index, cve_code) in cve_file.cve_codes.iter().enumerate() {
                    if cve_code.is_cve_code_valid() {
                        let cve_code_generale_path = cve_codes_folder_path.join(format!("{}_{}.{}", self.cve_id, cve_code_index, language.get_file_extension()));
                        cve_code_index += 1;
                        fs::write(cve_code_generale_path, cve_code.to_json()).unwrap();
                    }
                    let cve_inside_code_path = cve_codes_inside_folder_path.join(format!("{}.{}", index, language.get_file_extension()));
                    fs::write(cve_inside_code_path, cve_code.to_json()).unwrap();
                }
            }
        }
        // save the informations
        let cve_info = CveInfoWrapper::new(&self, language.clone());
        let cve_info_path = cve_folder_path.join("cve_info.json");
        fs::write(cve_info_path, serde_json::to_string(&cve_info).unwrap()).unwrap();
    }


    pub fn get_general_cve_code_path() -> String {
        "cve_codes".to_string()
    }

    
}