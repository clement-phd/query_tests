use std::fs;
use std::path::Path;

use log::{debug, info};
use rayon::prelude::*;
use rust_utils::tree_sitter_utils::query_wraper::QueryWrapper;
use tree_sitter::Tree;

use rust_utils::tree_sitter_utils::get_tree_from_file_content;
use rust_utils::tree_sitter_utils::languages::Language;


use crate::run_query::query_output::QueryOutput;
use crate::run_query::run_query;

/// Wrapper to get the query results from a query and display the results
pub fn get_query_result_from_query(
    queries : &Vec<QueryWrapper>, 
    tree_to_test : &Tree, 
    source_code_to_test : &str,
    file_path : &str
) -> Option<QueryOutput> {
    let mut result: Option<QueryOutput> = None;
    for query in queries {
        let output = run_query(&tree_to_test, &query, source_code_to_test, file_path);
        if result.is_some() {
            result = Some(output.fusion(&result.unwrap()));
        }else{
            result = Some(output);
        }
    }
    result
}

/// handle the calcul logic for one file(generator, language....)
/// in summary, it is a wrapper for the run_query function
/// arguments :
/// - query_path : the path to the query
/// - tree_to_test : the tree to test with the query
/// - source_code_to_test : the source code to test (the origine of tree_to_test, to have the line number)
/// - generator : the generator to use to get the query (if None, the query is in the query file)
/// - language : the language to use
/// - file_path : the path to the file to test
pub fn get_query_results(
    query_path : &str, 
    tree_to_test : &Tree, 
    source_code_to_test : &str,
    language : &Language,
    file_path : &str
) -> Option<QueryOutput> {
  
    //get the query
    let query_str = fs::read_to_string(query_path).expect("Error loading the query.");
    let queries = QueryWrapper::new(query_str.as_str(), language);
    debug!(target: "use_query", "Query loaded.");
    //run the query
    get_query_result_from_query(
        &queries, 
        tree_to_test, 
        source_code_to_test,
        file_path
    )
}

/// wrapper to get the query results from a file path and display the results
pub fn get_query_results_from_file(
    file_path : &str,
    query_path : &str,
    language : &Language,
) -> Option<QueryOutput> {

    if Language::is_file_language(language, file_path) {
        let file_path = Path::new(file_path);
        let source_code_to_test = fs::read_to_string(file_path).expect("Error loading the file.");
        let tree_to_test = get_tree_from_file_content(source_code_to_test.as_str(), language);

        let query_results = get_query_results(
            query_path, 
            &tree_to_test, 
            source_code_to_test.as_str(),
            language,
            file_path.to_str().unwrap()
        );

        info!(target: "use_query", "Query results: {}", serde_json::to_string(&query_results).unwrap());
        return query_results;
    }else{
        info!(target: "use_query", "The file {} is not a {} file.", file_path, language.clone());
        return None;
    }
    
}

/// use a query on a file or on a list of files
/// arguments :
/// - file_to_test_path : the path to the file to test
/// - files_to_tests_file_path : the path to the file containing the list of files to test
/// - query_path : the path to the query
/// - language : the language to use
/// - generator : the generator to use to get the query (if None, the query is in the query file)
pub fn use_query(
    file_to_test_path : Option<&str>, 
    files_to_tests_file_path : Option<&str>, 
    query_path : &str, 
    language : &Language
) -> Vec<QueryOutput> {
    

    if file_to_test_path.is_some() {
        let file_to_test_path = file_to_test_path.unwrap();
        info!(target: "use_query", "File to test : {}", file_to_test_path);
        let result = get_query_results_from_file(
            file_to_test_path,
            query_path,
            language
        );
        if result.is_some(){
            let result = result.unwrap();
            return vec![result];
        }else{
            return Vec::new();
        }
    } else if files_to_tests_file_path.is_some() {
        let files_to_tests_file_path = files_to_tests_file_path.unwrap();
        let all_files = fs::read_to_string(files_to_tests_file_path).expect("Error loading the files to test file.");
        let all_files : Vec<&str> = all_files.split("\n").collect();
       

        info!(target: "use_query", "Number of files to test (after filtering): {}", all_files.len());
        
        // Assuming `files_to_test` is a collection of file paths...
        let results = all_files.par_iter().filter_map(|file| {  // .par_iter() is provided by Rayon
            get_query_results_from_file(
                file,
                query_path,
                language
            )
        }).collect();

        return results;
    }else {
       unreachable!("One of the two arguments must be present : file_to_test_path, files_to_tests_file_path.")
    };
}