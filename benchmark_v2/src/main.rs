use log::info;
#[cfg(all(feature = "memory_monitoring", feature = "parrallelize"))]
use log::warn;
use pipeline::benchmark_pipeline;
use rust_utils::logger::common_logger::init_logger;

use crate::params::argv::get_program_args;

mod params;
mod errors;
mod repository;
mod pipeline;
mod prepared_dataset;
mod monitoring;
mod results;
mod instances;





fn main_with_args(argv: params::argv::Argv) {
    let benchmark_result = benchmark_pipeline(argv.dataset_path.as_str(), argv.clean_results, argv.clean_dataset);
    if benchmark_result.is_err() {
        let err = benchmark_result.err().unwrap();
        err.log();
        panic!();
    }
}



fn main() {
    dotenv::dotenv().ok();
    init_logger();
    let argv = get_program_args();
    info!("🚀 Start benchmark.");

    #[cfg(all(feature = "memory_monitoring", feature = "parrallelize"))]
    warn!("Memory monitoring and parrallelize are enabled. This can cause more instability on the memory mesurement, use the data with cautious.");

    main_with_args(argv);
}
