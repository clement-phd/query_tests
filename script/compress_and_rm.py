import os
import shutil
import sys
from tqdm import tqdm

def get_folder_size(folder_path):
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(folder_path):
        for filename in filenames:
            filepath = os.path.join(dirpath, filename)
            total_size += os.path.getsize(filepath)
    return total_size

def compress_folder(src_folder_path : str, dest_folder_path : str):
    folder_name = os.path.basename(src_folder_path)
    # Compress the folder
    shutil.make_archive(folder_name, 'zip', dest_folder_path, src_folder_path)

    # Remove the original folder
    shutil.rmtree(src_folder_path)

def process_directory(source_directory : str, result_directory : str):
    # Get a list of folders sorted by size (from smallest to largest)
    folders = sorted(os.listdir(source_directory), key=lambda folder: get_folder_size(os.path.join(source_directory, folder)))

    for folder in tqdm(folders, desc="Processing folders", unit="folder"):
        folder_path = os.path.join(source_directory, folder)
        compress_folder(folder_path, result_directory)
        tqdm.write(f"Folder compressed and removed: {folder_path}")

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python script.py <source_directory> <result_directory>")
        sys.exit(1)

    source_directory = sys.argv[1]
    result_directory = sys.argv[2]

    if not os.path.isdir(source_directory):
        print("Error: Source directory does not exist.")
        sys.exit(1)

    if not os.path.exists(result_directory):
        os.makedirs(result_directory)

    process_directory(source_directory, result_directory)
    print("Folders compressed and removed successfully.")
