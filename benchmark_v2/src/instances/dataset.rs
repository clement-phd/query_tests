use std::path::Path;

use rust_utils::tree_sitter_utils::languages::Language;
use serde_derive::{Deserialize, Serialize};

use crate::params::DATASET_PREPARATION_OUTPUT_DIR_PATH;
use crate::prepared_dataset::{Dataset, FileToTest};



/// This is the struct containing the dataset information of an instance
#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct DatasetInstance {
    name : String,
    language : Language,
    dataset_path : String,
}


impl DatasetInstance {
    pub fn new(language : Language) -> Self {
        let instance_name = format!("{}", language);
        let global_dataset_path_o = Path::new(DATASET_PREPARATION_OUTPUT_DIR_PATH.as_str());
        let dataset_path = global_dataset_path_o.join(instance_name.as_str()).to_str().unwrap().to_string();
        Self {
            name : instance_name,
            language,
            dataset_path,
        }
    }

    pub fn get_name(&self) -> &str {
        self.name.as_str()
    }

    pub fn get_dataset_path(&self) -> &str {
        self.dataset_path.as_str()
    }

    pub fn get_language(&self) -> &Language {
        &self.language
    }
    pub fn get_cve_code_dir_path(&self) -> String {
        let dataset_path_o = Path::new(self.get_dataset_path());
        let cve_code_dir_path = dataset_path_o.join("cve_codes");
        cve_code_dir_path.to_str().unwrap().to_string()
        
    }

    /// get all the files to test of the dataset
    pub fn get_files(&self) -> Vec<FileToTest> {
        Dataset::get_dataset(self.get_dataset_path(), self.get_language()).get_files_to_test()
    }
}