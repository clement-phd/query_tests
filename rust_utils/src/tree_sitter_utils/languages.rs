use std::fmt;

use clap::ValueEnum;
use serde_derive::{Serialize, Deserialize};

use cve_dataset::language::Language as CveDatasetLanguage;




/// Enum representing the languages that are supported by this crate.
#[derive(Clone, PartialEq, Eq, Hash, Serialize, Deserialize, ValueEnum, Debug, Copy, PartialOrd, Ord)]
pub enum Language {
    Rust,
    C,
    Cpp,
    Php,
    Java
}

impl From<CveDatasetLanguage> for Language {
    fn from(language: CveDatasetLanguage) -> Self {
        match language {
            CveDatasetLanguage::Rust => Language::Rust,
            CveDatasetLanguage::C => Language::C,
            CveDatasetLanguage::Cpp => Language::Cpp,
            CveDatasetLanguage::Php => Language::Php,
            CveDatasetLanguage::Java => Language::Java,
        }
    }
}

impl From<Language> for CveDatasetLanguage {
    fn from(language: Language) -> Self {
        match language {
            Language::Rust => CveDatasetLanguage::Rust,
            Language::C => CveDatasetLanguage::C,
            Language::Cpp => CveDatasetLanguage::Cpp,
            Language::Php => CveDatasetLanguage::Php,
            Language::Java => CveDatasetLanguage::Java,
        }
    }
}

impl From<&Language> for &CveDatasetLanguage {
    fn from(language: &Language) -> Self {
        match language {
            Language::Rust => &CveDatasetLanguage::Rust,
            Language::C => &CveDatasetLanguage::C,
            Language::Cpp => &CveDatasetLanguage::Cpp,
            Language::Php => &CveDatasetLanguage::Php,
            Language::Java => &CveDatasetLanguage::Java,
        }
    }
}

impl From<&CveDatasetLanguage> for &Language {
    fn from(language: &CveDatasetLanguage) -> Self {
        match language {
            CveDatasetLanguage::Rust => &Language::Rust,
            CveDatasetLanguage::C => &Language::C,
            CveDatasetLanguage::Cpp => &Language::Cpp,
            CveDatasetLanguage::Php => &Language::Php,
            CveDatasetLanguage::Java => &Language::Java,
        }
    }
}

impl fmt::Display for Language {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Language::Rust => write!(f, "Rust"),
            Language::C => write!(f, "C"),
            Language::Cpp => write!(f, "Cpp"),
            Language::Php => write!(f, "Php"),
            Language::Java => write!(f, "Java"),
        }
    }
}

impl Language {
    pub fn get_tree_sitter_language(&self) -> tree_sitter::Language {
        match self {
            Language::Rust => tree_sitter_rust::language(),
            Language::C => tree_sitter_c::language(),
            Language::Cpp => tree_sitter_cpp::language(),
            Language::Php => tree_sitter_php::language(),
            Language::Java => tree_sitter_java::language(),
        }
    }

    pub fn get_file_extension(&self) -> &str {
        match self {
            Language::Rust => "rs",
            Language::C => "c",
            Language::Cpp => "cpp",
            Language::Php => "php",
            Language::Java => "java",
        }
    }

    pub fn get_program_arg(&self) -> String {
        self.to_string().as_str().to_lowercase()
    }

    pub fn new_from_extension(extension: &str) -> Option<Language> {
        match extension {
            "rs" => Some(Language::Rust),

            "c" => Some(Language::C),

            "cpp" => Some(Language::Cpp),
            "cxx" => Some(Language::Cpp),
            "cc" => Some(Language::Cpp),
            "c++" => Some(Language::Cpp),
            "h" => Some(Language::Cpp),
            "hpp" => Some(Language::Cpp),
            "hxx" => Some(Language::Cpp),
            "hh" => Some(Language::Cpp),
            "h++" => Some(Language::Cpp),

            "php" => Some(Language::Php),

            "java" => Some(Language::Java),

            _ => None
        }
    }

    pub fn new_by_file_name(file_name: &str) -> Option<Language> {
        let extension = file_name.split(".").last().unwrap();
        return Language::new_from_extension(extension);
    }

    pub fn is_file_language(language: &Language, file_name: &str) -> bool {
        let file_language = Language::new_by_file_name(file_name);
        return file_language == Some(language.clone());
    }
    
    pub fn is_extension_language(language: &Language, extension: &str) -> bool {
        let file_language = Language::new_from_extension(extension);
        return file_language == Some(language.clone());
    }

    pub fn get_all_languages() -> Vec<Language> {
        vec![Language::Rust, Language::C, Language::Cpp, Language::Php, Language::Java]
    }

    pub fn get_from_display_name(display_name: &str) -> Option<Language> {
        for language in Language::get_all_languages() {
            if language.to_string() == display_name {
                return Some(language);
            }
        }

        return None;
    }
}