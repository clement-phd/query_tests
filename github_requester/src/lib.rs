use std::sync::Mutex;
use std::time;

use errors::GithubRequestError;
use log::debug;
use params::MIN_TIME_BETWEEN_REQUESTS;
use reqwest::blocking::Client;
use reqwest::header;
use serde::de::DeserializeOwned;


mod params;
pub mod errors;


/// Handle github informations to make requests threadsafe, authentically and with a minimum time between requests (avoiding rate limiting)
struct GithubRequester {
    client: Client,
    last_request_instant: Option<f64>,
    token: Option<String>,
}


impl GithubRequester {
    pub fn new() -> Self {
        GithubRequester {
            client: Client::new(),
            last_request_instant: None,
            token: None,
        }
    }

    pub fn get_client(&self) -> &Client {
        &self.client
    }

    pub fn get_token(&self) -> Option<&str> {
        self.token.as_deref()
    }

    pub fn set_token(&mut self, token: String) {
        if self.token.is_none() {
            // recreate the client with the new token

            let mut headers = header::HeaderMap::new();
            let mut authorization_value = header::HeaderValue::from_str(&format!("Bearer {}", token)).unwrap();
            authorization_value.set_sensitive(true);
            // WARN : the user agent is required by the github api
            headers.insert(header::USER_AGENT, header::HeaderValue::from_static("reqwest"));
            headers.insert(header::AUTHORIZATION, authorization_value);

            self.client = Client::builder()
                .default_headers(headers)
                .build().unwrap();

            // set the token

            self.token = Some(token);
        }
    }

   
    pub fn get_time_since_last_request(&self) -> Option<f64> {
        let now = time::SystemTime::now().duration_since(time::UNIX_EPOCH).unwrap().as_secs_f64();
        if let Some(last_request_instant) = self.last_request_instant {
            Some(now - last_request_instant)
        }else{
            None
        }
    }

    pub fn reset_last_request_instant(&mut self) {
        self.last_request_instant = Some(time::SystemTime::now().duration_since(time::UNIX_EPOCH).unwrap().as_secs_f64());
    }
}



lazy_static::lazy_static! {
    pub(crate) static ref GITHUB_REQUESTER_DATA: Mutex<GithubRequester> = Mutex::new(GithubRequester::new());
}

/// Set the github token to use for the requests
/// WARN : If the token is already set, it will not be changed
pub fn set_github_token(token: String) {
    let mut github_requester_guarded = GITHUB_REQUESTER_DATA.lock().unwrap();
    github_requester_guarded.set_token(token);
}


/// Get the content of an url as a string
pub fn get_string_from_url(
    url : &str
) -> Result<String, GithubRequestError> {
    let body = reqwest::blocking::get(url)?.text()?;

    Ok(body)
}


/// Get the information of a commit
/// - repo_name: the name of the repo (owner/repo)
/// - commit_ref: the commit reference (sha)
/// - return: Some(json) if the request was successful, None if the request was a 404
pub fn get_commit_infos(
    repo_name: &str,
    commit_ref: &str
) -> Result<Option<serde_json::Value>, GithubRequestError> {
    let url = format!("repos/{}/commits/{}", repo_name, commit_ref);
    github_get_request(&url)
}





/// Make a get request to the github api
/// It will wait if the last request was made less than MIN_TIME_BETWEEN_REQUESTS seconds ago
/// It will return an error if the request fails
/// It will return an error if the response is an error
/// It will return an error if the response is not a json
/// - end_url: the end of the url to get (does not start with a /)
/// - return: Some(json) if the request was successful, None if the request was a 404
fn github_get_request<T>(
    end_url: &str
) -> Result<Option<T>, GithubRequestError> 
where T: DeserializeOwned
{
    let url = format!("{}/{}", params::GITHUB_API_URL, end_url);
    let mut github_requester_guarded = GITHUB_REQUESTER_DATA.lock().unwrap();
    if github_requester_guarded.get_token().is_none() {
        return Err(GithubRequestError::NoCredentialsError);
    }

    if let Some(time_since_last_request) = github_requester_guarded.get_time_since_last_request() {
        if time_since_last_request < MIN_TIME_BETWEEN_REQUESTS {
            std::thread::sleep(time::Duration::from_secs_f64(1.0 - time_since_last_request));
        }
    }

    debug!("GET {}", url);

    let resp = github_requester_guarded.get_client().get(url).send()?;

    github_requester_guarded.reset_last_request_instant();

    if resp.status() == reqwest::StatusCode::NOT_FOUND ||
        resp.status() == reqwest::StatusCode::UNPROCESSABLE_ENTITY
    {
        Ok(None)
    }else{
        let resp = resp.error_for_status()?;
        let json = resp.json::<T>()?;
        Ok(Some(json))
    }
}