use std::io;

use log::{debug, info};
use rayon::iter::{IntoParallelRefIterator, ParallelIterator};
use regex::Regex;
use rust_utils::tree_sitter_utils::get_tree_from_file_content;
use rust_utils::tree_sitter_utils::languages::Language;

use crate::data::ExtractedCveCodePayload;
use crate::errors::error::{GenerateQueryErrorPayload, GenerateQueryErrorType};
use crate::errors::warning::{GenerateQueryWarning, GenerateQueryWarningPayload, GenerateQueryWarningType};
use crate::generators;
use crate::params::argv::Generator;

/// get the query from the file content and the generator, labelized with the label
pub fn get_query_from_file_content(
    extracted_cve_code : &ExtractedCveCodePayload, 
    generator : &Generator, 
    language : &Language, 
    context_threshold : usize
) -> Result<(String, Option<GenerateQueryWarning>), GenerateQueryErrorPayload>  {
    let tree = get_tree_from_file_content(extracted_cve_code.get_extracted_cve_code().get_code(), language);
    let (query, warning) = match generator {
        Generator::ToSexp => {
            generators::to_sexp::get_query_from_node(extracted_cve_code.get_label(), &tree.root_node(), context_threshold)
        },
        Generator::AstRange => {
            generators::ast_range::ast_range_query_converter(extracted_cve_code, language, context_threshold)
        }
    }?;

    if *crate::params::QUERY_CHARACTER_SIZE_LIMIT != 0 && query.len() > *crate::params::QUERY_CHARACTER_SIZE_LIMIT {
        return Err(GenerateQueryErrorPayload::new(extracted_cve_code.get_label(), GenerateQueryErrorType::TooMuchCharacter, warning));
    }

    let mut warnings = warning;

    if query.contains("ERROR") {
        if warnings.is_none() {
            warnings = Some(GenerateQueryWarning::new(
                extracted_cve_code.get_label(),
                vec![GenerateQueryWarningPayload::new(
                    Some(query.as_str()),
                    GenerateQueryWarningType::QueryHasErrorNode
                )]
            ));
        }else{
            warnings.as_mut().unwrap().add_payload(GenerateQueryWarningPayload::new(
                Some(query.as_str()),
                GenerateQueryWarningType::QueryHasErrorNode
            ));
        }
    }

    if query.contains("MISSING") {
        if warnings.is_none() {
            warnings = Some(GenerateQueryWarning::new(
                extracted_cve_code.get_label(),
                vec![GenerateQueryWarningPayload::new(
                    Some(query.as_str()),
                    GenerateQueryWarningType::QueryHasMissingNode
                )]
            ));
        }else{
            warnings.as_mut().unwrap().add_payload(GenerateQueryWarningPayload::new(
                Some(query.as_str()),
                GenerateQueryWarningType::QueryHasMissingNode
            ));
        }
    }

    if query.contains("UNEXPECTED") {
        if warnings.is_none() {
            warnings = Some(GenerateQueryWarning::new(
                extracted_cve_code.get_label(),
                vec![GenerateQueryWarningPayload::new(
                    Some(query.as_str()),
                    GenerateQueryWarningType::QueryHasUnexpectedNode
                )]
            ));
        }else{
            warnings.as_mut().unwrap().add_payload(GenerateQueryWarningPayload::new(
                Some(query.as_str()),
                GenerateQueryWarningType::QueryHasUnexpectedNode
            ));
        }
    }

    // remove error from the query
    let re = Regex::new(r"ERROR|MISSING|UNEXPECTED").unwrap(); // Regular expression to match "ERROR" or "MISSING"
    let replaced_query = re.replace_all(query.as_str(), "_");

    // labelize the query with the cve and number of the file
    let labelized_query = format!("{} @{}", replaced_query, extracted_cve_code.get_label());

    // test if the query is valid

    let tree_sitter_query = tree_sitter::Query::new(language.get_tree_sitter_language(), labelized_query.as_str());
    if tree_sitter_query.is_err() {
        debug!("❌ Error when generating the query : {}", tree_sitter_query.err().unwrap());
        return Err(GenerateQueryErrorPayload::new(extracted_cve_code.get_label(), GenerateQueryErrorType::Malformed, warnings));
    }else{
        return Ok((labelized_query, warnings));
    }

}


/**
 * get the query from the path file and the pipeline
 * WARN : the language is guessed from the file name if it is not specified
 */
pub fn get_query(
    file_path : &str, 
    generator : &Generator, 
    language : Option<&Language>, 
    context_threshold : usize
) -> Result<(String, Option<GenerateQueryWarning>), GenerateQueryErrorPayload> {
    
    let language = match language {
        Some(language) => language.clone(),
        None => Language::new_by_file_name(file_path).unwrap(),
    };

    // labelize the query with the cve and number of the file
    let extracted_cve_code = ExtractedCveCodePayload::new_from_file(file_path);
   
    return get_query_from_file_content(&extracted_cve_code, generator, &language, context_threshold);
}

/// Generate all the queries from the files in the directory or in the files in the files_path
/// and save them in the output_path
/// return the queries
pub fn generate_all_queries(
    files_path : Option<&Vec<String>>,
    output_path : &str,
    directory_path : Option<&str>,
    generator : &Generator,
    language : Option<&Language>,
    context_threshold : usize
) -> Result<Vec<Result<(String, Option<GenerateQueryWarning>), GenerateQueryErrorPayload>>, io::Error> {


    let mut all_files_path = Vec::new();


    if let Some(files) = files_path {
        all_files_path = files.clone();
    }

    if let Some(directory) = directory_path {
        let files = std::fs::read_dir(directory).expect("Error reading the directory");
        for file in files {
            let file = file.expect("Error reading the file");
            let path = file.path();
            if path.is_file() {
                let path: &str = path.to_str().expect("Error converting the path to a string");
                all_files_path.push(path.to_string());
            }
        }
    }

    
    let queries: Vec<_> = all_files_path
        .par_iter() // Parallel mutable iterator
        .map(|file| get_query(file, generator, language, context_threshold))
        .collect();
    let filtered_queries = queries.iter().filter_map(|query| {
        match query {
            Ok(query) => Some(query.clone()),
            Err(error) => {
                info!("❌ Error when generating the query : {}", error);
                None
            }
        }
    }).collect::<Vec<(String, Option<GenerateQueryWarning>)>>();
    let filtered_str_queries = filtered_queries.iter().map(|(query, _)| query.clone()).collect::<Vec<String>>();
    // save the query
    std::fs::write(output_path, filtered_str_queries.join("\n"))?;
    info!("✅ Query generated and saved in {}.", output_path);
    Ok(queries)
}

