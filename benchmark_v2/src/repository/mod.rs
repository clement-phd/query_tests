use std::fs;
use std::path::Path;
use std::process::Command;
use std::str;

use rust_utils::utils::get_all_files;
use serde_derive::{Deserialize, Serialize};

use crate::errors::repo_error::RepoError;

use self::diff_file::DiffFile;

pub mod diff_file;


/// Repository struct
/// get all the informations needed to manage a repository
/// If it is not a git repository, Don't call the methods that need git
#[derive(Clone, PartialEq, Serialize, Deserialize, Debug)]
pub struct Repository {
    pub url : Option<String>,
    pub name : String,
    pub folder_path : String,
    /// the commit hash of the current commit
    actual_commit_hash : String,
    /// the commit hash of the known last commit
    last_commit_hash : String,
}


impl Repository {
    /// create a new repository from the folder path
    pub fn new(folder_path: &str) -> Result<Repository, RepoError> {
        // Check if the directory exists and if it is a repository
        if !Path::new(folder_path).exists() {
            return Err(RepoError::UnexpectedError(format!("The folder {} doesn't exist", folder_path)));
        }
        let url; 
        let actual_commit_hash;
        let last_commit_hash;
        if Path::new(folder_path).join(".git").exists() {
            // get the url
            let output = Command::new("git")
                .arg("config")
                .arg("--get")
                .arg("remote.origin.url")
                .current_dir(folder_path)
                .output();

            url = match output {
                Ok(output) => {
                    if output.status.success() {
                        Ok(Some(String::from_utf8(output.stdout).unwrap().trim().to_string()))
                    } else {
                        Err(RepoError::UnexpectedError(String::from_utf8_lossy(&output.stderr).to_string()))
                    }
                }
                Err(e) => Err(e.into()),
            }?;

            actual_commit_hash = Repository::get_commit_hash(folder_path, "HEAD")?;
            last_commit_hash = actual_commit_hash.clone();
        }else{
            url = None;
            actual_commit_hash = String::new();
            last_commit_hash = String::new();
        };
        

        Ok(Repository {
            url: url,
            name: folder_path.split('/').last().unwrap_or_default().to_string(),
            folder_path: folder_path.to_string(),
            actual_commit_hash,
            last_commit_hash,
        })
    }


    pub fn get_last_commit_hash(&self) -> &str {
        self.last_commit_hash.as_str()
    }

    pub fn get_actual_commit_hash(&self) -> &str {
        self.actual_commit_hash.as_str()
    }


    /// get the full path of the file
    pub fn get_full_path(&self, file : &DiffFile) -> String{
        let repo_path_o = Path::new(self.folder_path.as_str());
        repo_path_o.join(file.inside_file_path.as_str()).to_str().unwrap().to_string()
    }

    /// get all the commit of the repository
    /// NOTE : need Git
    /// WARN : This method can be very slow on big repositories
    pub fn count_commits_from(&self, ref_ : &str) -> Result<usize, RepoError> {
        let output = Command::new("git")
            .arg("rev-list")
            .arg("--count")
            .arg(ref_)
            .current_dir(self.folder_path.as_str())
            .output();
    
        match output {
            Ok(output) if output.status.success() => {
                let count = String::from_utf8(output.stdout)
                    .unwrap()
                    .trim()
                    .parse::<usize>()
                    .unwrap();
                Ok(count)
            },
            Ok(output) => Err(RepoError::UnexpectedError(String::from_utf8_lossy(&output.stderr).to_string())),
            Err(e) => Err(e.into()),
        }
    }

    pub fn get_principal_branch(&self) -> Result<String, RepoError> {
        // Execute `git remote show origin` to get details about the origin remote
        let output = Command::new("git")
        .arg("remote")
        .arg("show")
        .arg("origin")
        .current_dir(self.folder_path.as_str())
        .output()?;

        // Convert the command output to a string
        let output_str = str::from_utf8(&output.stdout);
        if output_str.is_err() {
            return Err(RepoError::UnexpectedError(output_str.unwrap_err().to_string()));
        }
        let output_str = output_str.unwrap();

        // Search for the line that starts with "HEAD branch" to find the default branch
        let principal_branch = output_str.lines()
            .find(|line| line.starts_with("  HEAD branch: "))
            .and_then(|line| line.split(": ").nth(1))
            .ok_or("Default branch not found");
        match principal_branch {
            Err(e) => Err(RepoError::UnexpectedError(e.to_string())),
            Ok(principal_branch) => Ok(principal_branch.to_string())
        }
    }

    /// reset the repository to the head commit and return the new hash
    /// NOTE : need Git
    /// WARN : Invalidate the caller
    pub fn reset_to_head(&mut self) -> Result<&str, RepoError> {
        let principal_branch = self.get_principal_branch()?;
        let _ = Command::new("git")
            .arg("reset")
            .arg("--hard")
            .current_dir(self.folder_path.as_str())
            .status()?;

        let _ = Command::new("git")
                .arg("checkout")
                .arg(principal_branch.as_str())
                .current_dir(self.folder_path.as_str())
                .status()?;

        let _ = Command::new("git")
            .arg("pull")
            .current_dir(self.folder_path.as_str())
            .status()?;

        let head_hash = Repository::get_commit_hash(self.folder_path.as_str(), "HEAD")?;
        self.goto_commit(head_hash.as_str())?;
        Ok(&self.actual_commit_hash)
    }

    /// reset the repository to the last commit (the one stored in memory)
    /// NOTE : need Git
    /// WARN : Invalidate the caller
    pub fn reset_to_last_commit(&self) -> Result<Repository, RepoError> {
        let mut result_repo = self.clone();
        result_repo.goto_commit(self.get_last_commit_hash())?;
        Ok(result_repo)
    }

    /// get the previous commit hash of the repository
    /// NOTE : need Git
    pub fn get_previous_commit_hash(&self) -> Result<String, RepoError> {
        let previous_commit_output = Command::new("git")
            .arg("log")
            .arg("--format=%H")
            .arg("-n")
            .arg("2")
            .arg(self.get_actual_commit_hash())
            .current_dir(&self.folder_path)
            .output();

        match previous_commit_output {
            Ok(output) => {
                if output.status.success() {
                    // Skipping the first commit (current) and taking the next one (previous)
                    let commit_lines = String::from_utf8(output.stdout).unwrap();
                    let next_commit = commit_lines.lines().nth(1);
                    match next_commit {
                        Some(commit) => Ok(commit.to_string()),
                        None => return Err(RepoError::CommitNotFound(String::from("Previous commit not found"))),
                    }

                } else {
                    Err(RepoError::UnexpectedError(String::from_utf8_lossy(&output.stderr).to_string()))
                }
            }
            Err(e) => Err(e.into()),
        }
    }

    /// get the files that have been modified between the current commit and the commit with the given hash
    /// NOTE : need Git
    pub fn get_changed_files(&self, prec_commit_hash : &str, next_commit_hash : &str) -> Result<Vec<DiffFile>, RepoError> {
        let diff_output = Command::new("git")
            .arg("diff")
            .arg("--name-status")
            .arg(prec_commit_hash)
            .arg(next_commit_hash)
            .current_dir(&self.folder_path)
            .output();

        match diff_output {
            Ok(output) => {
                if output.status.success() {
                    let commit_lines = String::from_utf8(output.stdout).unwrap();
                    let files = DiffFile::get_from_diff_output(&commit_lines);
                    Ok(files)
                } else {
                    Err(RepoError::UnexpectedError(String::from_utf8_lossy(&output.stderr).to_string()))
                }
            }
            Err(e) => Err(e.into()),
        }
    }

    /// go to the previous commit of the repository and return the list of files
    /// that have been modified (deleted, modified from the next commit) and the hash of the 
    /// new commit
    /// NOTE : need Git
    pub fn goto_previous_commit(&mut self) -> Result<(String, Vec<DiffFile>), RepoError>{
        let actual_commit_hash = self.get_actual_commit_hash();
        let previous_commit_hash = self.get_previous_commit_hash()?;
        let files = self.get_changed_files(actual_commit_hash,previous_commit_hash.as_str())?;
        {
            self.goto_commit(&previous_commit_hash)?;
        }
        Ok((previous_commit_hash, files))
    }

    /// go to the commit of the repository
    /// NOTE : need Git
    pub fn goto_commit(&mut self, commit_hash : &str) -> Result<(), RepoError>{
        let output = Command::new("git")
            .arg("checkout")
            .arg(commit_hash)
            .current_dir(&self.folder_path) // Set the directory to the repository's location
            .output();

        match output {
            Ok(output) => {
                if output.status.success() {
                    self.actual_commit_hash = commit_hash.to_string();
                    Ok(())
                } else {
                    Err(RepoError::UnexpectedError(String::from_utf8_lossy(&output.stderr).to_string()))
                }
            },
            Err(e) => Err(RepoError::UnexpectedError(e.to_string())),
        }
    }

    /// get all the path of the files of the repository (recursively)
    /// Note: exclude the .git folder and the folders in the exclude_folder_list
    pub fn get_all_files(&self, exclude_folder_list : &Vec<String>) -> Result<Vec<String>, RepoError>{
        let repo_path_o = Path::new(self.folder_path.as_str());
        let all_files = get_all_files(self.folder_path.as_str());

        // make the finale list
        let git_folder_path = repo_path_o.join(".git").to_str().unwrap().to_string();

        let mut final_list = Vec::new();
        'file_loop: for file in all_files {
            // exclude the .git folder and the folders in the exclude_folder_list
            if file.starts_with(git_folder_path.as_str()) {
                continue 'file_loop;
            }
            for exclude_folder in exclude_folder_list {
                if file.starts_with(exclude_folder) {
                    continue 'file_loop;
                }
            }


            final_list.push(file);
        }

        Ok(final_list)
    }

    /// clone a github repository if it doesn't exist
    /// and return the repository
    pub fn clone(name : &str) -> Result<Repository, RepoError>{
        let url = format!("{}:{}.git", crate::params::GITHUB_URL, name);
        let path = Path::new(&*crate::params::REPOS_DIR_PATH).join(name.split('/').last().unwrap_or_default());
        // check if the repository already exists
        if path.exists() {
            let url_output = Command::new("git")
                .arg("config")
                .arg("--get")
                .arg("remote.origin.url")
                .current_dir(&path)
                .output()?;

            let url_o;
            if url_output.status.success() {
                url_o = String::from_utf8(url_output.stdout).unwrap().trim().to_string();
            } else {
                return Err(RepoError::UnexpectedError(String::from_utf8_lossy(&url_output.stderr).to_string()));
            }

            if url_o != url {
                return Err(RepoError::UrlMismatch(url_o, url));
            }
        }else{
            fs::create_dir_all(&path)?;
            let _ = Command::new("git")
                .arg("clone")
                .arg(url)
                .current_dir(&*crate::params::REPOS_DIR_PATH)
                .status()?;
        }

        let repo = Repository::new(path.to_str().unwrap())?;

        Ok(repo)
    }

    /// Helper function to get a commit hash based on a git ref
    /// NOTE : need Git
    fn get_commit_hash(folder_path: &str, git_ref: &str) -> Result<String, RepoError> {
        let output = Command::new("git")
            .args(&["rev-parse", git_ref])
            .current_dir(folder_path)
            .output();
        match output {
            Ok(output) => {
                if output.status.success() {
                    Ok(String::from_utf8(output.stdout).unwrap().trim().to_string())
                } else {
                    Err(RepoError::UnexpectedError(String::from_utf8_lossy(&output.stderr).to_string()))
                }
            }
            Err(e) => Err(e.into()),
        }
    }
}


