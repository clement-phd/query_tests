use clap::{Args, Parser};

/// Analyze the result of the benchmark
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub struct Argv {
    #[command(flatten)]
    pub action: Action,

    /// output dir
    #[clap(short, long)]
    pub output_dir: String,


    /// save the data in a json file
    #[clap(short, long, action)]
    pub save_data: bool,

    
}

#[derive(Args, Debug)]
#[group(required = true, multiple = false)]
pub struct Action {
     /// The directory where the data are extracted
     #[clap(short, long)]
    pub extracted_data_dir: Option<String>,
    /// only regenerate the html
    #[clap(long, action)]
    pub only_html: bool,
}


pub fn get_program_args() -> Argv {
    return Argv::parse();
}