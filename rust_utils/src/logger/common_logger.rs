use crate::params::common_params::LOG_CONFIG_PATH;




pub fn init_logger() {
    log4rs::init_file(LOG_CONFIG_PATH, Default::default()).unwrap();
}