use serde_derive::{Deserialize, Serialize};



/// represent a file that has been modified
#[derive(Clone, PartialEq, Serialize, Deserialize, Debug)]
pub struct DiffFile {
    /// the path of the file inside the repository
    pub inside_file_path: String,

    /// diff type
    pub diff_type: DiffType,
}

impl DiffFile {
    /// get the list of the modified files from the diff output 
    /// NOTE : the command must be : git diff --name-status
    pub fn get_from_diff_output(diff_output : &str) -> Vec<DiffFile> {
        let mut diff_files = Vec::new();
        for line in diff_output.lines() {
            let mut line = line.split("\t");
            let diff_type = line.next().unwrap();
            let inside_file_path = line.next().unwrap();
            let diff_type = DiffType::new_from_letter(diff_type);
            match diff_type {
                Some(diff_type) => {
                    diff_files.push(DiffFile {
                        inside_file_path: inside_file_path.to_string(),
                        diff_type,
                    });
                },
                None => {},
            };
        }
        diff_files
    }
}

#[derive(Clone, PartialEq, Eq, Hash, Debug, Serialize, Deserialize)]
pub enum DiffType {
    Added,
    Deleted,
    Modified,
}


impl DiffType {
    pub fn new_from_letter(letter: &str) -> Option<DiffType> {
        match letter {
            "A" => Some(DiffType::Added),
            "D" => Some(DiffType::Deleted),
            "M" => Some(DiffType::Modified),
            _ => None,
        }
    }
}