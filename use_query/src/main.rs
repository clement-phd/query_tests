use log::info;
use rust_utils::logger::common_logger::init_logger;

use crate::params::argv::get_program_args;
use crate::pipeline::use_query;

mod params;
mod run_query;
mod hand_made_tree_matching;
mod pipeline;

fn main() {
    dotenv::dotenv().ok();
    init_logger();
    let argv = get_program_args();
    info!(target: "use_query", "🚀 Start use_query.");
    use_query(
        argv.file_to_test_path.as_ref().map(|f|f.as_str()),
        argv.files_to_tests_file_path.as_ref().map(|f|f.as_str()),
        argv.query_path.as_str(),
        &argv.language
    );
}


