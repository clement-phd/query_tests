use std::path::Path;

use log::info;
use regex::Regex;
use rust_utils::utils::get_all_files;


pub mod keys;
pub mod repo_sample;
pub mod dataset_sample;
pub mod memory_sample;
pub mod multi_line_query_sample;
pub mod single_line_query_sample;
pub mod correctness_sample;
pub mod hunk_serie;


pub fn get_file_data_path(data_dir : &str) -> Vec<String> {
    let data_dir_o = Path::new(data_dir);
    let result_dir_o = data_dir_o.join("benchmark_results");

    let files = get_all_files(result_dir_o.to_str().unwrap());

    // isolate file results
    let pattern = r"\d+\.json$";
    let re = Regex::new(pattern).expect("Invalid regex pattern");
    let all_file_results_path : Vec<String> = files.iter().filter_map(|entry| {
        if re.is_match(entry) {
            let entry_o = Path::new(entry);
            if entry_o.components().rev().nth(1).unwrap().as_os_str() == "repo" { // remove repo results (hash can be only numbers)
                return None;
            }
            return Some(entry.clone());
        }else{
            return None;
        }
    })
    .collect();

    info!("Found {} file results", all_file_results_path.len());

    all_file_results_path
}
