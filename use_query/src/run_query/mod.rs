use std::path::Path;

use rust_utils::tree_sitter_utils::query_wraper::QueryWrapper;
use tree_sitter::{QueryCursor, Tree};

use self::query_output::{QueryMatch, QueryOutput};

pub mod query_output;

/// Run a query on a tree and return the matches
pub fn run_query(tree: &Tree, query: &QueryWrapper, source_code: &str, file_path : &str) -> QueryOutput {
    let mut matches_vec :  Vec<QueryMatch>  = Vec::new();
    let query_cursor = &mut QueryCursor::new();
    
    let matches = query_cursor.matches(query.get_query(), tree.root_node(), source_code.as_bytes());
    for m in matches {
        for c in m.captures {
            matches_vec.push(
                QueryMatch {
                    name : query.get_query().capture_names().get(c.index as usize).unwrap().clone(),
                    start : c.node.start_position().into(),
                    end : c.node.end_position().into()
                }
            );
        }
    }

    let file_path_o = Path::new(file_path);
    let file_name = file_path_o.file_name().unwrap().to_str().unwrap().to_string();

    return QueryOutput {
        file_path : file_path.to_string(),
        file_name : file_name,
        query_results : matches_vec
    };
}