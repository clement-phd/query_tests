# upgrade of the python benchmark project

Need the full project to be compiled with :
```shell
cargo build --release
```

Then launch directly with the executable, or :

```shell
cargo run --bin benchmark_v2 --release -- -h
```

## logging
if you don't want the progress bar messed up with the use_query, set the log level (in the log [config](../log4rs.yaml) file) of the use_query logger to `warn` or deactivate the appender stdout.