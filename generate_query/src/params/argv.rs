use std::fmt::Display;

use clap::{Parser, ValueEnum, ArgGroup};
use rust_utils::tree_sitter_utils::languages::Language;
use serde_derive::{Deserialize, Serialize};

/// Generate a tree sitter query from a file or a directory
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
#[command(group(ArgGroup::new("file_input_group").required(true).args(&["files_path", "directory_path"])))]
pub struct Argv {
    /// The pipeline to run to obtain the query
    #[arg(value_enum, short, long)]
    pub generator: Generator,

    /// The path to the file(s) to parse to query
    #[arg(short, long, group = "file_input_group")]
    pub files_path: Option<Vec<String>>,

    /// The path to the directory to parse to query
    #[arg(short, long, group = "file_input_group")]
    pub directory_path: Option<String>,

    /// The path to the file to save the query
    #[arg(short, long)]
    pub output_path: String,

    /// The language of the file to parse
    #[arg(value_enum, short, long)]
    pub language: Language,

    /// context threshold : the minimun number of line to keep
    #[arg(short, long)]
    pub context_threshold: usize,
}


#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum, Debug, Deserialize, Serialize, Hash)]
pub enum Generator {
    /// use the sexp function of tree siter and nothing else
    ToSexp,
    /// convert the infected file into an ast and get the node which contain the cve code, then convert it witn tosexp
    AstRange
}

impl Display for Generator {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Generator::ToSexp => write!(f, "ToSexp"),
            Generator::AstRange => write!(f, "AstRange"),
        }
    }
}

impl Generator {
    pub fn get_all_generators() -> Vec<Generator> {
        return vec![Generator::ToSexp, Generator::AstRange];
    }

    pub fn from_string(generator : &str) -> Option<Generator> {
        for gen in Generator::get_all_generators() {
            if gen.to_string() == generator {
                return Some(gen);
            }
        }
        return None;
    }
}


pub fn get_program_args() -> Argv {
    return Argv::parse();
}