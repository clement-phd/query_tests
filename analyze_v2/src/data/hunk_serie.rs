use std::collections::{HashMap, HashSet};
use std::fs;
use std::path::Path;

use benchmark_v2::instances::instance::Instance;
use generate_query::errors::error::GenerateQueryErrorPayload;
use generate_query::errors::warning::GenerateQueryWarning;
use generate_query::params::argv::Generator;
use plot_helper::data::plottable::sample::{MultipleSample, Sample};
use plot_helper::data::plottable::{PlottableMultipleSamplesFromPaths, PlottableStruct};
use rust_utils::tree_sitter_utils::languages::Language;
use serde::{Deserialize, Serialize};

use super::keys::HunkSerieKey;

/// HunkSerieKey is the key used to unickly identify a HunkSerie (it consist in the label of the patch, the threshold and the generator)
type HunkSampleIdentifier = (String, u64, Generator);


#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct HunkSample {
    threshold : u64,
    warnings : Option<GenerateQueryWarning>,
    query_error : Option<GenerateQueryErrorPayload>,
    language_patch : Language,
    label : String,
    generator : Generator,
    cve_code_path : String,
}

impl HunkSample {
    pub fn get_warnings(&self) -> &Option<GenerateQueryWarning> {
        &self.warnings
    }

    pub fn get_query_error(&self) -> &Option<GenerateQueryErrorPayload> {
        &self.query_error
    }


    fn get_warnings_from_folder(folder_path : &str) -> Result<HashMap<HunkSampleIdentifier, GenerateQueryWarning>, Box<dyn std::error::Error>> {
        let folder_path_o = Path::new(folder_path);
        let instance_name = folder_path_o.file_name().unwrap().to_str().unwrap();
        let instance = Instance::from_name(instance_name).unwrap();
        let warning_path_o = folder_path_o.join("warnings.json");
        let warnings : Vec<GenerateQueryWarning> = {
            let file_content = fs::read_to_string(warning_path_o)?;
            let warnings : Vec<GenerateQueryWarning> = serde_json::from_str(&file_content)?;
            warnings
        };
        let mut result = HashMap::new();
        for warning in warnings {
            let label = warning.get_label().to_string();
            let threshold = instance.get_min_line_of_code_for_cve_code() as u64;
            let generator = instance.get_generator().clone();
            let key = (label, threshold, generator);
            result.insert(key, warning);
        }
        
        Ok(result)
    
    }

    fn get_errors_from_folder(folder_path : &str) -> Result<HashMap<HunkSampleIdentifier, GenerateQueryErrorPayload>, Box<dyn std::error::Error>> {
        let folder_path_o = Path::new(folder_path);
        let instance_name = folder_path_o.file_name().unwrap().to_str().unwrap();
        let instance = Instance::from_name(instance_name).unwrap();
        let error_path_o = folder_path_o.join("errors.json");
        let errors : Vec<GenerateQueryErrorPayload> = {
            let file_content = fs::read_to_string(error_path_o)?;
            let errors : Vec<GenerateQueryErrorPayload> = serde_json::from_str(&file_content)?;
            errors
        };
        let mut result = HashMap::new();
        for error in errors {
            let label = error.get_label().to_string();
            let threshold = instance.get_min_line_of_code_for_cve_code() as u64;
            let generator = instance.get_generator().clone();
            let key = (label, threshold, generator);
            result.insert(key, error);
        }
        
        Ok(result)
    }

    fn get_queries_from_folder(folder_path : &str,) -> Result<HashMap<HunkSampleIdentifier, String>, Box<dyn std::error::Error>> {
        let folder_path_o = Path::new(folder_path);
        let instance_name = folder_path_o.file_name().unwrap().to_str().unwrap();
        let instance = Instance::from_name(instance_name).unwrap();
        let mut result = HashMap::new();
        for entry in fs::read_dir(&folder_path_o)? {
            let entry_o = entry?;
            if entry_o.file_type()?.is_file() {
                
                let query_path_o = entry_o.path();
                if query_path_o.extension().unwrap() == "json" {// get the error and warning file
                    continue;
                }else{
                    if query_path_o.file_name().unwrap().to_str().unwrap() == "combined_queries.scm" {
                        continue;
                    }
                    let label = query_path_o.file_stem().unwrap().to_str().unwrap().to_string();
                    let threshold = instance.get_min_line_of_code_for_cve_code() as u64;
                    let generator = instance.get_generator().clone();
                    let key = (label, threshold, generator);
                    let file_content = fs::read_to_string(&query_path_o)?;
                    result.insert(key, file_content);
                }
            
            }
        }
        Ok(result)
    
    }
}


impl Sample<HunkSerieKey> for HunkSample {
    fn get_numeric_value(&self, key: &HunkSerieKey) -> f32 {
        match key {
            HunkSerieKey::MinQueryLineOfCode => self.threshold as f32,
            HunkSerieKey::CveCodePath => unreachable!("CveCodePath is not a numeric value"),
            HunkSerieKey::Generator => unreachable!("Generator is not a numeric value"),
            HunkSerieKey::Language => unreachable!("Language is not a numeric value"),
            HunkSerieKey::Label => unreachable!("Label is not a numeric value"),
        }
    }

    fn get_string_value(&self, key : &HunkSerieKey) -> String {
        match key {
            HunkSerieKey::CveCodePath => self.cve_code_path.clone(),
            HunkSerieKey::Generator => self.generator.to_string(),
            HunkSerieKey::Language => self.language_patch.to_string(),
            HunkSerieKey::Label => self.label.clone(),
            HunkSerieKey::MinQueryLineOfCode => unreachable!("MinQueryLineOfCode is not a string value"),
        }
    }
}




impl MultipleSample<HunkSerieKey> for HunkSample {
    /// generate a sample from a folder path and not a file path
    fn new_from_file_path(file_path : &str) -> Result<Vec<Self>, Box<dyn std::error::Error>> {
        let folder_path = file_path;

        let warnings = HunkSample::get_warnings_from_folder(folder_path)?;
        let errors = HunkSample::get_errors_from_folder(folder_path)?;
        let queries = HunkSample::get_queries_from_folder(folder_path)?;

        let keys = warnings.keys().into_iter()
            .chain(errors.keys().into_iter())
            .chain(queries.keys().into_iter())
            .collect::<HashSet<_>>();

        let mut result = Vec::new();
        for key in keys {
            let (label, threshold, generator) = key;
            let warning = warnings.get(key);
            let error = errors.get(key);
            let folder_path_o = Path::new(folder_path);
            let instance_name = folder_path_o.file_name().unwrap().to_str().unwrap();
            let instance = Instance::from_name(instance_name).unwrap();
            let cve_code_path = Path::new(instance.get_dataset_instance().get_cve_code_dir_path().as_str()).join(format!("{}.{}", label, instance.get_dataset_instance().get_language().get_file_extension())).to_str().unwrap().to_string();
            let language_patch = instance.get_dataset_instance().get_language().clone();
            let label = label.to_string();
            let generator = generator.clone();
            let threshold = *threshold;
            let sample = HunkSample {
                threshold,
                warnings : warning.cloned(),
                query_error : error.cloned(),
                language_patch,
                label,
                generator,
                cve_code_path,
            };
            result.push(sample);
        }

        Ok(result)
    }

    
}

pub fn load_plottable_hunk(data_dir : &str) -> PlottableStruct<HunkSample, HunkSerieKey> {
    let data_dir_o = Path::new(data_dir);
    let queries_dir_o = data_dir_o.join("queries");
    let mut paths = Vec::new();

    for entry in fs::read_dir(&queries_dir_o).unwrap() {
        let entry_o = entry.unwrap();
        if entry_o.file_type().unwrap().is_dir() {
            paths.push(entry_o.path().to_str().unwrap().to_string());
        }
    }

    PlottableStruct::new_async_from_paths(&paths)
}