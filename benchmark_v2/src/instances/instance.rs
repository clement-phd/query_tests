

use std::{fs, io};
use std::path::Path;

use generate_query::params::argv::Generator;
use rust_utils::tree_sitter_utils::languages::Language;

use crate::params::{BENCHMARK_RESULTS_DIR_PATH, QUERIES_DIR_PATH};

use super::dataset::DatasetInstance;





/// Instance is a struct that contains the name of the instance, the language, the generator and the min_line_of_code_for_cve_code.
/// This is an instance to test the pipeline.
#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub struct Instance {
    name : String,
    language : Language,
    generator : Generator,
    min_line_of_code_for_cve_code : u8,
    result_path : String,
    repo_result_path : String,
    queries_path : String,


    all_query_paths : Vec<String>,
}

impl PartialOrd for Instance {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.name.cmp(&other.name))
    }
}

impl Ord for Instance {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.name.cmp(&other.name)
    }
}



impl Instance {
    pub fn from_name(name : &str) -> Option<Self> {
        let instance_name = name.to_string();
        let instance_name_split : Vec<&str> = instance_name.split("-").collect();
        if instance_name_split.len() != 3 {
            return None;
        }
        let language = Language::get_from_display_name(instance_name_split[0])?;
        let generator = Generator::from_string(instance_name_split[1])?;
        let min_line_of_code_for_cve_code = instance_name_split[2].parse::<u8>().unwrap();

        Some(Self::new(language, generator, min_line_of_code_for_cve_code))
    }

    pub fn new(
        language : Language, 
        generator : Generator, 
        min_line_of_code_for_cve_code : u8
    ) -> Self {
        let instance_name = format!("{}-{}-{}", language, generator, min_line_of_code_for_cve_code);
        let global_result_path_o = Path::new(BENCHMARK_RESULTS_DIR_PATH.as_str());
        let result_path_o = global_result_path_o.join(instance_name.as_str());
        let result_path = result_path_o.to_str().unwrap().to_string();
        let repo_result_path = result_path_o.join("repo").to_str().unwrap().to_string();
        let global_queries_path_o = Path::new(QUERIES_DIR_PATH.as_str());
        let queries_path = global_queries_path_o.join(instance_name.as_str()).to_str().unwrap().to_string();

        let queries = if fs::metadata(queries_path.as_str()).is_ok() {
            Self::load_all_query_paths(queries_path.as_str()).unwrap()
        } else {
            Vec::new()
        };

        Self {
            name : instance_name,
            language,
            generator,
            min_line_of_code_for_cve_code,
            result_path,
            repo_result_path,
            queries_path,

            all_query_paths: queries,
        }
    }

    pub fn get_name(&self) -> &str {
        self.name.as_str()
    }

    pub fn get_language(&self) -> &Language {
        &self.language
    }

    pub fn get_generator(&self) -> &Generator {
        &self.generator
    }

    pub fn get_result_path(&self) -> &str {
        self.result_path.as_str()
    }

    pub fn get_repo_result_path(&self) -> &str {
        self.repo_result_path.as_str()
    }

    pub fn get_queries_path(&self) -> &str {
        self.queries_path.as_str()
    }

    pub fn get_all_query_paths(&self) -> &Vec<String> {
        &self.all_query_paths
    }

    pub fn get_combined_query_path(&self) -> String {
        let mut combined_query_path = self.queries_path.clone();
        combined_query_path.push_str("/combined_queries.scm");
        combined_query_path
    }

    fn load_all_query_paths(path : &str) -> Result<Vec<String>, io::Error> {
        let query_paths_to_test = fs::read_dir(path)?
                    .map(|res| {
                        res.map(|e| {
                            e.path()
                                .to_str()
                                .unwrap()
                                .to_string()
                        })
                    })
                    .collect::<Result<Vec<String>, io::Error>>()?;
        let mut query_paths_to_test : Vec<String> = query_paths_to_test.into_iter().filter(|x| x.contains(".scm")).collect();
        query_paths_to_test.sort();
        Ok(query_paths_to_test)
    }

    pub fn get_min_line_of_code_for_cve_code(&self) -> u8 {
        self.min_line_of_code_for_cve_code
    }

    pub fn get_dataset_instance(&self) -> DatasetInstance {
        DatasetInstance::new(self.language.clone())
    }
}