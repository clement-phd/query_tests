use std::collections::HashMap;
use std::fs;
use std::path::Path;

use generate_query::errors::error::{GenerateQueryErrorPayload, GenerateQueryErrorType};
use log::debug;
use plot_helper::data::plottable::sample::Sample;
use plot_helper::data::plottable::{Plottable, PlottableSamples, PlottableStruct};
use plot_helper::static_html::presentation_data::{Collapsable, Text, TextContent, TextLink};
use serde::{Deserialize, Serialize};

use crate::data::hunk_serie::HunkSample;
use crate::data::keys::HunkSerieKey;



/// used to store the stats of the queries in csv
#[derive(Debug, Clone, Deserialize, Serialize)]
struct DatasetPatchStatsWrapper {
    language : String,
    cve : String,
    #[serde(rename(serialize = "# of queries total", deserialize = "# of queries total"))]
    total_files_number : u64,
    #[serde(rename(serialize = "min # of line for the context", deserialize = "min # of line for the context"))]
    threshold : String,
    #[serde(rename(serialize = "# of queries with too small context", deserialize = "# of queries with too small context"))]
    context_threshold_error_number : u64,
    #[serde(rename(serialize = "# of queries including all the file", deserialize = "# of queries including all the file"))]
    all_the_file_error_number : u64,
    #[serde(rename(serialize = "# of queries with too much char", deserialize = "# of queries with too much char"))]
    too_much_character_error_number : u64,
    #[serde(rename(serialize = "# of queries rejected by tree sitter", deserialize = "# of queries rejected by tree sitter"))]
    malformed_error_number : u64,
    #[serde(rename(serialize = "# of queries kept for the study", deserialize = "# of queries kept for the study"))]
    kept_files_number : u64,
}

impl DatasetPatchStatsWrapper {
    pub fn new(
        language : &str,
        cve : &str,
        threshold : u64,
    ) -> DatasetPatchStatsWrapper {
        DatasetPatchStatsWrapper {
            language : language.to_string(),
            cve : cve.to_string(),
            
            total_files_number : 0,
            
            threshold : threshold.to_string(),
            
            all_the_file_error_number : 0,
            context_threshold_error_number : 0,
            too_much_character_error_number : 0,
            malformed_error_number : 0,
            kept_files_number : 0,
            
        }
    }

    pub fn add_error(&mut self, error : &GenerateQueryErrorPayload) {
        match error.get_type_error() {
            GenerateQueryErrorType::AllTheFile => self.all_the_file_error_number += 1,
            GenerateQueryErrorType::ContextThreshold => self.context_threshold_error_number += 1,
            GenerateQueryErrorType::TooMuchCharacter => self.too_much_character_error_number += 1,
            GenerateQueryErrorType::Malformed => self.malformed_error_number += 1,
        }
        self.total_files_number += 1;
    }

    pub fn add_kept(&mut self) {
        self.kept_files_number += 1;
        self.total_files_number += 1;
    }
}



pub fn gen_hunk_data(data : &PlottableStruct<HunkSample, HunkSerieKey>, output_dir_path : &str) -> Result<(), Box<dyn std::error::Error>> {
    let folder_path_o = Path::new(output_dir_path).join("hunk_data");
    fs::create_dir_all(folder_path_o.to_str().unwrap())?;

    // patch informations
    {
        let save_folder_path_o = folder_path_o.join("hunk_detailed_informations");
        fs::create_dir_all(save_folder_path_o.to_str().unwrap())?;
        {
            let all_languages = data.get_string_series(&HunkSerieKey::Language);
            let mut unique_languages: Vec<String> = data.get_string_series(&HunkSerieKey::Language)
                    .into_iter()
                    .collect::<std::collections::HashSet<String>>()
                    .into_iter()
                    .collect();
            unique_languages.sort();

            let all_thresholds = data.get_numeric_series(&HunkSerieKey::MinQueryLineOfCode);
            let mut unique_thresholds: Vec<u64> = data.get_numeric_series(&HunkSerieKey::MinQueryLineOfCode)
                .into_iter()
                .map(|f| f as u64)
                .collect::<std::collections::HashSet<u64>>()
                .into_iter()
                .collect();
            unique_thresholds.sort();

            for language in unique_languages {
                let language_path_o = save_folder_path_o.join(format!("{}.csv", language));
                let mut csv_writer = csv::Writer::from_path(language_path_o.to_str().unwrap())?;
                for threshold in unique_thresholds.iter() {                   

                    let cves = 
                        data.get_string_series(&HunkSerieKey::Label)
                        .iter().enumerate().filter(|(i, _)| all_languages[*i] == language && all_thresholds[*i] as u64 == *threshold)
                        .map(|(_, cve)| cve.clone()).collect::<Vec<_>>();

                    let error = 
                        data.get_samples()
                        .iter().enumerate().filter(|(i, _)| all_languages[*i] == language && all_thresholds[*i] as u64 == *threshold)
                        .map(|(_, hunk)| hunk.get_query_error().clone()).collect::<Vec<_>>();

                    let mut result : HashMap<String, DatasetPatchStatsWrapper> = HashMap::new();

                    for (_cve, error) in cves.iter().zip(error.iter()) {

                        /*let stats = result.entry(cve.clone()).or_insert_with(|| {
                            DatasetPatchStatsWrapper::new(
                                language.get_program_arg().as_str(),
                                cve,
                                *threshold
                            )
                        });

                        stats.add_error(error);*/

                        let tot_stats = result.entry("total".to_string()).or_insert_with(|| {
                            DatasetPatchStatsWrapper::new(
                                language.as_str(),
                                "total",
                                *threshold
                            )
                        });
                        if let Some(error) = error {
                            tot_stats.add_error(error);
                        }else{
                            tot_stats.add_kept();
                        }
                    }

                    let mut keys = result.keys().collect::<Vec<_>>();
                    keys.sort();

                    for key in keys { // total is the last one
                        csv_writer.serialize(result.get(key).unwrap())?;
                    }
                }
            }
            

        }
    }

    // error tree sitter unnexpected
    {
        let save_folder_path_o = folder_path_o.join("hunk_error_tree_sitter_unnexpected");
        fs::create_dir_all(save_folder_path_o.to_str().unwrap())?;
        {
            let all_languages = data.get_string_series(&HunkSerieKey::Language);
            let mut unique_languages: Vec<String> = data.get_string_series(&HunkSerieKey::Language)
                    .iter()
                    .map(|f| f.clone())
                    .collect::<std::collections::HashSet<String>>()
                    .into_iter()
                    .collect();
            unique_languages.sort();

            let all_thresholds = data.get_numeric_series(&HunkSerieKey::MinQueryLineOfCode);
            let mut unique_thresholds: Vec<u64> = data.get_numeric_series(&HunkSerieKey::MinQueryLineOfCode)
                    .iter()
                    .map(|f| *f as u64)
                    .collect::<std::collections::HashSet<u64>>()
                    .into_iter()
                    .collect();
            unique_thresholds.sort();

            let mut content : Vec<TextContent> = Vec::new();

            for language in unique_languages {                
                let collapsable_title = format!("Error tree sitter unnexpected for language {} (without warning): ", language);
                let ref_threshold = unique_thresholds[0];  

                let samples = 
                    data.get_samples()
                    .iter().enumerate().filter(|(i, _)| all_languages[*i] == language && all_thresholds[*i] as u64 == ref_threshold)
                    .map(|(_, hunk)| hunk).collect::<Vec<_>>();                

                let mut collapsable_content : Vec<TextContent> = Vec::new();
                for sample in samples {
                    match sample.get_query_error() {
                        None => {},
                        Some(e) => {
                            match e.get_type_error() {
                                GenerateQueryErrorType::Malformed => {
                                    if sample.get_warnings().is_some() {
                                        continue;
                                    }
                                    debug!("Malformed error for {}", sample.get_string_value(&HunkSerieKey::CveCodePath));
                                    let absolute_path = fs::canonicalize(sample.get_string_value(&HunkSerieKey::CveCodePath))?;
                                    collapsable_content.push(TextLink::new(
                                        absolute_path.to_str().unwrap().to_string(),
                                        format!("{}", sample.get_string_value(&HunkSerieKey::CveCodePath))
                                    ).into());
                                    collapsable_content.push(format!(": generator {}\n", sample.get_string_value(&HunkSerieKey::Generator)).into());
                                },
                                _ => {}
                            }
                        }
                    }
                    
                    
                }

                if collapsable_content.len() == 0 {
                    collapsable_content.push("None".into());
                }
                
                content.push(Collapsable::new(collapsable_title, collapsable_content).into());
                
                
            }
            let text : Text = content.into();
            text.save_to_file(save_folder_path_o.to_str().unwrap(), "error_tree_sitter_unnexpected")?;
        }
    }

    Ok(())
}