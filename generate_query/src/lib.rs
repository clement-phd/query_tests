// entry point for the library crate
// get the params from the command line to have access to the different generators
// get the pipeline to generate the query

pub mod params;
mod generators;
pub mod pipeline;
pub mod errors;
pub mod data;