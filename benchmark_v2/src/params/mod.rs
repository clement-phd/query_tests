use std::env;
use std::path::Path;

use generate_query::params::argv::Generator;
use lazy_static::lazy_static;


pub mod argv;


// --------------------- CONSTANTS ---------------------//

/// The default directory where the benchmark data is stored
const BENCHMARK_DATA_DIR_DEFAULT: &str = "benchmark_data/";

/// The directory where the repo are stored
/// NOTE : inside the benchmark_data directory
const REPOS_DIR: &str = "repos/";

/// The directory where the benchmark results are stored
/// NOTE : inside the benchmark_data directory
const BENCHMARK_RESULTS_DIR: &str = "benchmark_results/";

/// The directory where the dataset preparation results are stored
/// NOTE : inside the benchmark_data directory
const DATASET_PREPARATION_OUTPUT_DIR: &str = "dataset_preparation/";

/// The directory where the queries are stored
/// NOTE : inside the benchmark_data directory
const QUERIES_DIR: &str = "queries/";

// --------------------- Public ---------------------//

pub const GITHUB_URL: &'static str = "git@github.com";

#[cfg(feature = "memory_monitoring")]
pub const MONITORING_INTERVAL_MILLI: u64 = 5;

pub const MAX_COMMIT_TO_CHECK: u32 = 0;

lazy_static! {
    //--------------------- PATHS ---------------------//

    /// The directory where the benchmark data is stored
    pub static ref BENCHMARK_DATA_DIR_PATH: String = {
        let env_var = env::var("BENCHMARK_DATA_DIR").unwrap_or_else(|_| BENCHMARK_DATA_DIR_DEFAULT.to_string());
        if env_var.ends_with("/") {
            env_var
        } else {
            format!("{}/", env_var)
        }
    };

    /// The directory where the repos are stored
    pub static ref REPOS_DIR_PATH: String = {
        let path_o = Path::new(&*BENCHMARK_DATA_DIR_PATH).join(REPOS_DIR);
        path_o.to_str().unwrap().to_string()
    };

    /// The directory where the benchmark results are stored
    pub static ref BENCHMARK_RESULTS_DIR_PATH: String = {
        let path_o = Path::new(&*BENCHMARK_DATA_DIR_PATH).join(BENCHMARK_RESULTS_DIR);
        path_o.to_str().unwrap().to_string()
    };

    /// The directory where the dataset preparation results are stored
    pub static ref DATASET_PREPARATION_OUTPUT_DIR_PATH: String = {
        let path_o = Path::new(&*BENCHMARK_DATA_DIR_PATH).join(DATASET_PREPARATION_OUTPUT_DIR);
        path_o.to_str().unwrap().to_string()
    };

    /// The directory where the queries are stored
    pub static ref QUERIES_DIR_PATH: String = {
        let path_o = Path::new(&*BENCHMARK_DATA_DIR_PATH).join(QUERIES_DIR);
        path_o.to_str().unwrap().to_string()
    };

    // --------------------- COMPUTATION PARAMS ---------------------//

    /// The thresholds to tests
    pub static ref MIN_LINE_OF_CODE_FOR_CVE_CODE_TO_TEST : Vec<u8> = vec![0/*, 5, 10, 15, 20, 25, 30, 35, 40, 50*/];

    /// The generator to test
    pub static ref GENERATORS_TO_TEST : Vec<Generator> = vec![Generator::AstRange/*, Generator::ToSexp*/];

    pub static ref REPO_TO_TESTS : Vec<(&'static str, &'static str)> = vec![
        ("systemd/systemd", "738ad08b0db11d7e66c14ff4b9852cf16abf3aa9"),
        ("uriparser/uriparser", "70383a4f3a2d0b6fba8576fffceea17a044a479e"),
        ("rust-lang/rust", "af88f7db51f6f2a1472f9279d7c7e7c822afff77")
    ];
}