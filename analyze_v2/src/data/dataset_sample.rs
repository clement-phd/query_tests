use std::fs;
use std::path::Path;

use plot_helper::data::plottable::sample::{Sample, SimpleSample};
use plot_helper::data::plottable::{PlottableSimpleSamplesFromPaths, PlottableStruct};
use rust_utils::tree_sitter_utils::languages::Language;
use rust_utils::utils::get_all_files;
use serde::{Deserialize, Serialize};


use dataset_preparation::cve::CveInfoWrapper;

use super::keys::DatasetSerieCveKey;


#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct DatasetSample {
    cve : String,
    number_files: u64,
    number_other_than_language: u64,
    number_other_than_modified_files: u64,
    total_relevant_files: u64,
    language : Language,
}

impl Sample<DatasetSerieCveKey> for DatasetSample {
    fn get_numeric_value(&self, key : &DatasetSerieCveKey) -> f32 {
        match key {
            DatasetSerieCveKey::Cve => unreachable!("cve is not numeric"),
            DatasetSerieCveKey::NumberFiles => self.number_files as f32,
            DatasetSerieCveKey::NumberOtherThanLanguage => self.number_other_than_language as f32,
            DatasetSerieCveKey::NumberOtherThanModifiedFiles => self.number_other_than_modified_files as f32,
            DatasetSerieCveKey::TotalRelevantFiles => self.total_relevant_files as f32,
            DatasetSerieCveKey::Language => unreachable!("language is not numeric"),
        }
    }

    fn get_string_value(&self, key : &DatasetSerieCveKey) -> String {
        match key {
            DatasetSerieCveKey::Cve => self.cve.clone(),
            DatasetSerieCveKey::NumberFiles => unreachable!("number files is not string"),
            DatasetSerieCveKey::NumberOtherThanLanguage => unreachable!("number other than language is not string"),
            DatasetSerieCveKey::NumberOtherThanModifiedFiles => unreachable!("number other than modified files is not string"),
            DatasetSerieCveKey::TotalRelevantFiles => unreachable!("total relevant files is not string"),
            DatasetSerieCveKey::Language => self.language.to_string(),
        }
    }
}

impl SimpleSample<DatasetSerieCveKey> for DatasetSample {
    fn new_from_file_path(file_path : &str) -> Result<Self, Box<dyn std::error::Error>> {
        let file_content = fs::read_to_string(file_path).unwrap();
        let commit_result : CveInfoWrapper = serde_json::from_str(&file_content).unwrap();
        Ok(DatasetSample {
            cve : commit_result.get_cve_id().to_string(),
            number_files: commit_result.get_files_inter_path().len() as u64,
            number_other_than_language: commit_result.get_other_than_language_files_intern_path().len() as u64,
            number_other_than_modified_files: commit_result.get_other_than_modified_files_inter_path().len() as u64,
            total_relevant_files: commit_result.get_files_inter_path().len() as u64 + commit_result.get_other_than_modified_files_inter_path().len() as u64,
            language : commit_result.get_language().clone(),
        })
    }
}

pub fn load_plottable_dataset(data_dir : &str) -> PlottableStruct<DatasetSample, DatasetSerieCveKey> {
    let data_dir_o = Path::new(data_dir);
    let dataset_prep_dir_o = data_dir_o.join("dataset_preparation");
    let files = get_all_files(dataset_prep_dir_o.to_str().unwrap());

    // isolate dataset preparation results
    let all_repo_results_path : Vec<String> = files.iter().filter_map(|entry| {
        let path_o = Path::new(entry);
        if path_o.file_name().unwrap().to_str().unwrap() == "cve_info.json" {
            return Some(entry.clone());
        }else{
            return None;
        }
    })
    .collect();

    PlottableStruct::new_async_from_paths(&all_repo_results_path)
}