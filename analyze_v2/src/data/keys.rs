use std::fmt::{Display, Formatter};

use plot_helper::generate_plot_key;
use serde_derive::{Deserialize, Serialize};
use plot_helper::data::plottable::key::SerieKey;

generate_plot_key!(
    MultiLineQueryKey[
        ParsingTime { "parsing time (s)", Numeric},
        ParsingMaxMemory { "parsing max memory (Mb)", Numeric },
        File { "file", String },
        MinQueryLineOfCode { "# lines context threshold", Numeric },
        Language { "language", String },
        Generator { "generator", String },
        FileNbOfLines { "file # lines", Numeric },
        FileNbOfChars { "file # chars", Numeric },
        QueryNbOfLines { "query # lines", Numeric },
        QueryTime { "query time (s)", Numeric },
        QueryMaxMemory { "query max memory (Mb)", Numeric },
        QueryOutput { "query output", String },
        MatchsNumber { "matchs number", Numeric },
        TimeDifferenceSingleMultiLineQuery { "Single line query time against multi one (%)", Numeric },
        TruePositive { "true positive", Numeric },
        FalsePositive { "false positive", Numeric },
        FalseNegative { "false negative", Numeric },
        TrueNegative { "true negative", Numeric },
        FilteredTruePositive { "filtered true positive", Numeric },
        FilteredFalsePositive { "filtered false positive", Numeric },
        FilteredFalseNegative { "filtered false negative", Numeric },
        FilteredTrueNegative { "filtered true negative", Numeric },
        NonBiasedTruePositive { "non biased true positive", Numeric },
        NonBiasedFalsePositive { "non biased false positive", Numeric },
        NonBiasedFalseNegative { "non biased false negative", Numeric },
        NonBiasedTrueNegative { "non biased true negative", Numeric }
    ],
    SingleLineQueryKey[
        ParsingTime { "parsing time (s)", Numeric },
        File { "file", String },
        MinQueryLineOfCode { "# lines context threshold", Numeric },
        Language { "language", String },
        FileNbOfLines { "file # lines", Numeric },
        FileNbOfChars { "file # chars", Numeric },
        QueryTime { "query time (s)", Numeric },
        QueryMaxMemory { "query max memory (Mb)", Numeric },
        Generator { "generator", String }
    ],
    CorrectnessQueryLinesKey[
        Language { "language", String },
        Generator { "generator", String },
        MinQueryLineOfCode { "# lines context threshold", Numeric },
        TruePositive { "true positive", Numeric },
        FalsePositive { "false positive", Numeric },
        FalseNegative { "false negative", Numeric },
        TrueNegative { "true negative", Numeric },
        Recall { "recall", Numeric },
        Precision { "precision", Numeric },
        F1 { "F1", Numeric },
        Accuracy { "accuracy", Numeric }
    ],
// ----------------------------------- Repo keys -----------------------------------
    RepoKey[
        Repo { "repo", String },
        Commit { "commit", String },
        ExecutionTime { "execution time (s)", Numeric },
        FileNumber { "file number", Numeric },
        LineNumber { "line number", Numeric },
        CharNumber { "char number", Numeric },
        MinQueryLineOfCode { "# lines context threshold", Numeric },
        Language { "language", String }
    ],
// ----------------------------------- Dataset keys -----------------------------------
    DatasetSerieCveKey[
        Cve { "cve", String },
        NumberFiles { "number files", Numeric },
        NumberOtherThanLanguage { "number other than language", Numeric },
        NumberOtherThanModifiedFiles { "number other than modified files", Numeric },
        TotalRelevantFiles { "total relevant files", Numeric },
        Language { "language", String }
    ],
// ----------------------------------- Hunk keys -----------------------------------
    HunkSerieKey[
        Language { "language", String },
        MinQueryLineOfCode { "# lines context threshold", Numeric },
        Label { "cve label", String },
        Generator { "generator", String },
        CveCodePath { "cve code path", String }
    ],
// ----------------------------------- MemoryMonitoring keys -----------------------------------
    MemoryMonitoringSerieKey[
        Time { "time (s)", Numeric },
        Memory { "Memory (b)", Numeric },
        MemoryType { "Memory type", String }
    ]
);