use std::fs;
use std::path::Path;

use serde_derive::{Serialize, Deserialize};

use rust_utils::tree_sitter_utils::languages::Language;

use self::cve::Cve;


pub mod cve;

/// A file to test against the query
#[derive(Clone, PartialEq, Eq, Hash, Serialize, Deserialize, Debug)]
pub struct FileToTest {
    /// The file path
    pub file_path: String,
    /// cve id
    pub cve_id: String,
    /// if the file is infected
    pub is_infected: bool,
}


/// The prepared dataset
#[derive(Clone, PartialEq, Eq, Hash, Serialize, Deserialize, Debug)]
pub struct Dataset {
    cves: Vec<Cve>,
}

impl Dataset {
    /// Create a new Dataset from the path of the folder directly above the cve folders and a language
    pub fn get_dataset(path : &str, language: &Language) -> Dataset {
        let cve_folders = Dataset::find_cve_folders(Path::new(path));

        let mut cves: Vec<Cve> = cve_folders
            .iter()
            .map(|folder| Cve::from_cve_folder_path(folder, &language))
            .collect();

        // Sorting CVEs based on their IDs
        cves.sort_by(|a, b| a.cve_id.cmp(&b.cve_id));

        Dataset { cves }
    }

    fn find_cve_folders(root_folder: &Path) -> Vec<String> {
        let mut cve_folders = Vec::new();

        for entry in fs::read_dir(root_folder).expect("Directory not found") {
            let entry = entry.expect("Failed to read entry");
            let path = entry.path();
            if path.is_dir() && path.file_name().unwrap().to_str().unwrap().starts_with("CVE") {
                cve_folders.push(path.to_str().unwrap().to_string());
            }
        }
        cve_folders
    }

    pub fn get_files_to_test(&self) -> Vec<FileToTest> {
        let mut files_to_test = Vec::new();

        for cve in &self.cves {
            for cve_file in &cve.cve_files {
                files_to_test.push(FileToTest {
                    file_path: cve_file.original_file_path.clone(),
                    cve_id: cve.cve_id.clone(),
                    is_infected: true,
                });
                files_to_test.push(FileToTest {
                    file_path: cve_file.fixed_file_path.clone(),
                    cve_id: cve.cve_id.clone(),
                    is_infected: false,
                });
            }
        }

        files_to_test
    }
}