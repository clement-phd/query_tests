use clap::Parser;
use rust_utils::tree_sitter_utils::languages::Language;
/// Apply a tree sitter query to a file
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub struct Argv {
    /// The path to the file to parse to query
    #[arg(short, long, required_unless_present = "files_to_tests_file_path")]
    pub file_to_test_path: Option<String>,

    /// The path to a file containing the list of files to test, one file per line
    #[arg(short = 'd', long, required_unless_present = "file_to_test_path")]
    pub files_to_tests_file_path: Option<String>,

    /// The path to the file to get the query (either the query itself, or the source code of the query)
    #[arg(short, long)]
    pub query_path: String,

    /// The language of the file to test
    #[arg(short, long, value_enum)]
    pub language: Language,
}


pub fn get_program_args() -> Argv {
    return Argv::parse();
}