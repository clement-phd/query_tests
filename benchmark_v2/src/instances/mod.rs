use std::collections::VecDeque;

use generate_query::params::argv::Generator;
use rust_utils::tree_sitter_utils::languages::Language;

use crate::params::{GENERATORS_TO_TEST, MIN_LINE_OF_CODE_FOR_CVE_CODE_TO_TEST};

use self::dataset::DatasetInstance;
use self::instance::Instance;

pub mod instance;
pub mod dataset;

/// all the dataset instances
#[derive(Debug)]
pub struct DatasetInstances {
    dataset_instances : VecDeque<DatasetInstance>,
}

impl DatasetInstances {
    pub fn new() -> Self {
        let mut all_dataset_instances = VecDeque::new();
        let all_languages = Language::get_all_languages();
        all_dataset_instances.reserve(all_languages.len());

        for language in &all_languages {
            let dataset_instance = DatasetInstance::new(language.clone());
            all_dataset_instances.push_back(dataset_instance);
        }

        Self {
            dataset_instances : all_dataset_instances,
        }
    }

    pub fn len(&self) -> usize {
        self.dataset_instances.len()
    }
}

impl Iterator for DatasetInstances {
    type Item = DatasetInstance;

    fn next(&mut self) -> Option<Self::Item> {
        self.dataset_instances.pop_front()
    }
}



/// Instances is a struct that contains a list of instances to test the pipeline. This is an iterator
#[derive(Debug)]
pub struct Instances {
    instances : VecDeque<Instance>,

}


impl Instances {
    /// construct an iterator of instances. 
    /// NOTE : It's garanty that all the instances with the same dataset are at back to back in the iterator.
    /// If an instance is poped with a different dataset, it's garanty that the previous dataset will not return.
    pub fn new() -> Self {
        let mut all_instances = VecDeque::new();
        

        let all_languages = Language::get_all_languages();
        //let all_generators = Generator::get_all_generators();
        all_instances.reserve(all_languages.len() * GENERATORS_TO_TEST.len() * MIN_LINE_OF_CODE_FOR_CVE_CODE_TO_TEST.len());

        for language in &all_languages {
            for min_line_of_code_for_cve_code in &*MIN_LINE_OF_CODE_FOR_CVE_CODE_TO_TEST {
                for generator in &*GENERATORS_TO_TEST {
                    let instance = Instance::new(language.clone(), generator.clone(), *min_line_of_code_for_cve_code);
                    all_instances.push_back(instance);
                }
            }
        }



        Self {
            instances : all_instances,
        }
    }

    pub fn get_all_instances() -> Vec<Instance> {
        Instances::new().collect()
    }

    pub fn len(&self) -> usize {
        self.instances.len()
    }
}

impl Iterator for Instances {
    type Item = Instance;

    fn next(&mut self) -> Option<Self::Item> {
        self.instances.pop_front()
    }
}