use clap::Parser;
use rust_utils::tree_sitter_utils::languages::Language;

/// Prepare the CVE dataset to be used by the detection algorithm. The cve dataset is a 
/// folder which must contain one folder per CVE (nammed by the cve id), 
/// and each of these folder contain a file `data.json` describing the pull request github 
/// (from the api), and a folder `files/` containing 3 other folders, `fixed/` (code fixed), 
/// `original/` (code infected by the cve) and `patch/` (the patch to apply to the original 
/// file to obtain the fixed one, and is to the format `file_name.patch`). The script will 
/// then generate one folder by cve containing one folder by file, and each of these folder
/// contain the original file, the fixed file, and the cve code. It generate also a folder
/// `cve_codes/` containing all the cve codes found in the files.
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub struct Argv {
   /// The path to the input dataset folder (folder containing the cve folders)
   #[arg(short, long)]
   pub input_dataset_folder_path: String,

    /// The path to the output dataset folder
    #[arg(short, long)]
    pub output_dataset_folder_path: String,

    /// Language of the files to parse
    #[arg(short, long, value_enum)]
    pub language: Language,
}



pub fn get_program_args() -> Argv {
    return Argv::parse();
}