fn main() {
    let a = 10;
    let b = 5;

    let result = add(a, b);
    println!("The result of {} + {} is {}", a, b, result);
}

fn add(x: i32, y: i32) -> i32 {
    x + y
}