use std::fs;
use std::path::Path;

use plot_helper::data::plot_data::Layout;
use plot_helper::data::plottable::PlottableStruct;

use crate::data::keys::MemoryMonitoringSerieKey;
use crate::data::memory_sample::MemorySample;

use super::plot_wrapper;







pub fn gen_memory_data(data : &PlottableStruct<MemorySample, MemoryMonitoringSerieKey>, output_dir_path : &str) -> Result<(), Box<dyn std::error::Error>> {
    let folder_path_o = Path::new(output_dir_path).join("memory_data");
    fs::create_dir_all(folder_path_o.to_str().unwrap())?;

    {
        let save_folder_path_o = folder_path_o.join("memory_time_progression");
        fs::create_dir_all(save_folder_path_o.to_str().unwrap())?;
        {
            plot_wrapper(
                data,
                Some(MemoryMonitoringSerieKey::MemoryType),
                save_folder_path_o.to_str().unwrap(),
                "memory_time_progression.png",
                &Layout::new(1, 1),
                vec![
                    (MemoryMonitoringSerieKey::Time, MemoryMonitoringSerieKey::Memory, None),
                ],
                vec![]
            )?;
        }
    }
    

    Ok(())
}