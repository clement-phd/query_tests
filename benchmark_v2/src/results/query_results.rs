use std::fs;

use serde_derive::{Serialize, Deserialize};
use use_query::run_query::query_output::QueryOutput;

use crate::monitoring::MonitoringResults;



/// The result and stats of the run of a query on a file
#[derive(Clone, PartialEq, Serialize, Deserialize, Debug)]
pub struct QueryResult {
    
    pub query_output: Vec<QueryOutput>,
    /// The content of the query, line by line
    pub query_content_lines: Vec<String>,
    // Stats
    pub query_time: f64,
    pub query_max_memory: f64,

    pub query_parsing_time: f64,
    pub query_parsing_max_memory: f64,
    

    pub query_nb_of_lines: u32,
    
    
}


impl QueryResult {
    pub fn new(
        query_output : Vec<QueryOutput>, 
        query_monitoring_stats : MonitoringResults,
        query_parsing_time : MonitoringResults,
        query_path : String,
    ) -> QueryResult {
        let query_content = fs::read_to_string(query_path).unwrap();
        let query_content_lines : Vec<String> = query_content.lines().map(|s| s.to_string()).collect();
        let query_nb_of_lines = query_content_lines.len() as u32;
        QueryResult {
            query_time: query_monitoring_stats.time.as_secs_f64(),
            query_max_memory: 0.0,
            query_parsing_time: query_parsing_time.time.as_secs_f64(),
            query_parsing_max_memory: 0.0,
            query_output,
            query_content_lines: query_content_lines,
            query_nb_of_lines,
        }
    }

    pub fn get_all_cve_names(&self) -> Vec<String> {
        let mut cve_names : Vec<String> = Vec::new();
        for line in &self.query_content_lines {
            let mut spliting = line.split("@");
            spliting.next();
            let cve_part = spliting.next().unwrap();

            // remove the last "_" part
            let cve = match cve_part.rfind('_') {
                Some(index) => cve_part[..index].to_string(),
                None => cve_part.to_string(), // return the original string if no underscore is found
            };
            cve_names.push(cve);
        }
        cve_names
    }
}