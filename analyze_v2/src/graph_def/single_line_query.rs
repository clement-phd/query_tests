use std::fs;
use std::path::Path;

use log::debug;
use plot_helper::data::filtering::Filter;
use plot_helper::data::plot_data::Layout;
use plot_helper::data::plottable::{Plottable, PlottableStruct};
use plot_helper::stats::stats_serie::MetricName;
use plot_helper::static_html::presentation_data::{Collapsable, TextContent, TextLink, Text};
use rust_utils::tree_sitter_utils::languages::Language;

use crate::data::keys::SingleLineQueryKey;
use crate::data::single_line_query_sample::{MultiLineQueryPlottable, SingleLineQuerySample};

use super::{plot_wrapper, QueryStatsWrapper};






pub fn gen_single_line_query_data(data : &PlottableStruct<SingleLineQuerySample, SingleLineQueryKey>, output_dir_path : &str) -> Result<(), Box<dyn std::error::Error>> {
    let folder_path_o = Path::new(output_dir_path).join("single_line_query");
    fs::create_dir_all(folder_path_o.to_str().unwrap())?;
    debug!("Generating single line query data");
    // separate on generator
    let mut sorted_generators: Vec<_> = 
        data.get_string_series(&SingleLineQueryKey::Generator).into_iter()
            .collect::<std::collections::HashSet<_>>().into_iter().collect();
    sorted_generators.sort();
    
    for generator in sorted_generators {
        let folder_path_o = folder_path_o.join(generator.as_str());
        fs::create_dir_all(folder_path_o.to_str().unwrap())?;
        let generator_filter = Filter::new_str(
            SingleLineQueryKey::Generator, 
            move |g : &str| g == generator.as_str()
        );
        // performance comparaison language
        {
            debug!("Generating performance comparaison language");
            let save_folder_path_o = folder_path_o.join("language_comparaison");
            fs::create_dir_all(save_folder_path_o.to_str().unwrap())?;
            {
                plot_wrapper(
                    data,
                    Some(SingleLineQueryKey::Language),
                    save_folder_path_o.to_str().unwrap(),
                    "performance_comparaison_language.png",
                    &Layout::new(1, 3),
                    vec![
                        (SingleLineQueryKey::FileNbOfLines, SingleLineQueryKey::QueryTime, Some(vec![&generator_filter])),
                        //(SingleLineQueryKey::FileNbOfLines, SingleLineQueryKey::QueryMaxMemory, None),
                        (SingleLineQueryKey::FileNbOfChars, SingleLineQueryKey::QueryTime, Some(vec![&generator_filter])),
                        //(SingleLineQueryKey::FileNbOfChars, SingleLineQueryKey::QueryMaxMemory, None),
                        (SingleLineQueryKey::MinQueryLineOfCode, SingleLineQueryKey::QueryTime, Some(vec![&generator_filter])),
                        //(SingleLineQueryKey::MinQueryLineOfCode, SingleLineQueryKey::QueryMaxMemory, None),
                    ],
                    vec![SingleLineQueryKey::QueryTime/*, SingleLineQueryKey::QueryMaxMemory*/]
                )?;
            }
        }

        // performance per language
        {
            debug!("Generating performance per language");
            let save_folder_path_o = folder_path_o.join("performance_comparaison_by_language");
            fs::create_dir_all(save_folder_path_o.to_str().unwrap())?;

            let all_languages = data.get_string_series(&SingleLineQueryKey::Language);
            let unique_languages = all_languages.iter().map(|s| s.to_string()).collect::<std::collections::HashSet<_>>().into_iter().collect::<Vec<_>>();


            for language in unique_languages {
                let language_folder_path_o = save_folder_path_o.join(language.to_string());
                fs::create_dir_all(language_folder_path_o.to_str().unwrap())?;

                let save_file_name = format!("performance_{}.png", language);
                let language_filter = Filter::new_str(
                    SingleLineQueryKey::Language, 
                    move |l : &str| l == language.to_string()
                );

                plot_wrapper(
                    data,
                    Some(SingleLineQueryKey::Language),
                    language_folder_path_o.to_str().unwrap(),
                    save_file_name.as_str(),
                    &Layout::new(1, 3),
                    vec![
                        (SingleLineQueryKey::FileNbOfLines, SingleLineQueryKey::QueryTime, Some(vec![&language_filter, &generator_filter])),
                        //(SingleLineQueryKey::FileNbOfLines, SingleLineQueryKey::QueryMaxMemory, Some(vec![&language_filter])),
                        (SingleLineQueryKey::FileNbOfChars, SingleLineQueryKey::QueryTime, Some(vec![&language_filter, &generator_filter])),
                        //(SingleLineQueryKey::FileNbOfChars, SingleLineQueryKey::QueryMaxMemory, Some(vec![&language_filter])),
                        (SingleLineQueryKey::MinQueryLineOfCode, SingleLineQueryKey::QueryTime, Some(vec![&language_filter, &generator_filter])),
                        //(SingleLineQueryKey::MinQueryLineOfCode, SingleLineQueryKey::QueryMaxMemory, Some(vec![&language_filter])),
                    ],
                    vec![SingleLineQueryKey::QueryTime/*, SingleLineQueryKey::QueryMaxMemory*/]
                )?;
            }
        }
    }

    // more informations
    {
        debug!("Generating more informations");
        let save_folder_path_o = folder_path_o.join("detailed_language_informations");
        fs::create_dir_all(save_folder_path_o.to_str().unwrap())?;

        {
            let stats = data.collect_stats_sorted_by_unique_values( 
                &vec![
                    SingleLineQueryKey::QueryTime,
                    SingleLineQueryKey::QueryMaxMemory,
                    SingleLineQueryKey::ParsingTime,
                    SingleLineQueryKey::FileNbOfLines,
                ],
                &SingleLineQueryKey::Language
            );

            let mut stats_stored : Vec<QueryStatsWrapper> = Vec::new();

            for (language, computed_stats) in stats {
                stats_stored.push(
                    QueryStatsWrapper::new (
                        language.clone(),
                        computed_stats.get(&SingleLineQueryKey::ParsingTime).unwrap().stats.get(&MetricName::Mean).unwrap().value,
                        computed_stats.get(&SingleLineQueryKey::ParsingTime).unwrap().stats.get(&MetricName::Median).unwrap().value,
                        computed_stats.get(&SingleLineQueryKey::QueryTime).unwrap().stats.get(&MetricName::Mean).unwrap().value,
                        computed_stats.get(&SingleLineQueryKey::QueryTime).unwrap().stats.get(&MetricName::Median).unwrap().value,
                        computed_stats.get(&SingleLineQueryKey::FileNbOfLines).unwrap().stats.get(&MetricName::Mean).unwrap().value,
                    )
                )
            }

            let file_path = save_folder_path_o.join(format!("stats.csv"));
            let mut csv_writer = csv::Writer::from_path(file_path.to_str().unwrap())?;
            for stat in stats_stored {
                csv_writer.serialize(stat)?;
            }

            csv_writer.flush()?;
        }
    }

    {
        debug!("Generating outliers");
        let save_folder_path_o = folder_path_o.join("outliers");
        fs::create_dir_all(save_folder_path_o.to_str().unwrap())?;

        let all_file_times = data.collect_stats_sorted_by_unique_values( 
            &vec![
                SingleLineQueryKey::QueryTime,
            ],
            &SingleLineQueryKey::Language
        );
        let anomalies_single_query_time_by_cve_id = data.aggregate_anomalies_single_query_time_by_cve_id();
        let all_languages = data.get_string_series(&SingleLineQueryKey::Language);
        let unique_languages = all_languages.iter().map(|s| s.to_string()).collect::<std::collections::HashSet<_>>().into_iter().collect::<Vec<_>>();

        for language in unique_languages.iter() {
            debug!("Processing outliers for language {} ({:?})", language, unique_languages);
            let save_file_name = format!("outliers_for_{}", language);
            let mut content : Vec<TextContent> = Vec::new();
            content.push(format!("Outliers for language {} : ", language).into());
            
            let outlier = anomalies_single_query_time_by_cve_id.get(&Language::get_from_display_name(language.as_str()).unwrap());

            let ref_stats = all_file_times.get(&language.to_string()).unwrap().get(&SingleLineQueryKey::QueryTime).unwrap();
            let ref_mean = ref_stats.stats.get(&MetricName::Mean).unwrap().value;

            if outlier.is_none() {
                content.push("None".into());
            }else{
                let outlier = outlier.unwrap();
                
                for (cve_id, files) in outlier.iter() {
                    let outlier_mean = files.iter().map(|(_, time)| time).sum::<f64>() / files.len() as f64;
                    
                    let pourcent = 100.0 * (outlier_mean - ref_mean) / ref_mean;

                    let collapsable_title = 
                        format!("\t>{} ({} times) with a mean query time (on the outlier) of {:.2} s ({:.0}%)\n", cve_id, files.len(), outlier_mean, pourcent);
                    
                    let mut collapsable_content : Vec<TextContent> = Vec::new();

                    for (file, time) in files.iter() {
                        let infected = if file.is_infected {
                            "(infected)"
                        }else{
                            ""
                        };
                        let file_path = file.file_path.clone();
                        let file_name = file_path.split("dataset_preparation/").last().unwrap();

                        let absolute_path = fs::canonicalize(file.file_path.as_str())?;
                        collapsable_content.push(
                            TextLink::new(
                                absolute_path.to_str().unwrap().to_string(),
                                format!("{} {}", file_name, infected)
                            ).into()
                        );
                        collapsable_content.push(format!(": {:.2} s\n", time).into());
                    }

                    content.push(
                        Collapsable::new(
                            collapsable_title,
                            collapsable_content.into()
                        ).into()
                    );
                }
            }
            let text : Text = content.into();
            debug!("Saving file {}", save_file_name);
            text.save_to_file(save_folder_path_o.to_str().unwrap(), save_file_name.as_str())?;      
        }
    }

    Ok(())
}