use dataset_preparation::conversion::line_removed::ExtractedCveCode;
use rust_utils::tree_sitter_utils::get_tree_from_file_content;
use rust_utils::tree_sitter_utils::languages::Language;

use crate::data::ExtractedCveCodePayload;
use crate::errors::error::{GenerateQueryErrorPayload, GenerateQueryErrorType};
use crate::errors::warning::{GenerateQueryWarning, GenerateQueryWarningPayload, GenerateQueryWarningType};

trait HasRange : Sized{
    fn start_byte(&self) -> usize;
    fn end_byte(&self) -> usize;

    fn is_in_range(&self, start_byte_goal : usize, end_byte_goal : usize) -> bool {
        self.start_byte() <= start_byte_goal && self.end_byte() >= end_byte_goal
    }

    fn get_size(&self) -> usize {
        self.end_byte() - self.start_byte()
    }

    fn compare_size(&self, other : impl HasRange) -> std::cmp::Ordering {
        self.get_size().cmp(&other.get_size())
    }
}

impl<'a> HasRange for tree_sitter::Node<'a> {
    fn start_byte(&self) -> usize {
        self.start_byte()
    }

    fn end_byte(&self) -> usize {
        self.end_byte()
    }
}

/// Convert the extracted cve code into a tree sitter query. To do so, we transform the infected code
/// into an ast and get the node which contain the cve code, then convert it with tosexp
pub fn ast_range_query_converter(
    extracted_cve_code : &ExtractedCveCodePayload, 
    language : &Language, 
    min_number_of_line : usize
) -> Result<(String, Option<GenerateQueryWarning>), GenerateQueryErrorPayload> {
    let mut warnings = None;
    let tree = get_tree_from_file_content(extracted_cve_code.get_extracted_cve_code().get_original_file_content(), language);
    if tree.root_node().has_error() {
        warnings = Some(GenerateQueryWarning::new(
            extracted_cve_code.get_label(),
            vec![GenerateQueryWarningPayload::new(
                None,
                GenerateQueryWarningType::OriginalFileMalformed
            )]
        ));
    }


    let tree_cursor = &mut tree.walk();
    
    let (start_byte_goal, end_byte_goal) = get_byte_range_of_extracted_cve_code(extracted_cve_code.get_extracted_cve_code());

    let minimal_node = get_minimal_node_containing_cve_code(tree_cursor, start_byte_goal, end_byte_goal).expect("The range isn't in the tree");

    let query = minimal_node.to_sexp();
    // check if the minimal node is not the root node
    if minimal_node.start_byte() == 0 && 
        minimal_node.end_byte() == extracted_cve_code.get_extracted_cve_code().get_original_file_content().as_bytes().len()
     {
        return Err(GenerateQueryErrorPayload::new(extracted_cve_code.get_label(), GenerateQueryErrorType::AllTheFile, warnings));
    }

    // check that the node englobe at least min_number_of_line
    if minimal_node.end_position().row - minimal_node.start_position().row < min_number_of_line {
        return Err(GenerateQueryErrorPayload::new(extracted_cve_code.get_label(), GenerateQueryErrorType::ContextThreshold, warnings));
    }
    
    Ok((query, warnings))
}

fn get_byte_range_of_extracted_cve_code(extracted_cve_code : &ExtractedCveCode) -> (usize, usize) {
    let start_line = extracted_cve_code.get_start_line();
    let end_line = extracted_cve_code.get_end_line();
    let start_file = extracted_cve_code.get_original_file_content();

    let mut start_byte = 0;
    let mut end_byte = 0;
    let mut current_starting_byte = 0;
    let mut current_line_number = 0;
    for line in start_file.lines() {
        if current_line_number == start_line {
            start_byte = current_starting_byte;
        }
        if current_line_number == end_line {
            end_byte = current_starting_byte + line.as_bytes().len();
            break;
        }
        current_starting_byte = current_starting_byte + line.as_bytes().len() + 1;
        current_line_number = current_line_number + 1;
    }
    (start_byte, end_byte)
}


fn get_minimal_node_containing_cve_code<'a>(
    cursor : &mut tree_sitter::TreeCursor<'a>,
    start_byte_goal : usize,
    end_byte_goal : usize,
) -> Option<tree_sitter::Node<'a>> {

    let mut actu_node = cursor.node();

    // test if we are in the range 
    if !actu_node.is_in_range(start_byte_goal, end_byte_goal) {
        return None;
    }

    // test the children if we have

    // goto first children
    if cursor.goto_first_child() {
        // loop on siblings
        loop {
            // if we have a child node match, we save it
            if let Some(child_min_node) = get_minimal_node_containing_cve_code(cursor, start_byte_goal, end_byte_goal) {
                actu_node = child_min_node;
                break;
            }

            // else we test the next one
            if !cursor.goto_next_sibling() {
                break;
            }
        }

        // reset to parent for the recursif call
        cursor.goto_parent();
    }

    return Some(actu_node);
}


#[cfg(test)]
mod tests {
    use super::*;

    const PROGRAMMING_LANGUAGE : Language = Language::Rust;

    const TESTED_PROGRAMS_1 : &'static str = include_str!("../../tests/ressources/rust1.rs");

    #[test]
    fn test_byte_range_of_extracted_cve_code() {
        let extracted_cve_code = ExtractedCveCode::new(
            9, 
            9,
            TESTED_PROGRAMS_1.to_string()
        );
        let (start_byte, end_byte) = get_byte_range_of_extracted_cve_code(&extracted_cve_code);
        assert_eq!(start_byte, 166);
        assert_eq!(end_byte, 175);
    }

    #[test]
    fn test_minimal_node_containing_cve_code_rust1_line_test1() {
        let extracted_cve_code = ExtractedCveCode::new(
            9, 
            9,
            TESTED_PROGRAMS_1.to_string()
        );
        //println!("{}", extracted_cve_code.get_original_file_content());

        let tree = get_tree_from_file_content(extracted_cve_code.get_original_file_content(), &PROGRAMMING_LANGUAGE);
        //println!("{}", tree.root_node().to_sexp());

        let mut tree_cursor = tree.walk();
        let (start_byte_goal, end_byte_goal) = get_byte_range_of_extracted_cve_code(&extracted_cve_code);
        let minimal_node = get_minimal_node_containing_cve_code(&mut tree_cursor, start_byte_goal, end_byte_goal);
        assert!(minimal_node.is_some());
        //println!("{}", minimal_node.unwrap().to_sexp());
        assert!(minimal_node.unwrap().start_byte() <= start_byte_goal);
        assert!(minimal_node.unwrap().end_byte() >= end_byte_goal);
        let tree_sitter_minimal_node = tree.root_node().descendant_for_byte_range(start_byte_goal, end_byte_goal).unwrap();
        assert_eq!(minimal_node.unwrap(), tree_sitter_minimal_node);

    }

    #[test]
    fn test_minimal_node_containing_cve_code_rust1_line_test2() {
        let extracted_cve_code = ExtractedCveCode::new(
            5, 
            5,
            TESTED_PROGRAMS_1.to_string()
        );
        //println!("{}", extracted_cve_code.get_original_file_content());

        let tree = get_tree_from_file_content(extracted_cve_code.get_original_file_content(), &PROGRAMMING_LANGUAGE);
        //println!("{}", tree.root_node().to_sexp());

        let mut tree_cursor = tree.walk();
        let (start_byte_goal, end_byte_goal) = get_byte_range_of_extracted_cve_code(&extracted_cve_code);
        let minimal_node = get_minimal_node_containing_cve_code(&mut tree_cursor, start_byte_goal, end_byte_goal);
        assert!(minimal_node.is_some());
        //println!("{}", minimal_node.unwrap().to_sexp());
        assert_eq!(minimal_node.unwrap().child_count(), 6);
    }
}