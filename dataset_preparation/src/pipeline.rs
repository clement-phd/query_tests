use std::{fs, io};
use std::path::Path;
use cve_dataset::cve_commit::CveCommit;
use rayon::prelude::*;

use log::{error, info, debug};
use rust_utils::tree_sitter_utils::languages::Language;

use crate::conversion::line_removed::extract_cve_removed_code_changes;

fn count_cve_and_files(directory: &str) -> (usize, usize) {
    let mut cve_folder_count = 0;
    let mut cve_codes_file_count = 0;

    // Create a path from the directory string
    let path = Path::new(directory);

    // Iterate over the entries in the directory
    if let Ok(entries) = fs::read_dir(path) {
        for entry in entries {
            if let Ok(entry) = entry {
                // Check if it's a directory and if the name starts with "CVE-"
                if entry.path().is_dir() && entry.file_name().to_string_lossy().starts_with("CVE-") {
                    cve_folder_count += 1;
                }

                // Specifically handle the 'cve_codes' folder
                if entry.file_name() == "cve_codes" {
                    if let Ok(cve_codes_entries) = fs::read_dir(entry.path()) {
                        cve_codes_file_count = cve_codes_entries.filter_map(Result::ok).filter(|e| e.path().is_file()).count();
                    }
                }
            }
        }
    }

    (cve_folder_count, cve_codes_file_count)
}

pub fn prepare_dataset(
    input_dataset_folder_path : &str, 
    output_dataset_folder_path : &str, 
    language : &Language
) -> Result<(), io::Error>{

    let input_dataset_folder_path = Path::new(input_dataset_folder_path);
    let output_dataset_folder_path = Path::new(output_dataset_folder_path);
    if output_dataset_folder_path.exists(){
        if !output_dataset_folder_path.is_dir() {
            error!("The output folder path is not a folder");
            panic!();
        }
        fs::remove_dir_all(output_dataset_folder_path)?;
    }
    
    fs::create_dir_all(output_dataset_folder_path)?;


    // Read the directory, filter out non-directories, collect, and sort entries
    let mut entries: Vec<_> = input_dataset_folder_path.read_dir()
                               .unwrap()
                               .filter_map(|entry| {
                                   let entry = entry.unwrap();
                                   if entry.path().is_dir() {
                                       Some(entry.path())
                                   } else {
                                       None
                                   }
                               })
                               .collect();

    entries.sort(); // Sort the directory entries

    let total_folders = entries.len(); // Count the number of folders
    info!("{} folders to prepare", total_folders);

    // Now iterate over the sorted and filtered entries
    let nb_skipped_files : usize = entries.par_iter().enumerate().map(|(index, cve_path)| {
        let cve_id = cve_path.file_name().unwrap().to_str().unwrap().to_string();
        info!("preparation of the cve : {} ({}/{})", cve_id, index + 1, total_folders);

        let pull_request = CveCommit::new(cve_id.as_str(), input_dataset_folder_path.to_str().unwrap());
        if !pull_request.has_file_of_language(language.into()) {
            debug!("The cve {} has no file of language {}", cve_id, language);
            return 0usize;
        }else{
            let cve = extract_cve_removed_code_changes(&pull_request, language);
            let nb_skipped_files = cve.other_than_modified_files.len();
            cve.save(output_dataset_folder_path.to_str().unwrap(), language);
            return nb_skipped_files;
        }
    }).sum();
    /*for (index, cve_path) in entries.iter().enumerate() {
        let cve_id = cve_path.file_name().unwrap().to_str().unwrap().to_string();
        info!("preparation of the cve : {} ({}/{})", cve_id, index + 1, total_folders);
    
        let pull_request = PullRequest::new(cve_id.as_str(), input_dataset_folder_path.to_str().unwrap());
        if !pull_request.has_file_of_language(language) {
            debug!("The cve {} has no file of language {}", cve_id, language);
        } else {
            let cve = extract_cve_removed_code_changes(&pull_request, language);
            cve.save(output_dataset_folder_path.to_str().unwrap(), language, min_line_length_of_cve_code);
        }
    }*/
    let (cve_folder_count, cve_codes_file_count) = count_cve_and_files(output_dataset_folder_path.to_str().unwrap());

    info!("Dataset preparation finished, with {} cves, {} codes and skip {} not modified files", cve_folder_count, cve_codes_file_count, nb_skipped_files);

    Ok(())
}
