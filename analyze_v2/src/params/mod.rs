
pub mod argv;

/// in millisecondes
pub const MEMORY_MIN_TIME_INTERVAL : u64 = 1000; 

pub const MEMORY_BENCHMARK_FILE : &'static str = "benchmark_data/benchmark_memory.txt";

pub const ANOMALIE_SINGLE_QUERY_TIME_THRESHOLD : f64 = 1.0; // in second




