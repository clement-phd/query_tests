use std::path::Path;

use rust_utils::tree_sitter_utils::languages::Language;
use serde_derive::{Deserialize, Serialize};
use walkdir::WalkDir;



/// Data about a file for a CVE in the prepared dataset
#[derive(Clone, PartialEq, Eq, Hash, Serialize, Deserialize, Debug)]
pub struct CveFile {
    /// the file path, the last folder with .patch, a folder containing original, fixed and cve codes
    pub file_folder_path : String,
    /// the path to the original code file
    pub original_file_path : String,
    /// the path to the fixed code file
    pub fixed_file_path : String,
}


impl CveFile {
    /// Create a new CveFile from a file folder path and a language
    pub fn new(file_folder_path : &str, language : &Language) -> CveFile {
        let extension = language.get_file_extension();
        let file_folder_path_o = Path::new(file_folder_path);

        let original_file_path = file_folder_path_o.join(format!("original.{}", extension));
        let fixed_file_path = file_folder_path_o.join(format!("fixed.{}", extension));

        return CveFile {
            file_folder_path : file_folder_path.to_string(),
            original_file_path : original_file_path.to_str().unwrap().to_string(),
            fixed_file_path : fixed_file_path.to_str().unwrap().to_string(),
        }
    }
}

/// Data about a CVE in the prepared dataset
#[derive(Clone, PartialEq, Eq, Hash, Serialize, Deserialize, Debug)]
pub struct Cve {
    pub cve_id: String,
    pub cve_folder_path: String,
    pub cve_files: Vec<CveFile>,
}

impl Cve {
    pub fn from_cve_folder_path(cve_folder_path: &str, language : &Language) -> Cve {
        let cve_folder_path_o = Path::new(cve_folder_path);
        let cve_id = cve_folder_path_o.file_name().unwrap().to_str().unwrap().to_string();
        let cve_files = Cve::find_patch_folders(cve_folder_path);

        // Assuming a method 'from_file_name' for CVEFile
        let cve_files = cve_files.iter().map(|f| CveFile::new(f, language)).collect();

        Cve {
            cve_id,
            cve_folder_path: cve_folder_path.to_string(),
            cve_files,
        }
    }

    /// find all the patch folder (finished by .patch, and contain original file, fixed file and cve_code folder)
    fn find_patch_folders(root_folder: &str) -> Vec<String> {
        let mut patch_folders = Vec::new();

        for entry in WalkDir::new(root_folder) {
            if let Ok(entry) = entry {
                let path = entry.path();
                if path.is_dir() && path.extension().unwrap_or_default() == "patch" {
                    patch_folders.push(path.to_str().unwrap().to_string());
                }
            }
        }
        
        patch_folders
    }
}