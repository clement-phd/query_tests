
use serde_derive::{Deserialize, Serialize};
use tree_sitter::Point;


/// This struct is used to represent a point in the source code
/// It is 0-indexed
#[derive(Clone, PartialEq, Eq, Hash, Debug, Deserialize, Serialize)]
pub struct CustomPoint {
    pub row : usize,
    pub column : usize,
}

impl From<Point> for CustomPoint {
    fn from(point : Point) -> Self {
        CustomPoint {
            row : point.row,
            column : point.column,
        }
    }
}

/// this is one match of the query
#[derive(Clone, PartialEq, Eq, Hash, Debug, Deserialize, Serialize)]
pub struct QueryMatch {
    pub name : String,
    pub start : CustomPoint,
    pub end : CustomPoint,
}

impl QueryMatch {
    /// get the cve_name from the name
    fn get_cve_name(&self) -> &str {
        // Find the last occurrence of '_' in the name
        if let Some(index) = self.name.rfind('_') {
            // Slice the name to remove the '_number' at the end
            &self.name[..index]
        } else {
            // If no '_' is found, return the original name
            &self.name
        }
    }
}

/// This structure handle the output of the query
#[derive(Clone, PartialEq, Eq, Hash, Debug, Deserialize, Serialize)]
pub struct QueryOutput {
    pub file_path : String,
    pub file_name : String,
    pub query_results : Vec<QueryMatch>
}

impl QueryOutput{

    /// test if a query output has a match for a given cve
    pub fn has_cve_match(&self, cve_id : &str) -> bool {
        for query_match in &self.query_results {
            if query_match.get_cve_name() == cve_id {
                return true;
            }
        }

        false
    }

    pub fn fusion(&self, other : &QueryOutput) -> QueryOutput {
        assert!(self.file_path == other.file_path);
        assert!(self.file_name == other.file_name);
        let mut query_results = self.query_results.clone();
        query_results.extend(other.query_results.clone());

        QueryOutput {
            file_path : self.file_path.clone(),
            file_name : self.file_name.clone(),
            query_results
        }
    }
}

