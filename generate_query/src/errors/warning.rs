use serde_derive::{Deserialize, Serialize};





/// represent the warning type that can occur when generating a query
#[derive(Debug, Clone, PartialEq, Eq, Hash, PartialOrd, Ord, Serialize, Deserialize)]
pub enum GenerateQueryWarningType {
    OriginalFileMalformed,
    QueryHasErrorNode,
    QueryHasMissingNode,
    QueryHasUnexpectedNode,
}

impl std::fmt::Display for GenerateQueryWarningType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            GenerateQueryWarningType::OriginalFileMalformed => write!(f, "The original file is malformed, and contain warning when parsing"),
            GenerateQueryWarningType::QueryHasErrorNode => write!(f, "The query contain an error node"),
            GenerateQueryWarningType::QueryHasMissingNode => write!(f, "The query contain a missing node"),
            GenerateQueryWarningType::QueryHasUnexpectedNode => write!(f, "The query contain an unexpected node"),
        }
    }
}


/// represent the payload of the warning that can occur when generating a query
/// This error can be recovered. The stored quey is the invalid one, if possible
#[derive(Debug, Clone, Serialize, Deserialize, Hash)]
pub struct GenerateQueryWarningPayload {
    pub query : Option<String>,
    pub type_error : GenerateQueryWarningType,
}


impl GenerateQueryWarningPayload {
    pub fn new(query : Option<&str>, type_error : GenerateQueryWarningType) -> GenerateQueryWarningPayload {
        GenerateQueryWarningPayload {
            query : query.map(|s| s.to_string()),
            type_error : type_error,
        }
    }
}

impl std::fmt::Display for GenerateQueryWarningPayload {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.type_error)
    }
}

/// represent a warning
/// This error can be recovered. The stored quey is the invalid one, if possible
#[derive(Debug, Clone, Serialize, Deserialize, Hash)]
pub struct GenerateQueryWarning {
    payloads : Vec<GenerateQueryWarningPayload>,
    label : String,
}

impl GenerateQueryWarning {
    pub fn new(label : &str, payloads : Vec<GenerateQueryWarningPayload>) -> GenerateQueryWarning {
        GenerateQueryWarning {
            payloads : payloads,
            label : label.to_string(),
        }
    }

    pub fn add_payload(&mut self, payload : GenerateQueryWarningPayload) {
        self.payloads.push(payload);
    }

    pub fn get_label(&self) -> &str {
        &self.label
    }

    pub fn get_payloads(&self) -> &Vec<GenerateQueryWarningPayload> {
        &self.payloads
    }
}

impl std::fmt::Display for GenerateQueryWarning {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut res = format!("Warning when genrating the query {} (", self.label);
        for payload in &self.payloads {
            res.push_str(&format!("{},", payload));
        }
        res.push(')');
        

        write!(f, "{}", res)
    }
}