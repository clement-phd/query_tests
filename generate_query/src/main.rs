use std::io;

use log::info;
use params::argv::get_program_args;
use rust_utils::logger::common_logger::init_logger;

use crate::pipeline::generate_all_queries;

mod params;
mod generators;
mod pipeline;
mod errors;
mod data;




fn main() -> Result<(),  io::Error> {
    dotenv::dotenv().ok();
    init_logger();
    let argv = get_program_args();
    info!("🚀 Start generate_query.");
    generate_all_queries(
        argv.files_path.as_ref(),
        argv.output_path.as_str(),
        argv.directory_path.as_ref().map(|s| s.as_str()),
        &argv.generator,
        Some(&argv.language),
        argv.context_threshold
    )?;
    Ok(())
}