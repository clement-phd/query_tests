
use std::fs;
use std::path::Path;

use plot_helper::data::plot_data::Layout;
use plot_helper::data::plottable::sample::Sample;
use plot_helper::data::plottable::{PlottableSamples, PlottableStruct};
use plot_helper::plotter::line_plot::line_plot;
use plot_helper::stats::stats_serie::MetricName;
use plot_helper::wrapper::correctness_wrapper::CorrectnessStatsWrapper;

use crate::data::correctness_sample::CorrectnessSample;
use crate::data::keys::CorrectnessQueryLinesKey;

fn save_correctness(
    data : &PlottableStruct<CorrectnessSample, CorrectnessQueryLinesKey>,
    save_path_o : &Path,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut csv_writer = csv::Writer::from_path(save_path_o.to_str().unwrap())?;
    let mut headers = vec![
        "language".to_string(),
        "generator".to_string(),
        "context_threshold".to_string(),
    ];
    headers.append(&mut CorrectnessStatsWrapper::get_headers().into_iter().map(|x| x.to_string()).collect::<Vec<String>>());

    csv_writer.write_record(&headers)?;

    let mut global_by_language = std::collections::HashMap::new();
    let mut global = CorrectnessStatsWrapper::default();

    data.get_samples().iter().for_each(|sample| {

        let language = sample.get_string_value(&CorrectnessQueryLinesKey::Language);
        let mut record = vec![
            language.clone(),
            sample.get_string_value(&CorrectnessQueryLinesKey::Generator),
            sample.get_numeric_value(&CorrectnessQueryLinesKey::MinQueryLineOfCode).to_string(),
        ];
        record.append(&mut sample.get_correctness().get_values().into());
        csv_writer.write_record(&record).unwrap();

        let global_by_language_entry = global_by_language.entry(language.clone()).or_insert(CorrectnessStatsWrapper::default());
        *global_by_language_entry += sample.get_correctness().clone();

        global += sample.get_correctness().clone();
    });

    

    let mut language_keys = global_by_language.keys().cloned().collect::<Vec<String>>();
    language_keys.sort();

    for language in language_keys {
        let correctness = global_by_language.get(&language).unwrap();
        let mut record = vec![
            language,
            "global".to_string(),
            "global".to_string(),
        ];
        record.append(&mut correctness.get_values().into());
        csv_writer.write_record(&record).unwrap();
    }

    let mut global_record = vec![
        "global".to_string(),
        "global".to_string(),
        "global".to_string(),
    ];
    global_record.append(&mut global.get_values().into());
    csv_writer.write_record(&global_record).unwrap();

    csv_writer.flush()?;
    Ok(())
}



pub fn gen_correctness_data(
    data : &PlottableStruct<CorrectnessSample, CorrectnessQueryLinesKey>, 
    output_dir_path : &str, 
    generator : &str,
    additionnal_name : &str
) -> Result<(), Box<dyn std::error::Error>> {
    let folder_path_o = Path::new(output_dir_path).join("correctness");
    fs::create_dir_all(folder_path_o.to_str().unwrap())?;
    let folder_path_o = folder_path_o.join(generator);
    fs::create_dir_all(folder_path_o.to_str().unwrap())?;
    let folder_path_o = folder_path_o.join(additionnal_name);
    fs::create_dir_all(folder_path_o.to_str().unwrap())?;


    // plot
    {
        let save_folder_path_o = folder_path_o.join("correctness_evolutions");
        fs::create_dir_all(save_folder_path_o.to_str().unwrap())?;
        {
            let file_path_o = save_folder_path_o.join("correctness_evolutions.png");

            line_plot(
                data,
                Some(CorrectnessQueryLinesKey::Generator),
                file_path_o.to_str().unwrap(),
                &Layout::new(2, 4),
                vec![
                    (CorrectnessQueryLinesKey::MinQueryLineOfCode, CorrectnessQueryLinesKey::TruePositive, None),
                    (CorrectnessQueryLinesKey::MinQueryLineOfCode, CorrectnessQueryLinesKey::FalsePositive, None),
                    (CorrectnessQueryLinesKey::MinQueryLineOfCode, CorrectnessQueryLinesKey::TrueNegative, None),
                    (CorrectnessQueryLinesKey::MinQueryLineOfCode, CorrectnessQueryLinesKey::FalseNegative, None),
                    (CorrectnessQueryLinesKey::MinQueryLineOfCode, CorrectnessQueryLinesKey::Precision, None),
                    (CorrectnessQueryLinesKey::MinQueryLineOfCode, CorrectnessQueryLinesKey::Recall, None),
                    (CorrectnessQueryLinesKey::MinQueryLineOfCode, CorrectnessQueryLinesKey::F1, None),
                    (CorrectnessQueryLinesKey::MinQueryLineOfCode, CorrectnessQueryLinesKey::Accuracy, None),
                ],
                None,
                MetricName::Mean
            )?;
        }
    }
    // csv
    {
        let folder_path_o = folder_path_o.join("correctness_details_data");
        fs::create_dir_all(folder_path_o.to_str().unwrap())?;
        let file_path_o = folder_path_o.join("correctness_details_data.csv");
        save_correctness(data, &file_path_o)?;
    }

    Ok(())
}