use std::path::Path;

use benchmark_v2::results::commit_results::CommitResults;
use plot_helper::data::plottable::sample::{Sample, SimpleSample};
use plot_helper::data::plottable::{PlottableSimpleSamplesFromPaths, PlottableStruct};
use regex::Regex;
use rust_utils::tree_sitter_utils::languages::Language;
use rust_utils::utils::get_all_files;
use serde::{Deserialize, Serialize};

use super::keys::RepoKey;

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct RepoSample {
    repo : String,
    commit : String,
    execution_time : f64,
    file_number : u32,
    line_number : u32,
    char_number : u32,
    threshold : u32,
    language : Language
}


impl Sample<RepoKey> for RepoSample {
    fn get_numeric_value(&self, key : &RepoKey) -> f32 {
        match key {
            RepoKey::ExecutionTime => self.execution_time as f32,
            RepoKey::Repo => unreachable!("repo is not numeric"),
            RepoKey::Commit => unreachable!("commit is not numeric"),
            RepoKey::FileNumber => self.file_number as f32,
            RepoKey::LineNumber => self.line_number as f32,
            RepoKey::CharNumber => self.char_number as f32,
            RepoKey::MinQueryLineOfCode => self.threshold as f32,
            RepoKey::Language => unreachable!("language is not numeric"),
        }
    }

    fn get_string_value(&self, key : &RepoKey) -> String {
        match key {
            RepoKey::Repo => self.repo.clone(),
            RepoKey::Commit => self.commit.clone(),
            RepoKey::ExecutionTime => unreachable!("execution time is not string"),
            RepoKey::FileNumber => unreachable!("file number is not string"),
            RepoKey::LineNumber => unreachable!("line number is not string"),
            RepoKey::CharNumber => unreachable!("char number is not string"),
            RepoKey::MinQueryLineOfCode => unreachable!("min query line of code is not string"),
            RepoKey::Language => self.language.to_string(),
        }
    }
}

impl SimpleSample<RepoKey> for RepoSample {
    fn new_from_file_path(file_path : &str) -> Result<Self, Box<dyn std::error::Error>> {
        let file_content = std::fs::read_to_string(file_path).unwrap();
        let commit_result : CommitResults = serde_json::from_str(&file_content).unwrap();
        let mut time: f64 = 0.0;
        for file_result in &commit_result.results {
            for query_result in &file_result.query_results {
                time += query_result.query_time;
            }
        }

        let mut line_number = 0;
        let mut char_number = 0;
        for file_result in &commit_result.results {
            line_number += file_result.file_nb_of_lines;
            char_number += file_result.file_nb_of_chars;
        }

        Ok(RepoSample {
            repo : commit_result.repo_url.clone(),
            commit : commit_result.commit_hash.clone(),
            execution_time : time,
            file_number : commit_result.results.len() as u32,
            line_number : line_number,
            char_number : char_number,
            threshold : commit_result.min_query_line_of_code as u32,
            language : commit_result.language.clone()
        })
    }
}



pub fn load_plottable_repo(data_dir : &str) -> PlottableStruct<RepoSample, RepoKey> {
    let data_dir_o = Path::new(data_dir);
    let result_dir_o = data_dir_o.join("benchmark_results");
    let files = get_all_files(result_dir_o.to_str().unwrap());

    // isolate repo results
    let all_repo_results_path : Vec<String> = files.iter().filter_map(|file_path| {
        if isolate_repo_result(file_path) {
            return Some(file_path.to_string());
        }
        return None;
    }).collect();

    PlottableStruct::<RepoSample, RepoKey>::new_async_from_paths(&all_repo_results_path)
}

fn isolate_repo_result(file_path : &str) -> bool {
    let file_path_o = Path::new(file_path);
    let file_name = file_path_o.file_name().unwrap().to_str().unwrap();
    let pattern = r"[0-9a-f]{40}\.json$";
    let re = Regex::new(pattern).expect("Invalid regex pattern");
    if re.is_match(file_name) {
        let folder = file_path_o.parent().unwrap().file_name().unwrap().to_str().unwrap();

        if folder == "repo" { // get only repo results (there can be file json with 40 digits)
            return true;
        }
    }
    return false;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_isolate_repo_result() {
        let all_files_path = vec![
            "analyze_v2/tests/ressources/repo/02c127b344ed068bd769daa54f0a15f31b5a355e.json",
            "analyze_v2/tests/ressources/repo/5ac03453298100ed5006664669e6588049d403e2.json",
            "analyze_v2/tests/ressources/repo/5c9afd93a93db74cbe83ed2ada7a3eefd6de3b70.json",
            "analyze_v2/tests/ressources/repo/6f412c00cf06c883bf08f0e7d6f7eb7299d198e9.json",
            "analyze_v2/tests/ressources/repo/7dc431839eeeffe6ed65acbe9bfe2a6e89422086.json",
            "analyze_v2/tests/ressources/repo/10aedb6f9de572e2744ad0f25adf5a715f0306ea.json",
            "/home/clahoche/Documents/github/cve-detection/analyze_v2/tests/ressources/repo/33d7fedca951912e8e4b2208058f05ee63dc77bf.json",
            "/home/clahoche/Documents/github/cve-detection/analyze_v2/tests/ressources/repo/34e17203bd1fda75e198a1b0ef90e30ced0b598a.json"
        ];

        for file_path in all_files_path {
            assert_eq!(isolate_repo_result(file_path), true);
        }
    }
}