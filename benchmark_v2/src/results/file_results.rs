use std::{fs, io};

use generate_query::params::argv::Generator;
use rust_utils::tree_sitter_utils::languages::Language;
use serde_derive::{Serialize, Deserialize};

use crate::instances::instance::Instance;
use crate::monitoring::MonitoringResults;
use crate::prepared_dataset::FileToTest;

use super::query_results::QueryResult;

/// The result and stats of the run of several query on a file
/// for a given generator and a given threshold of line of code
#[derive(Clone, PartialEq, Serialize, Deserialize, Debug)]
pub struct FileResult {
    // perfs
    pub parsing_time: f64,
    pub parsing_max_memory: f64,
    
    // file
    pub file: FileToTest,
    pub language: Language,

    // query
    pub generator: Generator,
    pub min_query_line_of_code: u32,
    pub query_results: Vec<QueryResult>,

    // stats
    pub file_nb_of_lines: u32,
    pub file_nb_of_chars: u32,
}


impl FileResult {
    /// Create a new FileResult from the results of the parsing and queries
    /// the query results are a vector of (query_output, query_monitoring_stats, query_file_path)
    pub fn new(
        parsing_monitoring_stats : MonitoringResults,
        file : FileToTest,
        instance : &Instance,
        query_test_results: Vec<QueryResult>,
    
    ) -> FileResult {
        
        let file_content = fs::read_to_string(&file.file_path).unwrap();
        let file_nb_of_lines = file_content.lines().count() as u32;
        let file_nb_of_chars = file_content.chars().count() as u32;

        FileResult {
            parsing_time: parsing_monitoring_stats.time.as_secs_f64(),
            #[cfg(feature = "memory_monitoring")]
            parsing_max_memory: parsing_monitoring_stats.max_memory_usage.rss as f64,
            #[cfg(not(feature = "memory_monitoring"))]
            parsing_max_memory: 0.0,
            query_results: query_test_results,
            file,
            min_query_line_of_code: instance.get_min_line_of_code_for_cve_code() as u32,
            language: instance.get_language().clone(),
            generator: instance.get_generator().clone(),
            file_nb_of_lines,
            file_nb_of_chars,
        }

    }

    pub fn save(&self, file_path : &str) -> Result<(), io::Error> {
        let file = fs::File::create(file_path)?;
        serde_json::to_writer(file, self)?;
        Ok(())
    }

    pub fn get_multi_line_query_results(&self) -> QueryResult{
        let vec : Vec<QueryResult> = self.query_results.iter()
            .filter(|query_result| query_result.query_nb_of_lines > 1)
            .cloned().collect();

        if vec.len() != 1 {
            panic!("There should be only one multi line query result : {}", self.file.file_path);
        }else{
            vec[0].clone()
        }
    }

    pub fn get_single_query_instances(&self) -> Vec<QueryResult>{
        self.query_results.iter()
            .filter(|query_result| query_result.query_nb_of_lines == 1)
            .cloned().collect()
    }
}