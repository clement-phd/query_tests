use serde_derive::{Deserialize, Serialize};

use super::warning::GenerateQueryWarning;









/// represent the error type that can occur when generating a query
#[derive(Debug, Clone, PartialEq, Eq, Hash, PartialOrd, Ord, Serialize, Deserialize)]
pub enum GenerateQueryErrorType {
    AllTheFile,
    ContextThreshold,
    TooMuchCharacter,
    Malformed
}


impl std::fmt::Display for GenerateQueryErrorType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            GenerateQueryErrorType::AllTheFile => write!(f, "The query range is the whole file"),
            GenerateQueryErrorType::ContextThreshold => write!(f, "The query range doesn't contain enough context"),
            GenerateQueryErrorType::TooMuchCharacter => write!(f, "The query contain too much character"),
            GenerateQueryErrorType::Malformed => write!(f, "The query is malformed"),
        }
    }
}



/// represent the payload of the error that can occur when generating a query
#[derive(Debug, Clone, Serialize, Deserialize, Hash)]
pub struct GenerateQueryErrorPayload {
    label : String,
    warnings : Option<GenerateQueryWarning>,
    type_error : GenerateQueryErrorType,
}

impl GenerateQueryErrorPayload {
    pub fn new(label : &str, type_error : GenerateQueryErrorType, warnings : Option<GenerateQueryWarning>) -> GenerateQueryErrorPayload {
        GenerateQueryErrorPayload {
            label : label.to_string(),
            warnings,
            type_error : type_error,
        }
    }

    pub fn get_label(&self) -> &str {
        &self.label
    }

    pub fn get_warnings(&self) -> &Option<GenerateQueryWarning> {
        &self.warnings
    }

    pub fn get_type_error(&self) -> &GenerateQueryErrorType {
        &self.type_error
    }
}

impl std::fmt::Display for GenerateQueryErrorPayload {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Error when genrating the query {} ({})", self.label, self.type_error)
    }
}
