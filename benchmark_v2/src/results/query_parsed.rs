use rust_utils::tree_sitter_utils::languages::Language;
use rust_utils::tree_sitter_utils::query_wraper::QueryWrapper;

use crate::monitoring::MonitoringResults;





pub struct QueryParsed {
    /// WARN ! : keep the string to avoid the query to be dropped (c pointer)
    query_path : String,
    query_name : String,
    query : Vec<QueryWrapper>,
    load_stats : MonitoringResults,
}

impl QueryParsed {
    pub fn new(
        query_path : &str,
        language : &Language
    ) -> Self {
        let query_str = std::fs::read_to_string(query_path).expect("Error reading the query");


        let closure = || {
            QueryWrapper::new(query_str.as_str(), language)
        };
        
        let (query, load_stats) = MonitoringResults::monitor_thread(closure);

        let query_name = query_path.split("/").last().unwrap().replace(".scm", "").to_string();

        Self {
            query_path : query_path.to_string(),
            query_name,
            query,
            load_stats,
        }

    }
    pub fn get_query_path(&self) -> &str {
        self.query_path.as_str()
    }


    pub fn get_query_name(&self) -> &str {
        self.query_name.as_str()
    }

    pub fn get_query(&self) -> &Vec<QueryWrapper> {
        &self.query
    }

    pub fn get_load_stats(&self) -> &MonitoringResults {
        &self.load_stats
    }
}